<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'member/company';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin'] = 'admin/dashboard';

$route['influenza'] = 'member/influenza';

$route['questionnare'] = 'member/questionnare';
$route['questionnare/topic/(:num)'] = 'member/questionnare/topic/$1';


$route['admin/questionnare/topic/add'] = 'admin/questionnare/addTopic';
$route['admin/questionnare/topic/edit/(:num)'] = 'admin/questionnare/editTopic/$1';

$route['admin/questionnare/questions/add'] = 'admin/questionnare/addQuestion';
$route['admin/questionnare/questions/edit/(:num)'] = 'admin/questionnare/editQuestion/$1';

//Practice changes
$route['practice'] = 'practice/dashboard';

$route['practice/questionnare/topic/add'] = 'practice/questionnare/addTopic';
$route['practice/questionnare/topic/edit/(:num)'] = 'practice/questionnare/editTopic/$1';

$route['practice/questionnare/questions/add'] = 'practice/questionnare/addQuestion';
$route['practice/questionnare/questions/edit/(:num)'] = 'practice/questionnare/editQuestion/$1';

//Practice changes
$route['doctor'] = 'doctor/dashboard';

$route['doctor/questionnare/topic/add'] = 'doctor/questionnare/addTopic';
$route['doctor/questionnare/topic/edit/(:num)'] = 'doctor/questionnare/editTopic/$1';

$route['doctor/questionnare/questions/add'] = 'doctor/questionnare/addQuestion';
$route['doctor/questionnare/questions/edit/(:num)'] = 'doctor/questionnare/editQuestion/$1';