  <!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Login</title>
  
  
  <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Open+Sans:600'>
      <link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/login.css">
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
  
</head>
<body>

  <div class="login-wrap">
  <div class="login-html">
  <?php echo $this->session->userdata('first_time'); ?>
    <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Einloggen</label>
    <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Registrierung</label>
    <div class="login-form">
      <?php 
      $attributes = array('class' => 'sign-in-htm');
      echo form_open('login/validation', $attributes); ?>
        <div class="group">
          <label for="user" class="label">Email</label>
          <input id="username" name="username" type="text" class="input" required>
        </div>
        <div class="group">
          <label for="pass" class="label">Passwort</label>
          <input id="login_password" name="password" type="password" class="input" data-type="password" required>
        </div>
        <div class="group">
          <input type="submit" class="button" value="Einloggen">
        </div>
        <?php echo validation_errors('<div class="error">'); ?>
		<?php if($this->session->flashdata('msg')){
			echo $this->session->flashdata('msg');
		} ?>
		<?php if($this->session->flashdata('wrong')){ ?>
			<div class="alert alert-danger mt-3" role="alert">
				<?php echo $this->session->flashdata('wrong'); ?>
			</div>
		<?php } ?>
        <div class="hr"></div>
        <div class="foot-lnk">
          <a href="<?php echo base_url() ?>register/forgotpassword">Passwort vergessen?</a>
        </div>
      </form>
      <?php 
      $attributes = array('class' => 'sign-up-htm');
      echo form_open('register', $attributes); ?>
        <div class="group">
          <label for="user" class="label">Email</label>
          <input id="username" name="username" type="email" class="input">
        </div>
        <div class="group">
          <label for="pass" class="label">Passwort</label>
          <input id="password" name="password" type="password" class="input" data-type="password" required>
        </div>
        <div class="group">
          <label for="pass" class="label">Passwort bestätigen</label>
          <input id="confirm_password" type="password" class="input" data-type="password" required>
        </div>
        <?php echo $recaptcha ?>
        <div class="group">
          <input type="submit" class="button" value="Registrieren">
        </div>
        <div class="hr"></div>
        <div class="foot-lnk">
			<label for="tab-1">Schon registriert?</a></label>
        </div>
      </form>
    </div>
  </div>
</div>
  
<script>
  var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwörter stimmen nicht überein");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>  
</body>
</html>
