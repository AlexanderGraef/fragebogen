<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="UTF-8">
	<title>Forgot Password</title>


	<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Open+Sans:600'>
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/login.css">

</head>
<body>
<div class="login-wrap">
	<div class="login-html">
		<?php if($this->session->flashdata('msg')){
			echo $this->session->flashdata('msg');
		} ?>
		<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Reset Passwoord</label>
		<div class="login-form">
			<?php
			$attributes = array('class' => 'sign-in-htm');
			echo form_open('register/updatePassword/'.$token); ?>
			<input id="token" name="token" type="hidden" value="<?php echo $token ?>">
			<div class="group">
				<label for="password" class="label">Password</label>
				<input id="password" name="password" type="password" class="input" required>
			</div>
			<div class="group">
				<label for="password" class="label">Repeat Password</label>
				<input id="repeatPassword" name="repeatPassword" type="password" class="input" required>
			</div>
			<div class="group">
				<input type="submit" class="button" value="Send">
			</div>
			<?php echo validation_errors('<div class="error">'); ?>
			<div class="hr"></div>
			</form>
		</div>
	</div>
</div>
</body>
</html>
