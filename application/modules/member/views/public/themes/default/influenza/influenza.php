<?php 
$disabled = "";
if(isset($practice_accounted_terminated->is_deleted))
{
	if($practice_accounted_terminated->is_deleted == 1)
	{
		$disabled = "disabled";
		echo "<h3 style='color:red; margin-top:30px'>Note: Your Company is Terminated</h3>";	
	}
}

?>
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('influenza')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('influenza')?></h6>
  </div>
  <?php 
if(empty($influenza) || $disabled == "disabled")
{
	?>
		<div class="card-body" style="margin: auto;">
			<div class="form-group">
				<label style="font-size: 12px;"> 
		           No notification yet...
		          </label>
		    </div>
		</div>
	<?php
}
else
{

  ?>
 
  <div class="card-body" style="margin: auto;">
  <?php echo validation_errors('<div class="error">'); ?>
  <?php echo form_open(); ?>
	<div class="row mt-5">
	 <h4>
 <?= $this->lang->line('influenza')?> Date: <?php  echo $influenza->send_date; ?>
  </h4>
	</div>
	<div>
		<div class="form-group">
		<label style="font-size: 12px;"> 
	           Please give the amount of influenza you need. <?= $this->lang->line('influenza')?>.
	          </label>
	          <input type="hidden" name="influenza_id" value="<?php  echo $influenza->id; ?>">
			 <input type="number" min="0" required name="influenza_response" placeholder="Influenza Amount" class="form-control">
		</div>
	</div>
	 <div style="text-align: right">
		<div class="form-group">
			<?php
				echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
			?>
	  	</div>
    </div>
	<div>

		<div class="form-group">
	        <ul style="font-size: 12px; max-width: 400px">
	          <li>
	            <span style="background-color: yellow">Note:</span> Here you will submit the amount of <?= $this->lang->line('influenza')?> you need for your company. This will send an email to your admin.
	          </li>
	         
	      </ul>
		</div>
	</div>
   
	</div>
</form>

  </div>

  <?php
}
   ?>
</div>

</div>
<!-- /.container-fluid -->