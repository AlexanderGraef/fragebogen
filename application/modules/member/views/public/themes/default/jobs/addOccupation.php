

<h2 class="mt-5">Add new Occupation</h2>
<?php echo validation_errors('<div class="error">'); ?>
<?php echo form_open(); ?>
<div class="row mt-5">
	<div class="col-sm-5">
		<div class="form-group">
			<?php
			echo form_label("Occupation Name", "occupationName");
			echo form_input("occupationName", "", array("class" => "form-control"));
			?>
		</div>
	</div>
	<div class="col-sm-5">
		<div class="form-group">
			<?php
			echo form_label("Select Jobs", "job"); ?>
			<select id="hazardGroup" class="selectpicker" name="jobId[]" multiple="multiple">
				<?php foreach($jobs as $j):?>
					<option value="<?php echo $j->id; ?>"><?php echo $j->jobName; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
</div>

<div class="col-sm-5">
	<div class="form-group">
		<?php
		echo form_submit("submit", "Save", array("class" => "btn btn-success"));
		?>
	</div>
</div>
</form>

