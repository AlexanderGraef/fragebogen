
<h2 class="mt-5">Add Worker</h2>
<?php echo validation_errors('<div class="error">'); ?>
<?php echo form_open(); ?>
<div class="row mt-5">
	<div class="col-sm-5">
		<div class="form-group">
			<?php
			echo form_label("First Name", "firstName");
			echo form_input("firstName", "", array("class" => "form-control"));
			?>
		</div>
	</div>
	<div class="col-sm-5">
		<div class="form-group">
			<?php
			echo form_label("Last Name", "lastName");
			echo form_input("lastName", "", array("class" => "form-control"));
			?>
		</div>
	</div>
	<div class="col-sm-5">
		<div class="form-group">
			<?php
			echo form_label("Select Occupation", "occupation"); ?>
			<select id="occupation" name="occupationId" class="form-control" onchange="selectoption()">
				<?php foreach($occupations as $o):?>
					<option value="<?php echo $o->id; ?>"><?php echo $o->occupationName; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
	<table class="table">
		<thead>
		<tr>
			<th scope="col">Job</th>
			<th scope="col">Hazards</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>Mark</td>
			<td>Otto</td>
		</tr>
		</tbody>
	</table>
</div>

<div class="col-sm-5">
	<div class="form-group">
		<?php
		echo form_submit("submit", "Save", array("class" => "btn btn-success"));
		?>
	</div>
</div>
</form>
<script>
	function selectoption()
	{
		var optionVal = $('#occupation option:selected');
		for(var i = 0; i < optionVal.length; i++) {
			$.ajax(
				{
					type:"post",
					url: "<?php echo base_url(); ?>jobs/jobsOccupationsAjax/" + optionVal.eq(i).val(),
					success:function(response)
					{
						var obj = $.parseJSON(response);
						console.log(obj)
						$.each(obj, function (index, object) {
							if ($('#check'+ object["id"] +'').length === 0) {
								//$('.groups').append('<div id="check' + object['id'] +'" class="form-check"><label class="form-check-label"><input type="checkbox" class="form-check-input" name="hazardId[]" value="'+ object['id'] +'">'+ object['hazardName'] +'</label></div>');
							}
						})
					}
				}
			);
		}
		//if (listtwo.options[i].text === searchtext) listtwo.options[i].selected = true;
	}
</script>
