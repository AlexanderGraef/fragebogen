
<h2 class="mt-5">Add new Job</h2>
<?php echo validation_errors('<div class="error">'); ?>
<?php echo form_open(); ?>
<div class="row mt-5">
	<div class="col-sm-5">
		<div class="form-group">
			<?php
			echo form_label("Job Name", "jobName");
			echo form_input("jobName", "", array("class" => "form-control"));
			?>
		</div>
	</div>
	<div class="col-sm-5">
		<div class="form-group">
			<?php
			echo form_label("Hazard Group", "hazardGroup"); ?>
			<select id="hazardGroup" class="selectpicker" multiple="multiple" onchange="selectoption()">
			<?php foreach($hazardGroups as $hg):?>
				<option value="<?php echo $hg->id; ?>"><?php echo $hg->name; ?></option>
<!--				<input type="checkbox" id="hazardGroup" onchange="selectoption()" name="groupId" class="form-controll" class="hazardgroup" value="--><?php //echo $hg->id ?><!--">--><?php //echo $hg->name ?>
<!--			--><?php endforeach; ?>
			</select>
		</div>
	</div>
		<div class="col-sm-5 groups">
		</div>
</div>

<div class="col-sm-5">
	<div class="form-group">
		<?php
		echo form_submit("submit", "Save", array("class" => "btn btn-success"));
		?>
	</div>
</div>
</form>


<script>
    function selectoption()
    {
    	var optionVal = $('#hazardGroup option:selected');
        for(var i = 0; i < optionVal.length; i++) {
				$.ajax(
					{
						type:"post",
						url: "<?php echo base_url(); ?>jobs/hazardsAjax/" + optionVal.eq(i).val(),
						success:function(response)
						{
							var obj = $.parseJSON(response);
							$.each(obj, function (index, object) {
								if ($('#check'+ object["id"] +'').length === 0) {
									$('.groups').append('<div id="check' + object['id'] +'" class="form-check"><label class="form-check-label"><input type="checkbox" class="form-check-input" name="hazardId[]" value="'+ object['id'] +'">'+ object['hazardName'] +'</label></div>');
								}
							})
						}
					}
				);
		}
            //if (listtwo.options[i].text === searchtext) listtwo.options[i].selected = true;
    }
    </script>

