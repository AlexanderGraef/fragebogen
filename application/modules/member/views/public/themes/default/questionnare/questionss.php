
<h2 class="mt-5"><?php echo $topic['topicName'] ?></h2>
<?php echo $topic['description']  ?>
<?php echo ("\n"); ?>
<?php echo validation_errors('<div class="error">'); ?>
<?php echo form_open(); ?>
<table class="table">
	<tbody>
		<?php
		$company = $this->AdminModel->getCompany($this->uri->segment(4));
		if ($this->uri->segment(1) == "admin") {
			$userId = $company['userId'];
		} else {
			$userId = $this->session->userdata('id');
		}

$questions = array();
foreach($result as $row) {
  $row['childs'] = array();
  $questions[$row['id']] = $row;
}

// now loop your comments list, and everytime you find a child, push it 
//   into its parent
foreach ($questions as $k => &$v) {
  if ($v['parentId'] != 0) {
    $questions[$v['parentId']]['childs'][] =& $v;
  }
}
unset($v);

//  delete the childs comments from the top level
foreach ($questions as $k => $v) {
  if ($v['parentId'] != 0) {
    unset($questions[$k]);
  }
}

// now we display the questions list, this is a basic recursive function
function display_questions(array $questions, $level = 0) {
    $parentI = 1;
    $childI = 1;
  foreach ($questions as $q) : 
    //$answer = $this->QuestionnareModel->getAnswer($q['id'], $userId);
				?>
				<tr class="question-<?php echo $q['id']?>">
					<td><?php echo $q['question'] ?></td>
					<td>
						<?php if ($q['type'] == 2) :
							$checkedYes = FALSE;
							$checkedNo = FALSE;
							$checkedUnsure = FALSE;
							if (isset($q['answer'])) {
								if ($q['answer'] == "Yes") {
									$checkedYes = TRUE;
								} elseif ($q['answer'] == "No") {
									$checkedNo = TRUE;
								} elseif ($q['answer'] == "Unsure") {
									$checkedUnsure = TRUE;
								}
							} ?>
							<div class="form-check form-check-inline">
								<?php echo form_radio('answer-' . $q['id'] . '[0]', 'Yes', $checkedYes, array('class' => 'form-check-input', 'id' => 'inlineRadio1', 'data-id' => $q['id'])); ?>
								<label class="form-check-label" for="inlineRadio1">Ja</label>
							</div>
							<div class="form-check form-check-inline">
								<?php echo form_radio('answer-' . $q['id'] . '[0]', 'No', $checkedNo, array('class' => 'form-check-input', 'id' => 'inlineRadio2', 'data-id' => $q['id'])); ?>
								<label class="form-check-label" for="inlineRadio2">Nein</label>
							</div>
							<div class="form-check form-check-inline">
								<?php echo form_radio('answer-' . $q['id'] . '[0]', 'Unsure', $checkedUnsure, array('class' => 'form-check-input', 'id' => 'inlineRadio2', 'data-id' => $q['id'])); ?>
								<label class="form-check-label" for="inlineRadio2">Unsicher</label>
							</div>
						<?php else :
						echo form_input('answer-' . $q['id'] . '[0]', $q['answer'], array('class' => 'form-control'));
					endif; ?>
					</td>
					<input name="answer-<?php echo $q['id']; ?>[1]" class="form-control" type="hidden" value="<?php echo $q['id'] ?>" />
				</tr>
<?php
    //echo str_repeat('-', $level + 1).' comment '.$info['id']."<br>";
    if (!empty($q['childs'])) {
        display_questions($q['childs'], $level + 1);
    }
   
endforeach;

}

display_questions($questions);?>
	</tbody>
</table>
<?php echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success")); ?>
</form>
<script>
	$(document).on('change', 'input:radio[class="form-check-input"]', function() {

		if ($(this).is(':checked')) {
			var id = $(this).data('id')
			$('tr[data-parentId="' + id + '"]').remove();
			if ($(this).val() == 'Yes' || $(this).val() == 'No') {
				$.ajax({
					url: "<?php echo base_url('questionnare/subquestionAjax') ?>/" + id + "/" + $(this).val(),
					type: 'get',
					success: function(response) {
						console.log(response)
						if (response) {
							$(".question-" + id).after(response)
						}
					}
				})
			}
		}
	})
</script>
