<?php 
$disabled = "";
if(isset($practice_accounted_terminated->is_deleted))
{
	if($practice_accounted_terminated->is_deleted == 1)
	{
		$disabled = "disabled";
		echo "<h3 style='color:red; margin-top:30px'>Note: Your Company is Terminated</h3>";	
	}
}

?>
<h2 class="mt-5"><?php echo $this->lang->line('choose_one_topic')?></h2>
<table class="table">
  <thead>
    <tr>
      	<th scope="col">#</th>
      	<th scope="col"><?php echo $this->lang->line('topic')?></th>
		<th scope="col"><?php echo $this->lang->line('progress')?></th>
    </tr>
  </thead>
  <tbody>
<style type="text/css">
	.isDisabled {
  color: currentColor;
  cursor: not-allowed;
  opacity: 0.5;
  text-decoration: none;
}
</style>
  	<?php
  	$disabled = "";
	if(isset($practice_accounted_terminated->is_deleted))
	{
		if($practice_accounted_terminated->is_deleted == 1)
		{
			$disabled = "disabled class='isDisabled' onclick='return false'";
		}
	}
	$userId = $this->user->id;
  	$i = 1;
		foreach($topics as $topic) :
		$questionsByTopic = $this->QuestionnareModel->getQuestions($topic->id);
		$answeredN = 0;
		foreach ($questionsByTopic as $qt) {
			$answer = $this->QuestionnareModel->getAnswer($qt->id, $userId);
			if(isset($answer)) {
				 $answeredN++;
			}
		}
	$answeredQuestions = $this->QuestionnareModel->answeredQuestionsNumber($topic->id, $userId);
  	$questionsNumber = $this->QuestionnareModel->numberOfQuestions($topic->id);
  	?>
  	 <?php
	     $inc = $topic->is_deleted_by; 
	     $cs = !empty($inc)?explode(",",$inc):array();
	     $user = $this->ion_auth->user()->row();
	     $user_id = $user->id;
        if(!in_array($practice_id,$cs))
        {
        	 $new ='<span style=" padding-bottom:3px;color: white; border-radius: 5px;background-color:#0000ff94;">&nbsp;new&nbsp;</span>';
	          $viewed = $topic->is_viewed_by; 
	          $obov = !empty($viewed)?explode(",",$viewed):array();
	          if(in_array($user_id,$obov))
	          {
	            $new = "";
	          }
          ?>
		    <tr>
		      	<th scope="row"><?php echo $i ?></th>
		      	<td>
				<a  <?php echo $disabled; ?> href="<?php echo base_url().'questionnare/topic/'.$topic->topic_id; ?>"><?php echo $topic->topicName ?></a>
					<?php 
					echo $new;
					foreach ($notifications as $n) {
						if($topic->id == $n->topicId) {
							echo '<span class="badge badge-success">'.$this->lang->line("new").'</span>';
							break;
						}			
					}
					$unansweredQuestions = $questionsNumber - $answeredN;
					?></td>
				<td><?php echo $answeredN. ' / '. $unansweredQuestions?></td>
		    </tr>
    <?php
		}
    $i++;
    endforeach; ?>
  </tbody>
</table>

<script type="text/javascript">
	function disableLink(link) {
	link.addEventListener('click', function (event) {
  if (this.parentElement.classList.contains('isDisabled')) {
    event.preventDefault();
  }
});
}
</script>