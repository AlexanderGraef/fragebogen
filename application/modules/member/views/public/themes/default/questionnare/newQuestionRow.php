<?php if($q): 
	foreach($q as $que):
	?>
        <tr data-parentId="<?php echo $que->parentId?>" class="question-<?php echo $que->id?>">
          <td><?php echo $que->question ?></td>
          <td>
          <?php if($que->type == 2):
          	$checkedYes = FALSE;
          	$checkedNo = FALSE;
          	$checkedUnsure = FALSE;
        ?>
			  <div class="form-check form-check-inline">
				<?php echo form_radio('answer-'.$que->id.'[0]', 'Yes', $checkedYes, array('class' => 'form-check-input', 'id' => 'inlineRadio1', 'data-id' => $que->id)); ?>
				<label class="form-check-label" for="inlineRadio1">Ja</label>
			  </div>
			  <div class="form-check form-check-inline">
				<?php echo form_radio('answer-'.$que->id.'[0]', 'No', $checkedNo, array('class' => 'form-check-input', 'id' => 'inlineRadio2', 'data-id' => $que->id)); ?>
				<label class="form-check-label" for="inlineRadio2">Nein</label>
			  </div>
			  <div class="form-check form-check-inline">
				  <?php echo form_radio('answer-'.$que->id.'[0]', 'Unsure', $checkedUnsure, array('class' => 'form-check-input', 'id' => 'inlineRadio2')); ?>
				  <label class="form-check-label" for="inlineRadio2">Unsicher</label>
			  </div>
          <?php else:
          echo form_input('answer-'.$que->id.'[0]', '', array('class' => 'form-control'));
          endif; ?>
          </td>
          <input name="answer-<?php echo $que->id;?>[1]" class="form-control" type="hidden" value="<?php echo $que->id ?>"/>
        </tr>
				<?php 
			endforeach;
			endif; ?>