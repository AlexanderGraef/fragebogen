<?php 
$disabled = "";
if(isset($practice_accounted_terminated->is_deleted))
{
	if($practice_accounted_terminated->is_deleted == 1)
	{
		$disabled = "disabled";
		echo "<h3 style='color:red; margin-top:30px'>Note: Your Company is Terminated</h3>";	
		echo "<h2 class='mt-5' style='color:gray; margin-top:30px'>".$company['companyName']."</h2>";	
	}
}

?>
<?php echo validation_errors('<div class="error">'); ?>
<?php echo form_open(); ?>
	<div class="row mt-5">
    <div class="col-sm-5">
		<div class="form-group">
			     <?php 
				
				echo form_label($this->lang->line('company_name'), "companyName");
				echo form_input("companyName", $company['companyName'], array("class" => "form-control","required"=>1,$disabled=>""));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
    	
		<div class="form-group">
			<label class="label form_label">
					Email
			</label>
			<input type="email" name="email" class="form-control" value="<?php echo $this->user->email; ?>" <?php echo $disabled; ?>>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_street'), "companyStreet");
				echo form_input("companyStreet", $company['companyStreet'], array("class" => "form-control",$disabled=>""));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_house_number'), "companyHouseNumber");
				echo form_input("companyHouseNumber", $company['companyHouseNumber'], array("class" => "form-control",$disabled=>""));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_zip'), "companyZip");
				echo form_input("companyZip", $company['companyZip'], array("class" => "form-control",$disabled=>""));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_city'), "companyCity");
				echo form_input("companyCity", $company['companyCity'], array("class" => "form-control",$disabled=>""));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_phone'), "companyPhone");
				echo form_input("companyPhone", $company['companyPhone'], array("class" => "form-control",$disabled=>""));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_website'), "companyWebsite");
				echo form_input("companyWebsite", $company['companyWebsite'], array("class" => "form-control",$disabled=>""));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php
				echo form_label($this->lang->line('latest_number_of_blue_collars'), "numberOfBlueCollar");
				echo form_input("numberOfBlueCollar", $company['name'], array("class" => "form-control", "id" => "numberOfBlueCollar",$disabled=>""));
			?>
			<!-- <input class="blueCollar" type="hidden" name="numberOfBlueCollar" value="" /> -->
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php

				echo form_label($this->lang->line('latest_number_of_white_collars'), "numberOfWhiteCollar");
				echo form_input("numberOfWhiteCollar", $company['whiteCollarName'], array("class" => "form-control", "id" => "numberOfWhiteCollar",$disabled=>""));
			?>
			<!-- <input class="whiteCollar" type="hidden" name="numberOfWhiteCollar" value="" /> -->
	  	</div>
    </div>
        <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('safety_person_name'), "safetyPersonName");
				echo form_input("safetyPersonName", $company['safetyPersonName'], array("class" => "form-control",$disabled=>""));
			?>
	  	</div>
    </div>
     <?php 
if($member_parent <= 0)
{
    ?>
    <div class="col-sm-5">
		<div class="form-group">
			<?php
            $options = array();
            foreach($practices as $practice) {
                $options[$practice->user_id] = $practice->first_name.' '.$practice->last_name;
            }
				echo form_label($this->lang->line('practice_accounts'), "practiceaccounts");
				echo form_dropdown("practiceaccount", $options, '', array("class" => "form-control",$disabled=>""));
			?>
	  	</div>
    </div>
    <?php 
 } ?>
    <div class="col-sm-5">
		
	  	  	<div class="form-group">
			<label for="numberOfBg"><?php echo $this->lang->line('number_of_bg'); ?></label>
			<select name="numberOfBg" required class="form-control" <?php echo $disabled; ?>>
				<?php 
				foreach($bgs as $bg) 
				{
					$checked ="";
					if($bg->id == $company['numberOfBg'])
					{
						$checked = "selected";
					}
                	echo '<option '.$checked.' value="'.$bg->id.'">'.$bg->bg.'</option>';
                	$checked = "";
            	}
				?>
				
				
			</select>
	  	</div>
    </div>

    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('safety_person_email'), "safetyPersonEmail");
				echo form_input("safetyPersonEmail", $company['safetyPersonEmail'], array("class" => "form-control",$disabled=>""));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('safety_person_phone'), "safetyPersonPhone");
				echo form_input("safetyPersonPhone", $company['safetyPersonPhone'], array("class" => "form-control",$disabled=>""));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('direct_adresse_for_osha_names'), "directAdresseeForOshaNames");
				echo form_input("directAdresseeForOshaNames", $company['directAdresseeForOshaNames'], array("class" => "form-control",$disabled=>""));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('direct_adresse_for_osha_email'), "directAdresseeForOshaEmail");
				echo form_input("directAdresseeForOshaEmail", $company['directAdresseeForOshaEmail'], array("class" => "form-control",$disabled=>""));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('direct_adresse_for_osha_number'), "directAdresseeForOshaNumber");
				echo form_input("directAdresseeForOshaNumber", $company['directAdresseeForOshaNumber'], array("class" => "form-control",$disabled=>""));
			?>
	  	</div>
    </div>
         <?php 
         $user_practice = !empty($user_practice[0]->practiceaccount_id)?$user_practice[0]->practiceaccount_id:0;
         $q = $this->db->get_where("users_tokens",array("user_id"=>$user_practice));
if($member_parent > 0 &&  $q->num_rows()>0)
{
    ?>
    <div class="col-sm-5">
    	
		<div class="form-group">
			<label class="label form_label">
					Extra Details
			</label>
			<textarea name="extra_text" class="form-control" <?php echo $disabled; ?> ><?php echo $company['extra_text']; ?></textarea>
	  	</div>
    </div>
    <div class="col-sm-5">
    	
		<div class="form-group">
			<label class="label form_label">
					Invoice Email
			</label>
			<input type="email" name="invoice_email" class="form-control" value="<?php echo $invoice_email; ?>" <?php echo $disabled; ?>>
	  	</div>
    </div>
    <div class="col-sm-5">
    	<label></label>
	    	<div class="row" style="margin-top: 10px">
	    		<div class="col-sm-2">
				<div class="form-group">
			    	<input type="checkbox"  id="checkbox_id" name="receive_invoices" class="form-control" value="<?php echo $receive_invoices; ?>" <?php  echo ($receive_invoices == 1 || $receive_invoices == NULL ) ?'checked ':' '; echo $disabled; ?>>
			    </div>
	    	</div>
			<div class="col-sm-8">
				<div class="form-group">    	
					<label class="label form_label" for="checkbox_id">
							Want to receive invoices in future.

					</label>
				</div>
			</div>
    	</div>
    </div>
    <?php }

     ?>
   
    <div class="col-sm-5">
		<div class="form-group">
			<?php
				echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success",$disabled=>""));
			?>
	  	</div>
    </div>
	</div>
</form>
<h4 class="mt-5"><?php echo $this->lang->line('number_of_blue_collars_in_past_time')?></h4>
<table class="table">
	<thead>
	<tr>
		<th scope="col"><?php echo $this->lang->line('number_of_blue_collar')?></th>
		<th scope="col"><?php echo $this->lang->line('date_added')?></th>
	</tr>
	</thead>
	<tbody>
<?php
foreach ($blueCollars as $bCollar) : ?>
	<tr class="roww">
		<td><?php echo $bCollar->name ?></td>
		<td><?php echo $bCollar->year ?></td>
		<?php if($this->uri->segment(1) == 'admin'): ?>
		<td>
				<a class="btn btn-danger btn-sm" id="deleteBCollar" data-id="<?php echo $bCollar->id ?>" href="#">Delete</a>
		</td>
			<?php endif; ?>
	</tr>
<?php endforeach; ?>
	</tbody>
</table>
<h4 class="mt-5"><?php echo $this->lang->line('number_of_white_collars_in_past_time')?></h4>
<table class="table">
	<thead>
	<tr>
		<th scope="col"><?php echo $this->lang->line('number_of_white_collar')?></th>
		<th scope="col"><?php echo $this->lang->line('date_added')?></th>
	</tr>
	</thead>
	<tbody>
	<?php
	foreach ($whiteCollars as $wCollar) : ?>
		<tr class="roww">
			<td><?php echo $wCollar->whiteCollarName ?></td>
			<td><?php echo $wCollar->whiteCollarYear ?></td>
			<?php if($this->uri->segment(1) == 'admin'): ?>
		<td>
				<a class="btn btn-danger btn-sm" id="deleteWCollar" data-id="<?php echo $wCollar->id ?>" href="#">Delete</a>
		</td>
			<?php endif; ?>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<!-- Modal -->
<div class="modal fade" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="noticeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="noticeModalLabel">Notice</h5>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Do you have more than 20 whole time employees?
      </div>
      <div class="modal-footer">
        <a href="<?php echo base_url('member/company/noticeChecked/'.$company["companyId"]).'/2'?>" class="btn btn-secondary">No</a>
        <a href="<?php echo base_url('member/company/noticeChecked/'.$company["companyId"]).'/1'?>" class="btn btn-primary checkNotice">Yes</a>
      </div>
    </div>
  </div>
</div>
<!-- <?php if($showModal):?>
    <script> 
        $('#noticeModal').modal('show')
    </script>
    <?php $this->session->set_userdata('first_time', 'shown');?>
<?php endif;?> -->
<script>

		// $("#numberOfBlueCollar").keyup(function() {
		// 	var bcVal = $("#numberOfBlueCollar").val();
		// 	$(".blueCollar").val(bcVal);
		// })
		// $("#numberOfWhiteCollar").keyup(function() {
		// 	var wcVal = $("#numberOfWhiteCollar").val();
		// 	$(".whiteCollar").val(wcVal);
		// })
        

</script>
