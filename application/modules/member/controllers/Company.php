<?php 
class Company extends Member_Controller {
    private $showModal;
    public $is_admin;
    public $is_practice;
    public $is_doctor;
	function __construct()
	{	

		parent::__construct();
        $this->referred_from = $this->session->userdata('referred_from');
        $userId = $this->user->id;
        $company = $this->CompanyModel->getCompany($userId);
        $this->showNotice = $this->session->userdata('showNotice');
    }

	public function index() {
        $this->is_admin = $this->ion_auth->is_admin();
        if($this->is_admin)
        {
            redirect('/admin', 'refresh');  
        }
        $this->is_practice = $this->ion_auth->is_practice();
        if($this->is_practice)
        {
            redirect('/practice', 'refresh');  
        }
        $this->is_doctor = $this->ion_auth->is_doctor();
        if($this->is_doctor)
        {
            redirect('/doctor', 'refresh');  
        }
        else
        {
            $total_influenza = isset($this->InfluenzaModel->influenza()->send_date)?$this->InfluenzaModel->influenza():array();
    		$this->session->set_userdata('referred_from', current_url());
    		$this->load->library('form_validation');
            $userId = $this->user->id;
    		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('companyName', 'companyName', 'required');
            $member_parent_arr = $this->AdminModel->members_parent($userId);
            $practice_accounted_terminated = $this->AdminModel->practice_accounted_terminated($userId);
            $member_parent = count($member_parent_arr);
            $practices = "";
            if($member_parent<=0)
            {
        		$practices = $this->AdminModel->getpractices();        
            }
            
            $company = $this->CompanyModel->getCompany($userId);
            $invoice_settings = $this->db->get_where("invoice_preferences",array("member_id"=>$userId))->row();
          $invoice_email = !empty($invoice_settings->invoice_email)?$invoice_settings->invoice_email:'';
          $receive_invoices = isset($invoice_settings->receive_invoices)?$invoice_settings->receive_invoices:NULL;
    		$data = array(
                "showModal" => $this->showNotice,
    			"company" => $company,
    			"blueCollars" => $this->CompanyModel->getNumberOfBlueCollars($company['companyId']),
    			"whiteCollars" => $this->CompanyModel->getNumberOfWhiteCollars($company['companyId']),
    			"bgs" => $this->AdminModel->getBgs(),
                "member_parent" => $member_parent,
                "practices" => $practices,
                "total_influenza"=>$total_influenza,
                "invoice_email"=>$invoice_email,
                "receive_invoices"=>$receive_invoices,
                "user_practice"=>$member_parent_arr,
                "practice_accounted_terminated"=>$practice_accounted_terminated
            );

            $data['page'] = $this->config->item('ci_my_admin_template_dir_public') . "company";
    		if ($this->form_validation->run() === FALSE)
                {   
                    $this->load->view($this->_container, $data);                    
                }
            else
                {
                    if($company['name'] != $this->input->post('numberOfBlueCollar') 
                    || $company['whiteCollarName'] != $this->input->post('numberOfWhiteCollar')) {
                        $workerNumber = $this->input->post('numberOfBlueCollar')  + $this->input->post('numberOfWhiteCollar');
                        
                         if($workerNumber >= 20 && $workerNumber <= 30) {
                            $this->session->set_userdata('showNotice', true);
                           // $this->noticeChecked($company['companyId'], 1);
                        }
                      
                    }
                    $this->CompanyModel->updateCompany($userId);
                    $this->session->set_flashdata('msg', $this->lang->line('company_update_success_msg'));
                    redirect('/', 'refresh');                
                }
        }
       
    }
    function getpracticeBgs()
    {
       echo json_encode($this->CompanyModel->getpracticeBgs($_POST['id']));
    }
    function noticeChecked($companyId, $check) {
        $this->CompanyModel->noticeCheck($companyId, $check);
        $this->session->set_userdata('showNotice', false);
        //redirect($this->referred_from, 'refresh'); 
        redirect('/', 'refresh'); 
    }
}
