<?php 
class user extends Member_Controller {

	function __construct()
	{	
		parent::__construct();
        $this->referred_from = $this->session->userdata('referred_from');
    }

    function changeLanguage($language) {
        $this->session->set_userdata('language', $language);
        redirect(base_url(), 'refresh'); 
    }

    function changeLanguageAdmin($language) {
        $this->session->set_userdata('language', $language);
        redirect(base_url('admin'), 'refresh'); 
    } 
    function changeLanguagePractice($language) {
        $this->session->set_userdata('language', $language);
        redirect(base_url('practice'), 'refresh'); 
    }
    function changeLanguageDoctor($language) {
        $this->session->set_userdata('language', $language);
        redirect(base_url('doctor'), 'refresh'); 
    }
}
