<?php

class Jobs extends Member_Controller {

	function __construct()
	{

		parent::__construct();
		$this->logged();

	}

	function index() {
		$data = array(
			'jobs' => $this->JobsModel->getJobs()
		);
		$this->load->view('jobs/index', $data);
	}

	function addJob() {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('jobName', 'Topic Name', 'required');
		$data = array(
			'hazardGroups' => $this->AdminModel->getHazardGroups()
		);
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view("jobs/addJob", $data);
		}
		else
		{
			$this->JobsModel->addJob();
			$jobId = $this->db->insert_id();
			foreach($this->input->post('hazardId') as $hazard) {
				$this->JobsModel->jobsHazards($jobId, $hazard);
			}
			$this->session->set_flashdata('msg', 'Job has been created');
			redirect('jobs');
		}
	}

	function hazardsAjax($id) {
		$data = $this->JobsModel->getHazardsByGroup($id);
		echo json_encode($data);
	}

	function addOccupation() {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('occupationName', 'Occupation Name', 'required');
		$data = array(
			'jobs' => $this->JobsModel->getJobs()
		);
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view("jobs/addOccupation", $data);
		}
		else
		{
			$this->JobsModel->addOccupation();
			$occupationId = $this->db->insert_id();
			foreach($this->input->post('jobId') as $job) {
				$this->JobsModel->jobsOccupations($occupationId, $job);
			}
			$this->session->set_flashdata('msg', 'Occupation has been created');
			redirect('jobs/occupations');
		}
	}

	function occupations() {
		$data = array(
			'occupations' => $this->JobsModel->getOccupations()
		);
		$this->load->view('jobs/occupations', $data);
	}

	function addWorker() {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('firstName', 'First Name', 'required');
		$data = array(
			'occupations' => $this->JobsModel->getOccupations()
		);
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view("jobs/addWorker", $data);
		}
		else
		{
			$this->JobsModel->addWorker();

			$this->session->set_flashdata('msg', 'Worker has been created');
			redirect('jobs/workers');
		}
	}

	function workers() {
		$data = array(
			'workers' => $this->JobsModel->getWorkers()
		);
		$this->load->view('jobs/workers', $data);
	}

	function jobsOccupationsAjax($occupationId) {
		$data = $this->JobsModel->getJobsHazards($occupationId);
		echo json_encode($data);
	}

	protected function logged(){

		$is_logged_in = $this->session->userdata('logged_in');

		if(!isset($is_logged_in) || $is_logged_in != true)

		{
			redirect('login');
		}

	}
}
