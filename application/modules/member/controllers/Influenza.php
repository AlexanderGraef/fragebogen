<?php 
class Influenza extends Member_Controller {
    private $showModal;
    public $is_admin;
	function __construct()
	{	

		parent::__construct();
        $this->referred_from = $this->session->userdata('referred_from');
        $userId = $this->user->id;
        $company = $this->CompanyModel->getCompany($userId);
        $this->showNotice = $this->session->userdata('showNotice');
    }
public function index() 
    {
        $userId = $this->user->id;
        $this->is_admin = $this->ion_auth->is_admin();
        if($this->is_admin)
        {
            redirect('/admin', 'refresh');  
        }
        else
        {
            $total_influenza = isset($this->InfluenzaModel->influenza()->send_date)?$this->InfluenzaModel->influenza():array();
            $practice_accounted_terminated = $this->AdminModel->practice_accounted_terminated($userId);
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('influenza_response', 'influenza_response', 'required');
            $this->form_validation->set_rules('influenza_id', 'influenza_id', 'required');
            $this->session->set_userdata('referred_from', current_url());
            $data = array(
                'influenza' => $this->InfluenzaModel->influenza(),
                "total_influenza"=>$total_influenza,
                "practice_accounted_terminated"=>$practice_accounted_terminated

            );

            $data['page'] = $this->config->item('ci_my_admin_template_dir_public') . "influenza/influenza";
            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view($this->_container, $data);
            }
            else
            {
                $msg = $this->InfluenzaModel->respond_influenza();
                $this->session->set_flashdata('msg', $msg);
                redirect(base_url());
            }
        }
    }
}
