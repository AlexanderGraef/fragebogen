<?php
class Questionnare extends Member_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index() {
		$userId = $this->user->id;
		$practice_id = $this->AdminModel->members_parent($userId);
		$practice_id = isset($practice_id[0])?$practice_id[0]->practiceaccount_id:0;
		$practice_accounted_terminated = $this->AdminModel->practice_accounted_terminated($userId);
		$data = array(
			'topics' => $this->QuestionnareModel->getTopics($practice_id),
			'notifications' => $this->QuestionnareModel->getNotifs($userId),
             "practice_id"=>$practice_id,
           	"practice_accounted_terminated"=>$practice_accounted_terminated

		);
		$data['page'] = $this->config->item('ci_my_admin_template_dir_public') . "questionnare/topics";
		$this->load->view($this->_container, $data);
	}

	function topic($id) {
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->select('t.*');
		$this->db->from("topics as t");
		$this->db->where("FIND_IN_SET(".$user_id.",t.is_viewed_by) and t.id = ".$id);
		$q = $this->db->get();
		if($q->num_rows()<=0)
		{
			$res = $this->db->get_where("topics",array("id"=>$id))->row();
			if($res->is_viewed_by != NULL)
			{
				$this->db->set("is_viewed_by","CONCAT(is_viewed_by,',',".$user_id.")",FALSE);
				$this->db->where(array("id"=>$id));
				$this->db->update("topics");
			}
			else
			{
				$this->db->set("is_viewed_by",$user_id);
				$this->db->where(array("id"=>$id));
				$this->db->update("topics");
			}
		}
		$userId = $this->user->id;
		$practice_id = $this->AdminModel->members_parent($userId);
		$practice_id = isset($practice_id[0])?$practice_id[0]->practiceaccount_id:0;
        $this->QuestionnareModel->deleteNotif($userId, $id);
        $company = $this->CompanyModel->getCompany($userId);
		$data = array(
            'userCompany' => $company,
			'questions' => $this->QuestionnareModel->getQuestions($id),
			'topic' => $this->AdminModel->topic($id),
                "practice_id"=>$practice_id
		);
		$data['page'] = $this->config->item('ci_my_admin_template_dir_public') . "questionnare/questions";
		if (!$this->input->post())
            {
                $this->load->view($this->_container, $data);
            }
        else
            {
				foreach($this->input->post() as $i => $post) {
					if(isset($post[0]) && isset($post[0])) {
						if($post[0] != "S" && $post[0] != "") {
							$this->QuestionnareModel->setAnswers($userId, $post[0], $post[1]);
						}
					}		
					
				}
				$this->session->set_flashdata('msg', $this->lang->line('question_update_success_msg'));
                redirect('questionnare');
            }

	}

	function subquestionAjax($id, $qOption) {
		$data['q'] = $this->QuestionnareModel->getQuestionByParentId($id, $qOption);
		$this->load->view($this->config->item('ci_my_admin_template_dir_public') . "questionnare/newquestionRow", $data);
		//echo json_encode($data);
    }
    
    function questionNoticeAjax($id, $qOption) {
		$data = $this->QuestionnareModel->getQuestionNotice($id, $qOption);
		echo json_encode($data);
    }
}
