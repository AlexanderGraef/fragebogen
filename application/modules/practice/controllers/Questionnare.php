<?php

class Questionnare extends Practice_Controller {
    function __construct() {
        parent::__construct();
		$this->referred_from = $this->session->userdata('referred_from');
		$group = 'practice';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('practice');
        }
    }

    public function index() {
        $this->session->set_userdata('referred_from', current_url());
		$data = array(
			'topics' => $this->QuestionnareModel->getTopics()
		); 
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "questionnare/topics";
        $this->load->view($this->_container, $data);
    }

    function topic($topicId) {

		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->select('t.*');
		$this->db->from("topics as t");
		$this->db->where("FIND_IN_SET(".$user_id.",t.is_viewed_by) and t.id = ".$topicId);
		$q = $this->db->get();
		if($q->num_rows()<=0)
		{
			$res = $this->db->get_where("topics",array("id"=>$topicId))->row();
			if($res->is_viewed_by != NULL)
			{
				$this->db->set("is_viewed_by","CONCAT(is_viewed_by,',',".$user_id.")",FALSE);
				$this->db->where(array("id"=>$topicId));
				$this->db->update("topics");
			}
			else
			{
				$this->db->set("is_viewed_by",$user_id);
				$this->db->where(array("id"=>$topicId));
				$this->db->update("topics");
			}
		}

		$this->session->set_userdata('referred_from', current_url());
		$data = array(
			'result' => $this->QuestionnareModel->getQuestions($topicId),
			'topics' => $this->QuestionnareModel->getTopics(),
			'topic' => $this->QuestionnareModel->getTopic($topicId)
		);
		$data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "questionnare/questions";
        $this->load->view($this->_container, $data);
    }

    function addTopic() {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('topicName', 'Topic Name', 'required');
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "questionnare/add_topic";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->QuestionnareModel->addTopic();
			$this->session->set_flashdata('msg', 'Thema wurde erstellt.');
			if($this->input->post('submit')) {
				redirect('practice/questionnare/topic/add', 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
			
		}
    }
    
    function editTopic($id) {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('topicName', 'Topic Name', 'required');
		$data = array(
			'topic' => $this->QuestionnareModel->getTopic($id)
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "questionnare/edit_topic";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->QuestionnareModel->editTopic($id);
			$this->session->set_flashdata('msg', 'Thema wurde aktualisiert');
			if($this->input->post('submit')) {
				redirect('practice/questionnare/topic/edit/'.$id);
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }
    
    function deleteTopic($id) {
		$this->PracticeModel->deleteTopic($id);
		$this->session->set_flashdata('msg', 'Thema wurde gel�scht.');
		redirect('practice/questionnare');
    }
    
    function deleteQuestion($id) {
		$this->PracticeModel->deleteQuestion($id);
		$this->session->set_flashdata('msg', 'Frage wurde gel�scht.');
		redirect('practice/topics');
    }
    
    function addQuestion() {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('questionName', 'Question Name', 'required');
		$data = array(
			'topics' => $this->QuestionnareModel->getTopics()
		);
        $users = $this->PracticeModel->getUsers();
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "questionnare/add_question";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
            $allQuestionsSort = $this->PracticeModel->getQuestionsSortIndexByTopic($this->input->post('topicId'));
            $sortI = array();
            foreach($allQuestionsSort as $sort => $val) {
                $sortI[] = (int) $val['sortIndex'];
            }
            $sortIndex = 1; 
            if(count($allQuestionsSort) > 0) {
                $sortIndex = max($sortI) + 1;
            }
            
            
			$this->PracticeModel->addQuestion($sortIndex);
			$id = $this->db->insert_id();
			$questionId = $this->db->insert_id();
			if($this->input->post('parentId') == 0) {
				foreach($users as $user) {
					$this->PracticeModel->addNotif($user->id, $questionId);
				}
			}
			$this->session->set_flashdata('msg', 'Frage wurde erstellt.');
			if($this->input->post('submit')) {
				redirect('practice/questionnare/questions/add', 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }
    
    function editQuestion($id) {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('questionName', 'Question Name', 'required');
		$data = array(
			'topics' => $this->QuestionnareModel->getTopics(),
			'question' => $this->PracticeModel->getQuestion($id)
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "questionnare/edit_question";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->PracticeModel->editQuestion($id);
			$this->session->set_flashdata('msg', 'Frage wurde aktualisiert.');
			if($this->input->post('submit')) {
				redirect('practice/questionnare/questions/edit/'.$id, 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }
    
    function questionsAjax($id) {
		$data = $this->PracticeModel->getQuestionsByTopic($id);
		echo json_encode($data);
    }

    function subquestionAjax($id, $qOption) {
		$data['q'] = $this->QuestionnareModel->getQuestionByParentId($id, $qOption);
		$this->load->view('questionnare/newQuestionRow', $data);
		//echo json_encode($data);
	}
    
    function updateQuestionsOrder() {
		$position = $_POST['position'];

		$i=1;
		foreach($position as $k=>$v){
			$this->PracticeModel->updateQuestionsOrder($i, $v);
			$i++;
		}
    }
    
    function moveToTopic() {
		$topicId = $this->input->post('topicId');
		$questionId = $this->input->post('questionId');
		$this->PracticeModel->changeQuestionTopic($topicId, $questionId);
    }
    
    function sendNewQuestionsEmail() {
		$this->load->library('email');
		$notifs = $this->PracticeModel->getNotifs();
		$users = array();
		foreach($notifs as $not) {
			if (!in_array($not->userId, $users)) {
				$questions = $this->PracticeModel->getNewQuestionsByUserId($not->userId);
				
				$config=array(
					'charset'=>'utf-8',
					'wordwrap'=> TRUE,
					'mailtype' => 'html'
					);
					
				$this->email->initialize($config);
				if(count((array)$questions) > 0) {
					$this->email->from($this->config->item('email_from'), $this->config->item('name_from'));
					$this->email->to($not->username);
					$this->email->subject($this->lang->line('new_questions_email_subject'));
					$data['questions'] = $questions;
					/* $msg = "Hier sind die noch nicht beantworteten Fragen:<br><br>";
					foreach ($questions as $question) {
						$msg .= "<br>";
						$msg .= $question->question;
					} */
					$msg = $this->load->view('emails/newQuestions', $data, TRUE);
					$this->email->message($msg);
	
					$this->email->send();
				}

				array_push($users, $not->userId);
			}

		}
		$this->session->set_flashdata('msg', 'Email wurde verschickt.');
		redirect('practice/questionnare', 'refresh');
	}
}