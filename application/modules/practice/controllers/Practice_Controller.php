<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
class Practice_Controller extends MY_Controller {
    public $is_practice;
    public $logged_in_name;

    function __construct() {
        parent::__construct();
       
        // Set container variable
        $this->_container = $this->config->item('ci_my_admin_template_dir_practice') . "layout.php";
        $this->_modules = $this->config->item('modules_locations');

        $this->load->library(array('ion_auth'));

        if (!$this->ion_auth->logged_in()) {
            redirect('/auth', 'refresh');
        }

        $this->is_practice = $this->ion_auth->is_practice();
        $user = $this->ion_auth->user()->row();
        $this->logged_in_name = $user->first_name;

        log_message('debug', 'CI My Practice : Practice_Controller class loaded');
    }
}