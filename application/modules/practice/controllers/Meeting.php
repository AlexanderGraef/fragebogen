<?php

class Meeting extends Practice_Controller {
    function __construct() {
        parent::__construct();

        $group = 'practice';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('practice');
        }
    }

 public function index() {    
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $data['companies'] = $this->PracticeModel->companies($userId);
        if($this->input->post('company_id') != "" && $this->input->post('start_date') != "" && $this->input->post('meeting_duration') != "")
        {
            $this->PracticeModel->add_meeting_schedule();
        }
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "meetings/start_meeting";
        $this->load->view($this->_container, $data);
    }
  function meetings() {    
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $data['meetings'] = $this->PracticeModel->getmeetings();
        
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "meetings/meetings";
        $this->load->view($this->_container, $data);
    }

  function visits() {    
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $data['visits'] = $this->PracticeModel->getvisits();
        
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "visits/visits";
        $this->load->view($this->_container, $data);
    }

  function add_company_topic() {    
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $data['companies'] = $this->PracticeModel->companies($userId);
        $data['topics'] = $this->PracticeModel->getmeetingtopic();
        $data['bgs'] = $this->PracticeModel->getBgs();
        if($this->input->post('meeting_topic') != '' || $this->input->post('meeting_topics') != '')
        {
            $this->PracticeModel->addMeetingTopic();
        }
       
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "meetings/add_company_topic";
        $this->load->view($this->_container, $data);
    }
  function new_visit() {
        if($this->input->post('visit_name') != '')
        {
            $this->PracticeModel->addvisit();
        }
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $data['companies'] = $this->PracticeModel->companies($userId);
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "visits/new_visit";
        $this->load->view($this->_container, $data);
    }
    function ajax_company_topics($company_id)
    {
        $data['topics'] = $this->PracticeModel->get_company_topics($company_id);
        echo json_encode($data);
    }
    function ajax_company_non_topics($company_id)
    {
        $data['topics'] = $this->PracticeModel->get_company_non_topics($company_id);
        echo json_encode($data);
    }
    function ajax_delete_company_topics($topic_id)
    {
        return $this->PracticeModel->delete_company_topics($topic_id);
    }
}