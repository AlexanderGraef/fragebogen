<?php

class Dashboard extends Practice_Controller {
    function __construct() {
        parent::__construct();

        $group = 'practice';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('practice');
        }
    }

 public function index() {    
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $data['counts'] = array("companies"=> count($this->CompanyModel->getCompany($userId)), 'topics' => count($this->QuestionnareModel->getTopics()), "users"=>count($this->PracticeModel->admin_users()));
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "dashboard";
        $this->load->view($this->_container, $data);
    }
}