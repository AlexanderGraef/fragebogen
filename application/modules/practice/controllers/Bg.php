<?php

class Bg extends Practice_Controller {
    function __construct() {
		parent::__construct();
		
        $this->referred_from = $this->session->userdata('referred_from');
        // restrict practice from accessing bg
        $group = 'admin';
        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('practice');
        }
    }

    public function index() {    
        $this->session->set_userdata('referred_from', current_url());
		$data = array(
			'bgs' => $this->PracticeModel->getBgs()
		);
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "bg/list";
        $this->load->view($this->_container, $data);
    }

    public function add() {
        $this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('bg', 'Name der BG', 'required');
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "bg/add";
        
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->PracticeModel->addBg();
			$this->session->set_flashdata('msg', 'BG wurde hinzugef�gt.');
			if($this->input->post('submit')) {
				redirect('practice/bg/add', 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }

    function edit($id) {
        $this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('bg', 'BG', 'required');
    
		$data = array(
			'bg' => $this->PracticeModel->getBg($id)
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "bg/edit";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->PracticeModel->editBg($id);
			$this->session->set_flashdata('msg', 'BG Edited');
			if($this->input->post('submit')) {
				redirect('practice/bg/edit/'.$id, 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }

    function delete($id) {
		$this->db->where('id', $id);
		$this->db->delete('numberofbg');
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->where(array('practiceaccount_id'=> $user_id,"bg_id"=>$id));
		$this->db->delete('practicing_bgs');
	}
}