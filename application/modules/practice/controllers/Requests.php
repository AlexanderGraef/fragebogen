<?php

class Requests extends Practice_Controller {
    function __construct() {
		parent::__construct();
		$group = 'practice';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be a practicing admin to view the Company page.');
            redirect('practice');
        }
    }

    public function index() {    
        $this->session->set_userdata('referred_from', current_url());
        $data = array(
            'requests' => $this->PracticeModel->getrequests()
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "requests/list";
        $this->load->view($this->_container, $data);
    }

  
    function update_member_practice()
	{
		$data['request_id'] = $this->input->post('request_id');
		$data['action'] = $this->input->post('action');
		$data['user_id'] = $this->input->post('user_id');
		$this->PracticeModel->update_member_practice($data);
		redirect(base_url()."practice/requests", 'refresh');
	}
  
   
}