
<div class="card shadow mb-4" style="width: 350px; margin: auto; margin-top: 10%">



   <div class="card-body show">

      <form onsubmit="return check_creds()" action="<?php echo base_url().'admin/invoices/'; ?>" id="form_content" method="post">
        <div class="form-group">
            <div class="form_control" id="errormsg"></div>
        </div>
        <div class="form-group">
          <input type="text" value="<?php echo isset($token->token_url)?$token->token_url:''; ?>" name="url" style="font-size: 13px" placeholder="i.e: https://www.example.com/api/v1/" class="form-control">
          
        </div>
        <div class="form-group">
          <input type="text" value="<?php echo isset($token->token)?$token->token:''; ?>" name="token" style="font-size: 13px"  placeholder="Enter Token" class="form-control">
          <span style="font-size: 14px">Don't have a token ? <a target="_blank" href="https://app.invoiceninja.com">click here</a></span>
        </div>
        <div class="form-group">
          <input type="submit" class="btn-primary form-control" style="position: relative; width: auto; float:right" name="submit">
        </div>
      </form>
   
      </div>
      </div>
      <div style="width: 350px; margin: auto;">
        <ol  style="font-size: 12px">
      <li>
        URL will alike: "https://app.invoiceninja.com/api/v1/"
      </li>
      <li>
        Visit the link <a href="https://app.invoiceninja.com" target="_blank">https://app.invoiceninja.com</a>
      </li>
      <li>
        Sign up if you are new. or login to your invoice ninja account.
      </li>
      <li>
        Go to settings > API Tokens (Advance settings) > Add Token
      </li>
      <li>
        Copy the generated Token and paste up right there.
     </li>
      </ol>
      </div>
 <script type="text/javascript">
   function check_creds()
   {
    var flag = false;
      document.getElementById("errormsg").innerHTML="<img style='height:20px; float:left; padding-right:10px' src='<?php echo base_url().'assets/admin/img/ajax-loader-grey.gif' ?>'/><p style='font-size: 11px; padding-top:6px'>Please wait your token is being validated on url.</p>";
     $.ajax({
            url: 'ajax_check_token_creds',
            type: 'POST',
            data: $('#form_content').serialize(),
            success: function(data, textStatus, xhr) {
              if(data == 1)
              {
                document.getElementById("errormsg").innerHTML="<p style='font-size: 11px; padding-top:6px'><b style='color:green'>Status</b>: successfully token uploaded, Please wait being redirected.</p>";
                window.location.assign("<?php echo base_url().'practice/invoices/'; ?>");
              }
              else
              {
                document.getElementById("errormsg").innerHTML="<p style='font-size: 11px; padding-top:6px'><b style='color:red'>Error</b>: Recheck URL and Token</p>";
              }  
            },
            error: function (xhr, desc, err)
            {
               document.getElementById("errormsg").innerHTML="<p style='font-size: 11px; padding-top:6px'><b style='color:red'>Error</b>: Recheck URL and Token</p>";
            }
        });
     
      return false;
     
   }
 </script>