<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><?php echo lang('doctors');?></h1>
    </div>
<!-- DataTales Example -->
<!-- <div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo lang('index_subheading');?></h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?php echo lang('index_fname_th');?></th>
            <th><?php echo lang('index_lname_th');?></th>
            <th><?php echo lang('index_email_th');?></th>
            <th><?php echo lang('index_groups_th');?></th>
            <th><?php echo lang('index_status_th');?></th>
            <th><?php echo lang('index_action_th');?></th>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user):?>
		<tr>
            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
			<td>
				<?php foreach ($this->ion_auth->get_users_groups($user->user_id)->result() as $group):?>
          <?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8') ;?><br />
            <input type="hidden" value="<?php echo $group->id; ?>" name="group_id">
                <?php endforeach?>
			</td>
			<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->user_id, lang('index_active_link')) : anchor("auth/activate/". $user->user_id, lang('index_inactive_link'));?></td>
			<td><?php echo anchor("auth/edit_user/".$user->user_id, 'Edit') ;?></td>
		</tr>
	    <?php endforeach;?>
        </tbody>
      </table>
    </div>
  </div>
</div> -->

<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('doctors')?></h6>
  </div>
  <div class="card-body">
     
        <?php
    $i = 1;
    foreach($doctors as $doctor) : ?>
    <div class="row">
        
        <div class="col-md-3" >
          <div class="col-md-4">
            <p style="text-align: left; position: relative; border-left: 1px solid #d1d5da;padding: 50% 0; float: left; height: 100%"></p>
          <p style="border-top: 1px solid #d1d5da; width: 50%; height: 2px; position: absolute; margin-top: 33%"> </p>
          </div>
           <div class="col-md-8" style=" margin-top: 7%; float: right;">
            <p style="font-size: 14px; font-weight: bold;"><?php echo $doctor->first_name.' '.$doctor->last_name; ?></p>
        </div>
        </div>
       
          <!-- <p style="margin-top: 25px">
            -
          </p> -->
       <!--  <div  style="text-align: right; margin-top: 2%" class="col-md-1">
          <p style="font-size: 11px;"><?php echo $doctor->first_name.' '.$doctor->last_name; ?></p>           
        </div> -->
        <div class="col-md-6" style="margin-left: 10px; padding-top: 15px">
         
         <?php
          $companies = $this->PracticeModel->doctors_companies($doctor->user_id);
          foreach ($companies as $company)
          {
             echo "<button style='border:1px solid #00458075; font-size:10px; margin-right: 5px; margin-top: -5%;' disabled class='btn btn-default;'><i class='fas fa-plus'>  </i> ".$company->companyName." </button>";
          }
           
         ?>
        
        </div>
      
      
    </div>
      <?php 

    endforeach; ?>
       
  </div>
</div>
</div>