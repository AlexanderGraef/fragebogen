<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><?php echo lang('index_heading');?></h1>
    </div>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo lang('index_subheading');?></h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?php echo lang('index_fname_th');?></th>
            <th><?php echo lang('index_lname_th');?></th>
            <th><?php echo lang('index_email_th');?></th>
            <th><?php echo lang('index_groups_th');?></th>
            <th><?php echo lang('index_status_th');?></th>
            <th><?php echo lang('index_action_th');?></th>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user):?>
		<tr>
            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
			<td>
				<?php foreach ($this->ion_auth->get_users_groups($user->user_id)->result() as $group):?>
          <?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8') ;?><br />
            <input type="hidden" value="<?php echo $group->id; ?>" name="group_id">
                <?php endforeach?>
			</td>
			<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->user_id, lang('index_active_link')) : anchor("auth/activate/". $user->user_id, lang('index_inactive_link'));?></td>
			<td><?php echo anchor("auth/edit_user/".$user->user_id, 'Edit') ;?></td>
		</tr>
	    <?php endforeach;?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>