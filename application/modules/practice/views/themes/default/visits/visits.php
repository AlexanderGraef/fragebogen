<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('past_visits')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('past_visits')?></h6>
  </div>
  <div class="card-body">
         <div class="row">
        
        <div class="col-md-1" >
         
        </div>
        <div class="col-md-4">
          <div class="row" style=" padding-top: 20px">
            <div class="col-md-5" style=" margin-top: 2%">
              <p style="font-size: 15px; font-weight: bold;">Visit start</p>
          </div>
        
          <div  style="margin-top: 2%; text-align: right;" class="col-md-5">
           <p style="font-size: 15px; font-weight: bold;">Visit Duration</p>      
          </div>
          </div>
        </div>
      
        <div class="col-md-6" style="margin-left: 10px; padding-top: 25px">
         <p style="font-size: 15px; font-weight: bold;">Company</p>
        
        </div>
      
      
    </div>
        <?php
    $i = 1;
    foreach($visits as $visit) : ?>
    <div class="row">
        
        <div class="col-md-1" >
          <p style="text-align: left;border-left: 1px solid #d1d5da;padding: 50% 0; float: left; height: 100%"></p>
          <p style="border-top: 1px solid #d1d5da; width: 50%; height: 2px; margin-top: 50%"> </p>
        </div>
        <div class="col-md-4">
          <div class="row" style=" padding-top: 20px; text-align: left;">
            <div class="col-md-5" style=" margin-top: 2%">
              <p style="font-size: 11px;"><?php echo date("d M Y h:i A",strtotime($visit->start_date)); ?></p>
          </div>
          <p>
            -
          </p>
          <div  style="margin-top: 2%; text-align: center;" class="col-md-5">
            <p style="font-size: 11px;"><?php echo date("h",strtotime($visit->visit_duration)).' hours '.date("i",strtotime($visit->visit_duration)).' min'; ?></p>           
          </div>
          </div>
        </div>
      
        <div class="col-md-6" style="margin-left: 10px; padding-top: 15px">
         <p><b><?php echo $visit->visit_name.' - '.$visit->companyName; ?></b>
            <br><span style="font-size: 11px; margin-top: 50px">
              <?php echo $visit->comments; ?>
            </span>
         </p>
        
        </div>
      
      
    </div>
      <?php 

    endforeach; ?>
       
  </div>
</div>

</div>
<!-- /.container-fluid