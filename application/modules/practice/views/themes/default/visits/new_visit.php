<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('new_visit')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('new_visit')?></h6>
  </div>
  <div class="card-body">
  <?php echo validation_errors('<div class="error">'); ?>
  <?php echo form_open(); ?>
  <div class="row mt-5">
  </div>
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-4 offset-2">
        <div class="form-group">
            <label style="font-size: 12px;"> 
              Enter Visit name.
            </label>
            <div class="form-group">
              <input type="text" name="visit_name" class="form-control">             
            </div>
        </div> 
        <div class="form-group">
        	<label>Select Company</label>
           <select class="form-control" required name="company_id">
                  <option selected disabled>
                    Select company
                  </option>
                 <?php 
                foreach ($companies as $company)
                {

                ?>
             <option value="<?php echo $company->id; ?>" class="form-control">
              <?php echo $company->companyName; ?>
             </option>   
             <?php 
            }
             ?>
             </select> 
        </div>
       
      </div>
      <div class="col-md-4 ">
        <div class="form-group">
          <label style="font-size: 12px;">Select Start Date</label>
          <input type="datetime-local" required name="start_date" class="form-control">
        </div>
        <div class="form-group">
          <label style="font-size: 12px;">Give Duration</label>
          <input type="text" placeholder="i.e 03:45" required name="duration" class="form-control">
        </div>
       
        </div>  

    </div>
    <div class="row">
         	<div class="col-md-2">
         	</div>
         		<div class="col-md-8">
		        	<div class="form-group">
		          		<label style="font-size: 12px;">Comments</label>
		          		<textarea class="form-control" name="comments"></textarea>
		        	</div>
		        	 <div style="text-align: right;">
			          <div class="form-group">
			            <?php
			              echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
			            ?>
			            </div>
			          </div>
	    	</div>
        </div> 
       
  </div>
  <div>

    <div class="form-group">
          <ul style="font-size: 12px; max-width: 400px; list-style: none">
            <li class="li">
              <span style="background-color: yellow">Note:</span> You can add new visits here, where you have been. With comments and visiting dates.
            </li>
           
        </ul>
    </div>
  </div>
   
  </div>
</form>

  </div>
</div>

</div>
<!-- /.container-fluid -->
