<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?php echo $this->lang->line('add_question')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo $this->lang->line('add_question')?></h6>
  </div>
  <div class="card-body">
  <?php echo validation_errors('<div class="error">'); ?>
  <?php echo form_open(); ?>
	<div class="row mt-5">
		<div class="col-sm-5">
			<div class="form-group">
				<?php
				echo form_label($this->lang->line('question_name'), "questionName");
				echo form_input("questionName", "", array("class" => "form-control"));
				?>
			</div>
		</div>
        <div class="col-sm-5">
			<div class="form-group">
				<?php
				echo form_label($this->lang->line('less_than_20_notice'), "employeesNotice");
				echo form_input("employeesNotice", "", array("class" => "form-control"));
				?>
			</div>
		</div>
        <div class="col-sm-5">
			<div class="form-group">
				<?php
				$options = array(
                    "No" => "No",
                    "Yes" => "Yes"
                );
				echo form_label($this->lang->line('show_notice_on').":", "showNotice");
				echo form_dropdown("showNotice", $options, "", array("class" => "form-control"));
				?>
			</div>
		</div>
		<div class="col-sm-5">
			<div class="form-group">
				<div class="form-check form-check-inline">
					<?php echo form_radio('type', 2, TRUE, array('class' => 'form-check-input', 'id' => 'inlineRadio1')); ?>
					<label class="form-check-label" for="inlineRadio1"><?= $this->lang->line('yes_no_question')?></label>
				</div>
				<div class="form-check form-check-inline">
					<?php echo form_radio('type', 1, FALSE, array('class' => 'form-check-input', 'id' => 'inlineRadio2')); ?>
					<label class="form-check-label" for="inlineRadio2"><?= $this->lang->line('text_question')?></label>
				</div>
			</div>
		</div>
		<div class="col-sm-5">
			<div class="form-group">
				<style type="text/css">
					[type=checkbox]:after {
    content: attr(value);
    margin: -3px 15px;
    vertical-align: top;
    display: inline-block;
    white-space:nowrap;
    cursor:pointer;
}
				</style>
				<?php
				$options = array();
				$options[0] = "";
				foreach($topics as $t) {
					$options[$t->id] = $t->topicName;
				}
				echo form_label($this->lang->line('choose_topic'), "topic");
				echo form_dropdown("topicId", $options, "", array("id" => "chooseTopic", "class" => "form-control", "onchange" => "chooseSubquestion()"));
				?>
			</div>
			<div class="form-group">
				<input id="checkbox" type="checkbox" name="show_to_20less_emp"/>
				<label for="checkbox">Show question to less than 20 employees</label>
			</div>
		</div>

		<div id="subquestions" class="col-sm-5">
			
		</div>
		<div id="parentOption" class="col-sm-5">
			
		</div>
	</div>
	<div class="col-sm-5">
		<div class="form-group">
			<?php
			echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
			echo form_submit("saveAndBack", $this->lang->line('save_and_back'), array("class" => "btn btn-success"));
			?>
		</div>
	</div>
	</form>
  </div>
</div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript">
	function chooseSubquestion() {
		var optionVal = $('#chooseTopic option:selected').val();
		
		console.log(optionVal)
		$.ajax({
			url:"<?php echo base_url('admin/questionnare/questionsAjax')?>/"+ optionVal,
			type:'post',
			success:function(response){
				var obj = $.parseJSON(response);
				$("#subquestions").html('<div class="form-group"><label>Choose parent question</label><select class="form-control" name="parentId" onchange="chooseParentOption()" id="subquestion"></select></div>');
				$("#subquestion").append('<option value="0" selected="selected">No parent question</option>')
				console.log(obj)
				$.each(obj, function (index, object) {
					if(object['type'] == 2) {
						$("#subquestion").append('<option value="'+ object['id'] +'">'+ object['question'] +'</option>')
					}
				})
				
			}
		})
	
}

function chooseParentOption() {
	$("#parentOption").html('<div class="form-group"><label>Choose parent option</label><select class="form-control" name="parentOption" id="parentO"></select></div>');
	$("#parentO").append('<option value="Yes" selected="selected">Yes</option>')
	$("#parentO").append('<option value="No">No</option>')
	var optionVal = $('#subquestion option:selected').val();
	if(optional == 0) {
		$("#parentOption").html()
	}

}

</script>