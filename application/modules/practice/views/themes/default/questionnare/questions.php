<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?php echo $this->lang->line('questions')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo $this->lang->line('questions')?></h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?php echo $this->lang->line('question')?></th>
            <th></th>
            <th></th>
            <th></th>
            <th class="text-right"><?php echo $this->lang->line('actions')?></th>
          </tr>
        </thead>
        
        <?php
        $questions = array();
        foreach($result as $row) {
            $row = (array) $row;
        $row['childs'] = array();
        $questions[$row['id']] = $row;
        }

        foreach ($questions as $k => &$v) {
        if ($v['parentId'] != 0) {
            $questions[$v['parentId']]['childs'][] =& $v;
        }
        }
        unset($v);

        foreach ($questions as $k => $v) {
        if ($v['parentId'] != 0) {
            unset($questions[$k]);
        }
        }
        $tha = $this;
        
        function display_questions(array $questions, $level = 0, $topics, $tha) {
            $user = $tha->ion_auth->user()->row();
            $user_id = $user->id;
            $parentI = 1;
            $childI = 1;
        foreach ($questions as $info) :
          $childCount = count($info['childs']) + 1;
          $parentId = $info['parentId'];
        ?>
        <?php if($level == 0) echo '<tbody class="row_position" id="'.$info['id'].'">'; ?>
            <?php 
              $inc = $info['is_deleted_by']; 
              $cs = !empty($inc)?explode(",",$inc):array();
                if(!in_array($user_id,$cs))
                {
                     $new ='<span style="font-size:11px; padding-bottom:1px;color: white; border-radius: 5px;background-color:#0000ff94;">&nbsp; new &nbsp; </span>';
                      $viewed = $info['is_viewed_by']; 
                      $obov = !empty($viewed)?explode(",",$viewed):array();
                      if(in_array($user_id,$obov))
                      {
                        $new = "";
                      }
            ?>
            <tr id="<?php echo $info['id'] ?>" class="roww move" <?php if($parentId > 0) echo 'data-parent="'.$parentId.'"'?>>
            <?php if($level == 0) echo '<td rowspan="'.$childCount.'">#</td>' ?>
            <td><?php echo str_repeat('-', $level). " ". $info['question'] ?></a><?php echo $new; ?></td>
            <td>
                <a class="btn btn-success btn-sm" href="<?php echo base_url().'practice/questionnare/questions/edit/'.$info['id'] ?>"><?= $tha->lang->line('edit')?></a>
            </td>
            <td>
                <a class="btn btn-danger btn-sm" id="deleteQuestion" data-number="<?php echo $parentI ?>" data-id="<?php echo $info['id'] ?>" href="#"><?php echo $tha->lang->line('delete')?></a>
            </td>
            <td>
            <?php echo $tha->lang->line('move_to')?>
                <select id="moveToTopic<?php echo $info['id'] ?>" onchange="moveToTopic(<?php echo $info['id'] ?>)" class="form-control">
                    <option selected="selected"></option>
                    <?php foreach($topics as $topic) : ?>
						<option value="<?php echo $topic->id;?>"><?php echo $topic->topicName; ?></option>
					<?php endforeach; ?>
                </select>
            </td>
            <?php if($level == 0):?>
            <td rowspan="<?php $childCount ?>">
                <a style="cursor:pointer; color: blue" class="moveUp"><?php echo $tha->lang->line('up')?></a>
                <a style="cursor:pointer; color: blue" class="moveDown"><?php echo $tha->lang->line('down')?></a>
            </td>
        <?php
         endif;
    }
        ?>

        </tr>
        
        <?php
            //echo str_repeat('-', $level + 1).' comment '.$info['id']."<br>";
            if (!empty($info['childs'])) {
                display_questions($info['childs'], $level + 1, $topics, $tha);
            }
           if($level == 0) echo '</tbody>';
        endforeach;

        }

        display_questions($questions, 0, $topics, $tha);?>
        
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->
<style>
.ui-sortable-helper tr {
    display:table;
}
</style>
