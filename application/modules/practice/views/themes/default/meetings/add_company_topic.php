
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">+ <?= $this->lang->line('add_company_topic')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">+ <?= $this->lang->line('add_company_topic')?></h6>
  </div>
  <div class="card-body">
  <?php echo validation_errors('<div class="error">'); ?>
 
	<div class="row mt-5">
	</div>
	<div class="col-md-12">
		<div class="row"> 
			<div class="col-md-4">
		<?php echo form_open(); ?>
<h4>Add new topics</h4>
				<div class="form-group">
					<label style="font-size: 12px;"> Enter Topic : 1</label>
					<input type="text"  id="1" required class="form-control" onchange="chooseSubquestion(this)" name="meeting_topic[]">
				</div>
				<div id="items">
					
				</div>

<h5>Filter companies</h5>
				<div class="form-group">
					<label style="font-size: 12px;">No. of Emp (Range)</label>
					<div class="row">
						<div class="col-md-4">
						<input type="number" min="0" value="0" placeholder="start" style="width: 100px;" id="start_range"  class="form-control" onchange="filterItems()"> 
					</div>
					<p style="margin-top: 5px; margin-left: 5px"> > </p>
					<div class="col-md-4">
						<input type="number" min="1" placeholder="end" style="width: 100px"  id="end_range"  class="form-control" onchange="filterItems()">
					</div>
					</div>
					
				</div>
			<!-- 	<div class="form-group">
					<label style="font-size: 12px;">No. of BG (Range)</label>
					<div class="row">
						<div class="col-md-4">
						<input type="number" min="0" placeholder="start" style="width: 100px;" id="bg_start_range"  class="form-control" onchange="filterItems()"> 
					</div>
					<p style="margin-top: 5px; margin-left: 5px"> > </p>
					<div class="col-md-4">
						<input type="number" min="1" placeholder="end" style="width: 100px"  id="bg_end_range"  class="form-control" onchange="filterItems()">
					</div>
					</div>
					
				</div> -->
				<div class="form-group">
					<label style="font-size: 12px;"> 
			           Please select the BGs
			          </label>
			          <div class="form-group">
					<input type="text" placeholder="Select BGs" id="bgs" class="form-control" onchange="filter_bgs()">
			          <select class="form-control" onchange="filterItems()" id="bgs_id" multiple  name="bgs_id[]">

			          <?php 
			          foreach ($bgs as $bg)
			          {

			          ?>
						 <option value="<?php echo $bg->id; ?>" class="form-control">
						 	<?php echo $bg->bg; ?>
						 </option>   
						 <?php 
						}
						 ?>
						 </select>         	
			          </div>
				</div>
				<div class="form-group">
				</div>
				<div class="form-group">
					<label style="font-size: 12px;"> 
			           Please select the companies
			          </label>
			          <div class="form-group">
					<input type="text" placeholder="Filter Company" id="company_name" class="form-control" onchange="filterItems()">
			          <select class="form-control" id="compay_id" multiple required name="company_id[]">

			          <?php 
			          foreach ($companies as $company)
			          {

			          ?>
						 <option value="<?php echo $company->id; ?>" class="form-control">
						 	<?php echo $company->companyName; ?>
						 </option>   
						 <?php 
						}
						 ?>
						 </select>         	
			          </div>
				</div>

				 <div style="text-align: right;">
					<div class="form-group">
						<?php
							echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
						?>
				  	</div>
			    </div>
	</div>
			</form>	

	

		 <?php echo validation_errors('<div class="error">'); ?>
			<div class="col-md-6 offset-2">
  <?php echo form_open(); ?>
<h4>Assign topics</h4>

			
			<div class="form-group">
			 <label style="font-size: 12px;">
                  Select Companies
                </label>
				<select class="form-control" multiple name="company_id[]">
					
					<?php 
				foreach ($companies as $company)
				{
					echo "<option value='".$company->id."'>".$company->companyName."</option>";
				}
				?>
				</select>
				</div>
				<div class="form-group">
					<label style="font-size: 12px;">
						Select Topics
					</label>
				<select id='pre-selected-options' required multiple='multiple' name="meeting_topics[]">
				<?php 
				foreach ($topics as $topic)
				{
					echo "<option value='".$topic->id."'>".$topic->topic_name."</option>";
				}
				?>
				</select>
				
			</div>
			 <div style="text-align: right;">
				<div class="form-group">
					<?php
						echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
					?>
			  	</div>
    		</div>
		</div>
		</form>
			<!-- <div class="form-group">
		        <ul style="font-size: 12px; max-width: 400px; list-style: none">
		          <li class="li">
		            <span style="background-color: yellow">Note:</span> Click on any company to display the already assigned company topics. And click on the right topic list to add more topics in any selected company. 
		          </li>
		         
		      </ul>
			</div>  --> 
		</div>
	</div>
</div>
</div>
</div>
<!-- /.container-fluid -->

<script type="text/javascript">

	const heys =Array('<?php foreach ($companies as $company)
	{
		print_r($company->companyName."','");
	} ?>'); 
	const ids =Array('<?php foreach ($companies as $company)
	{
		print_r($company->id."','");
	} ?>'); 
	const bco =Array('<?php foreach ($companies as $company)
	{
		print_r($company->name."','");
	} ?>'); 
	const wco =Array('<?php foreach ($companies as $company)
	{
		print_r($company->whiteCollarName."','");
	} ?>');
	const nob =Array('<?php foreach ($companies as $company)
	{
		print_r($company->numberOfBg."','");
	} ?>'); 
	const albgs =Array('<?php foreach ($bgs as $bg)
	{
		print_r($bg->bg."','");
	} ?>'); 
	const albgsids =Array('<?php foreach ($bgs as $bg)
	{
		print_r($bg->id."','");
	} ?>'); 
	function filter_bgs()
	{
			var needle = document.getElementById("bgs").value;
			var id = 0;
			document.getElementById("bgs_id").innerHTML="";
			const heystack =  Object.keys(albgs).map(function(_) { return albgs[_]; });
  			let query = needle.toLowerCase();
  			res = heystack.filter(item => item.toLowerCase().indexOf(query) >= 0);
 			for(var i = 0; i< res.length; i++)
 			{
			 	id =  albgsids[heystack.indexOf(res[i])];
				if(id != "")
		 		{
		 			document.getElementById("bgs_id").insertAdjacentHTML("beforeend",'<option value="'+id+'" class="form-control">'+res[i]+'</option> ');
		 		}
		 	}
	}
const filterItems = () => {
	var needle = document.getElementById("company_name").value;
	var start_range = document.getElementById("start_range").value;
	var end_range = document.getElementById("end_range").value;
	var bgs = $('#bgs_id').val();

	var total_emp = 0;
	var total_nob = 0;
	var total_emp = 0;
	var id = 0;
	document.getElementById("compay_id").innerHTML="";
	//const heystack = ['apple', 'banana', 'grapes', 'mango', 'orange'];
	//const heystack = Array(<?php print_r(json_encode($companies)); ?>);
	const heystack =  Object.keys(heys).map(function(_) { return heys[_]; });
  	let query = needle.toLowerCase();
  	var res= Array();
  	if(needle == "")
  	{
  		for(var i = 0; i< heystack.length; i++)
	 	{
	 		total_emp = 0;
	 		total_emp += Number(bco[i])+Number(wco[i]);
	 		id =  ids[i];
	 		if(id != "")
	 		{
	 			if(bgs.length > 0 && start_range>=0 && end_range > 0)
	 			{
		 			if(bgs.length > 0 && total_emp >= start_range && total_emp <= end_range)
		 			{
		 				if(bgs.length > 0)
			 			{
				 			for(var j = 0; j< bgs.length; j++)
				 			{
				 				if(nob[i] == bgs[j] && total_emp >= start_range && total_emp <= end_range)
				 				{
					 				document.getElementById("compay_id").insertAdjacentHTML("beforeend",'<option value="'+id+'" class="form-control">'+heystack[i]+'</option> ');
				 				
				 				}
				 			}
			 			}
		 			
		 			}
		 		}
		 		else if(bgs.length > 0 && start_range>=0 && end_range == 0)
		 		{
		 			if (bgs.length > 0 && total_emp >= start_range)
		 			{
						if(bgs.length > 0)
			 			{
				 			for(var j = 0; j< bgs.length; j++)
				 			{
				 				if(nob[i] == bgs[j] && total_emp >= start_range)
				 				{
					 				document.getElementById("compay_id").insertAdjacentHTML("beforeend",'<option value="'+id+'" class="form-control">'+heystack[i]+'</option> ');
				 				
				 				}
				 			}
			 			}
		 			}
	 			}
	 			else if(start_range>=0 && end_range > 0)
		 		{
		 			if(total_emp >= start_range && total_emp <= end_range)
	 				{
		 				document.getElementById("compay_id").insertAdjacentHTML("beforeend",'<option value="'+id+'" class="form-control">'+heystack[i]+'</option> ');
	 				}
	 			}
	 			else if(end_range > 0)
		 		{
	 				if(total_emp <= end_range)
	 				{
		 				document.getElementById("compay_id").insertAdjacentHTML("beforeend",'<option value="'+id+'" class="form-control">'+heystack[i]+'</option> ');
	 				}
	 			}
	 			else if(start_range >= 0)
		 		{
	 				if(total_emp >= start_range)
	 				{
		 				document.getElementById("compay_id").insertAdjacentHTML("beforeend",'<option value="'+id+'" class="form-control">'+heystack[i]+'</option> ');
	 				}
	 			}

 				else if(bgs.length == 0 && start_range == 0 && end_range == 0)
 				{
 					document.getElementById("compay_id").insertAdjacentHTML("beforeend",'<option value="'+id+'" class="form-control">'+heystack[i]+'</option> ');
 				}
	 		}
  		
  		}
  	}
  	else if(needle != "")
  	{
  		res = heystack.filter(item => item.toLowerCase().indexOf(query) >= 0);
 		for(var i = 0; i< res.length; i++)
 		{
		 	id =  ids[heystack.indexOf(res[i])];
		 	total_emp = 0;
	 		total_emp += Number(bco[i])+Number(wco[i]);
			if(id != "")
	 		{
	 			if(bgs.length > 0 && start_range>=0 && end_range > 0)
	 			{
		 			if(bgs.length > 0 && total_emp >= start_range && total_emp <= end_range)
		 			{
		 				if(bgs.length > 0)
			 			{
				 			for(var j = 0; j< bgs.length; j++)
				 			{
				 				if(nob[i] == bgs[j] && total_emp >= start_range && total_emp <= end_range)
				 				{
					 				document.getElementById("compay_id").insertAdjacentHTML("beforeend",'<option value="'+id+'" class="form-control">'+res[i]+'</option> ');
				 				
				 				}
				 			}
			 			}
		 			
		 			}
		 		}
		 		else if(bgs.length > 0 && start_range>=0 && end_range == 0)
		 		{
		 			if (bgs.length > 0 && total_emp >= start_range)
		 			{
						if(bgs.length > 0)
			 			{
				 			for(var j = 0; j< bgs.length; j++)
				 			{
				 				if(nob[i] == bgs[j] && total_emp >= start_range)
				 				{
					 				document.getElementById("compay_id").insertAdjacentHTML("beforeend",'<option value="'+id+'" class="form-control">'+res[i]+'</option> ');
				 				
				 				}
				 			}
			 			}
		 			}
	 			}
	 			else if(start_range>=0 && end_range > 0)
		 		{
		 			if(total_emp >= start_range && total_emp <= end_range)
	 				{
		 				document.getElementById("compay_id").insertAdjacentHTML("beforeend",'<option value="'+id+'" class="form-control">'+res[i]+'</option> ');
	 				}
	 			}
	 			else if(end_range > 0)
		 		{
	 				if(total_emp <= end_range)
	 				{
		 				document.getElementById("compay_id").insertAdjacentHTML("beforeend",'<option value="'+id+'" class="form-control">'+res[i]+'</option> ');
	 				}
	 			}
	 			else if(start_range >= 0)
		 		{
	 				if(total_emp >= start_range)
	 				{
		 				document.getElementById("compay_id").insertAdjacentHTML("beforeend",'<option value="'+id+'" class="form-control">'+res[i]+'</option> ');
	 				}
	 			}

					else if(bgs.length == 0 && start_range == 0 && end_range == 0)
					{
						document.getElementById("compay_id").insertAdjacentHTML("beforeend",'<option value="'+id+'" class="form-control">'+res[i]+'</option> ');
					}
	 		}
	 	}
  	}

}


	var id = 2;
	function chooseSubquestion(val) 
	{
		var element =  document.getElementById(Number(val.id)+Number(1));
		var flag=false;
		if (element == null)
		{
		  flag = true;
		}
		if(val.value!="" && flag)
		{
			document.getElementById("items").insertAdjacentHTML('beforeend', '<div class="form-group">'+
				'<label style="font-size: 12px;"> Enter Topic : '+id+'</label>'+
					'<input type="text" id="'+id+'" onchange="chooseSubquestion(this)" class="form-control" name="meeting_topic[]"/>'+
				'</div>');
			id++;
		}
	}

</script>