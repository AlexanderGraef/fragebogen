<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('startmeeting')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('startmeeting')?></h6>
  </div>
  <div class="card-body">
  <?php echo validation_errors('<div class="error">'); ?>
  <?php echo form_open(); ?>
  <div class="row mt-5">
  </div>
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-4 ">

       
        <div class="form-group">
        <label style="font-size: 12px;"> 
                 Please select the company.
                </label>
                <div class="form-group">
               
                <select class="form-control" onchange="UpdateStatus(this.value)" required name="company_id">
                  <option>
                    Select company
                  </option>
                 <?php 
                foreach ($companies as $company)
                {

                ?>
             <option value="<?php echo $company->id; ?>" class="form-control">
              <?php echo $company->companyName; ?>
             </option>   
             <?php 
            }
             ?>
             </select>          
                </div>
        </div>
        <div class="form-group">
        <label>Select Start Date</label>
            <input type="datetime-local" required name="start_date" class="form-control">
        </div>
        <div class="form-group">
        <label>Meeting Duration</label>
            <input type="text" required placeholder="i.e 03:45 (3 hours 45 min)" name="meeting_duration" class="form-control">
        </div>
        <div class="form-group">
        <label>Notes</label>
            <textarea class="form-control" name="notes"></textarea>
        </div>
   <div style="text-align: right;">
    <div class="form-group">
      <?php
        echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
      ?>
      </div>
    </div>
      </div>
 <div class="col-md-4">
    <div class="form-group">
    <label style="font-size: 12px;"> 
             Company Topics.
            </label>
            <div >
              <ul id="company_topic_list" style="border:1px solid #00000014; list-style-type: none; padding: 10px">
                    <li> Select Company to displa topics </li>         
              </ul>
            </div>
    </div>
  </div>    
        
    </div>

  </div>

  <div>

    <div class="form-group">
          <ul style="font-size: 12px; max-width: 400px; list-style: none">
            <li class="li">
              <span style="background-color: yellow">Note:</span> This page will let you add New meetings for all companies working under you.
            </li>
           
        </ul>
    </div>
  </div>
   
  </div>
</form>

  </div>
</div>

</div>
<!-- /.container-fluid -->

<script type="text/javascript">
  function UpdateStatus(var1){
//make an ajax call and get status value using the same 'id'
$.ajax({

        type:"GET",//or POST
        url:'<?php echo base_url(); ?>practice/meeting/ajax_company_topics/'+var1,
                           
        //can send multipledata like {data1:var1,data2:var2,data3:var3
        //can use dataType:'text/html' or 'json' if response type expected 
        success:function(responsedata){
          var obj = JSON.parse(responsedata);
          $('#company_topic_list').html('');
          $.each( obj.topics, function( key, value ) {
            console.log(value)
          $('#company_topic_list').append('<li class="li" id="'+value.topic_id+'"><input name="topics[]" type="hidden" value="'+value.topic_name+'" id="t-'+value.topic_id+'" /> <a style="color:blue; cursor:pointer" id="'+value.topic_id+'" onclick="removetopic(this)"> X </a>'+value.topic_name+'</li>');
        });
        }
     })
}
function removetopic(topic){
  $.ajax({

        type:"GET",//or POST
        url:'<?php echo base_url(); ?>practice/meeting/ajax_delete_company_topics/'+topic.id,
                           
        //can send multipledata like {data1:var1,data2:var2,data3:var3
        //can use dataType:'text/html' or 'json' if response type expected 
        success:function(responsedata){
          $(topic).parents('li').remove();
        }
      });
}
</script>