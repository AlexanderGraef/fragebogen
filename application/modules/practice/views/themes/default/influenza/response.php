<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('influenza')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('influenza')?></h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?= $this->lang->line('member')?></th>
            <th><?= $this->lang->line('send_date')?></th>
            <th><?= $this->lang->line('status')?></th>
            <th><?= $this->lang->line('response')?></th>
            <th class="text-right"><?= $this->lang->line('actions')?></th>
          </tr>
        </thead>
        <tbody>
        <?php
		$i = 1;
		foreach($influenza as $c) : ?>

			<tr class="roww <?php if($c->mark_sent == 1):?>table-success <?php endif;?>">
        <td><?php echo $c->first_name.' '.$c->last_name; ?></td>
        <td><?php echo $c->send_date; ?></td>
        <td><?php echo $c->mark_sent==1?'Sent':'scheduled'; ?></td>
        <td><?php echo $c->response === '0'?'Company needs no Influenza':$c->response; ?></td>
                    <td class="text-right">
                        <a class="btn btn-success btn-sm" href="<?php echo base_url().'practice/influenza/delete/'.$c->id ?>"><?= $this->lang->line('delete')?></a>
                    </td>              
			</tr>
			<?php 

		endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->