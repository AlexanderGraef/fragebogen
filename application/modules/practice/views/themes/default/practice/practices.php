<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><?php echo $this->lang->line('practices')?></h1>
    </div>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo $this->lang->line('practices')?></h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?php echo $this->lang->line('practice')?></th>
            <th class="text-right"><?php echo $this->lang->line('actions')?></th>
          </tr>
        </thead>
        <tbody>
        <?php
		foreach($practices as $practice) : ?>
			<tr class="roww">
				<td><a href="<?php echo base_url().'admin/practices/'.$practice->id; ?>"><?php echo $practice->name ?></a></td>
				<td class="text-right">
					<a class="btn btn-success btn-sm" href="<?php echo base_url().'admin/practices/edit/'.$practice->id ?>"><?php echo $this->lang->line('edit')?></a>
					<a class="btn btn-danger btn-sm" id="deletePractice" data-id="<?php echo $practice->id ?>" href="<?php echo base_url().'admin/practices/delete/'.$practice->id ?>"><?php echo $this->lang->line('delete')?></a>
				</td>
			</tr>
			<?php
		endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->