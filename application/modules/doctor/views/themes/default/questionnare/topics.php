<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><?php echo $this->lang->line('topics')?></h1>
      <a href="<?= base_url() ?>doctor/questionnare/sendNewQuestionsEmail" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-envelope fa-sm text-white-50"></i> <?php echo $this->lang->line('new_questions_email')?></a>
    </div>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo $this->lang->line('topics')?></h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?php echo $this->lang->line('topic')?></th>
            <th class="text-right"><?php echo $this->lang->line('actions')?></th>
          </tr>
        </thead>
        <tbody>
        <?php
		foreach($topics as $topic) : ?>
			<tr class="roww">
				<td><a href="<?php echo base_url().'doctor/questionnare/topic/'.$topic->id; ?>"><?php echo $topic->topicName ?></a></td>
				<td class="text-right">
					<a class="btn btn-success btn-sm" href="<?php echo base_url().'doctor/questionnare/topic/edit/'.$topic->id ?>"><?php echo $this->lang->line('edit')?></a>
					<a class="btn btn-danger btn-sm" id="deleteTopic" data-id="<?php echo $topic->id ?>" href="<?php echo base_url().'doctor/questionnare/topic/delete/'.$topic->id ?>"><?php echo $this->lang->line('delete')?></a>
				</td>
			</tr>
			<?php
		endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->