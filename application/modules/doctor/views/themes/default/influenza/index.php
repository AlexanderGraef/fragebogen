<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('influenza')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('scheduled_date')?></h6>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-md-3" > 
      </div>
      <div class="col-md-7">
        <div class="row" style=" padding-top: 20px; margin-left: 10px">
            <p style="font-size: 20px; font-weight: bold;"><?= $this->lang->line('send_date')?></p>
        </div>
      </div>
      <div class="col-md-1">
        <div class="row" style=" padding-top: 20px; margin-left: 10px">
            <p style="font-size: 20px; font-weight: bold;"><?= $this->lang->line('action')?></p>
        </div>
      </div>
    </div>
        <?php
    $i = 1;
    foreach($influenza as $obo) : 
      if($obo->send_date > date("Y-m-d")): ?>
    <div class="row">  
      <div class="col-md-1"></div>
        <div class="col-md-1" style="text-align: left;border-left: 1px solid #d1d5da;">
          <div class="row" style=" padding-top: 25px; text-align: left;">
       
          </div>
           <div class="row" style="text-align: left;border-top: 1px solid #d1d5da; ">
       
          </div>
        </div>
      <div class="col-md-1"></div>
        <div class="col-md-7" style="margin-left: 10px; padding-top: 15px">
         <p><b><?php echo $obo->send_date; ?></b></p>
        </div>
        <div class="col-md-1" style="padding-top: 15px">
         <a style="color:white" href="<?php echo base_url(); ?>practice/influenza/delete/<?php  echo $obo->id; ?>" class="btn btn-primary">Delete</a>
        </div>
    </div>
      <?php 
    endif;
    endforeach; ?> 
  </div>
</div>


<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('past_date')?></h6>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-md-3" > 
      </div>
      <div class="col-md-4">
        <div class="row" style=" padding-top: 20px; margin-left: 10px">
            <p style="font-size: 20px; font-weight: bold;"><?= $this->lang->line('send_date')?></p>
        </div>
      </div>
    </div>
        <?php
    $i = 1;
    foreach($influenza as $obo) :
    if($obo->send_date <= date("Y-m-d")): ?>
    <div class="row">  
      <div class="col-md-1"></div>
        <div class="col-md-1" style="text-align: left;border-left: 1px solid #d1d5da;">
          <div class="row" style=" padding-top: 25px; text-align: left;">
       
          </div>
           <div class="row" style="text-align: left;border-top: 1px solid #d1d5da; ">
       
          </div>
        </div>
      <div class="col-md-1"></div>
        <div class="col-md-6" style="margin-left: 10px; padding-top: 15px">
         <p><b><?php echo $obo->send_date; ?></b></p>
        </div>
    </div>
      <?php 
    endif;
    endforeach; ?> 
  </div>
</div>
</div>
<!-- /.container-fluid