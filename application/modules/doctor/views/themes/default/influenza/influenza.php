<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('influenza')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('influenza')?></h6>
  </div>
  <div class="card-body" style="margin: auto;">
  <?php echo validation_errors('<div class="error">'); ?>
  <?php echo form_open(); ?>
	<div class="row mt-5">
	</div>
	<div>
		<div class="form-group">
		<label style="font-size: 12px;"> 
	           Please select the date you want to ask companies about <?= $this->lang->line('influenza')?>.
	          </label>
			 <input type="date" required name="send_date" placeholder="Select Date" class="form-control">
		</div>
	</div>
	 <div style="text-align: right">
		<div class="form-group">
			<?php
				echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
			?>
	  	</div>
    </div>
	<div>

		<div class="form-group">
	        <ul style="font-size: 12px; max-width: 400px">
	          <li>
	            <span style="background-color: yellow">Note:</span> Here you will ask all of your companies for <?= $this->lang->line('influenza')?>. This will send an email to all companies and companies will be able to response back with amount of <?= $this->lang->line('influenza_shots')?> they need. 
	          </li>
	         
	      </ul>
		</div>
	</div>
   
	</div>
</form>

  </div>
</div>

</div>
<!-- /.container-fluid -->