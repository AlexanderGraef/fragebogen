<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?php echo $this->lang->line('create_invoice')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo $this->lang->line('create_invoice')?></h6>
  </div>
  <div class="card-body">
  <?php echo validation_errors('<div class="error">'); ?>
  <?php echo form_open(); ?>
	<div class="row mt-5">

		<div class="col-sm-4">
			<div class="form-group">
				<?php
				
				
				echo form_label($this->lang->line('choose_client'), "clients");
				
				?>
				<select id="client" name="client_id" class="form-control">
				<option>
					Select Client
				</option>
					<?php 
					foreach($practices as $t) {
						echo "<option value='".$t->id."'>".$t->first_name.' '.$t->last_name."</option>";
					}
					 ?>
				</select>
			</div>
		</div>
		<div class="col-sm-4">
		</div>
		<div class="col-sm-4">
			
				<p id="client_name">Client Name: </p>
				<p id="client_email">Client Email: </p>
				<p id="client_phone">Client phone: </p>
		
		</div>
		
<div class="col-sm-12" style="border-top: 1px #0000001a solid">
</div>
	<br>
		<div class="col-sm-4">
			<div class="form-group">
				<?php
				echo form_label($this->lang->line('invoice_date'), "invoice_date");
				?>
				<input type="date" required name="invoice_date" class="form-control">
			</div>
		</div>
        <div class="col-sm-4">
        	<div class="form-group">
				<?php
				echo form_label($this->lang->line('due_date'), "due_date");
				?>
				<input type="date" required name="due_date" class="form-control">
			</div>
			
		</div>
        <div class="col-sm-4">
			<div class="form-group">
				<?php
				echo form_label($this->lang->line('discount'), "discount");
				echo form_input("discount", "", array("class" => "form-control"));
				?>
			</div>
		</div>

<div class="card-body show" id="collapseMyTable">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?= $this->lang->line('item')?></th>
            <th><?= $this->lang->line('description')?></th>
            <th><?= $this->lang->line('unit_cost')?></th>
            <th><?= $this->lang->line('quantity')?></th>
            <th><?= $this->lang->line('line_total')?></th>
          </tr>
        </thead>
    <tbody id="items">
    	<tr class="roww">
    	 <td>
				<div class="form-group">
					<input type="text" id="1" class="form-control" onchange="chooseSubquestion(this)" name="item[]" id="item">
				</div>
		</td>
		 <td>
				<div class="form-group">
					<textarea name="description[]" class="form-control"></textarea>
				</div>
			</td>
		 <td>
				<div class="form-group">
					<p class="form-control" id="unit_cost1"></p>
					<input type="hidden" class="form-control" name="unit_cost[]">
				</div>
			</td>
		 <td>
				<div class="form-group">
					<?php
					echo form_input("quantity[]", "", array("class" => "form-control"));
					?>
				</div>
			</td>
		 <td>
				<div class="form-group">
					<p class="form-control" id="line_total1"></p>
				</div>
		</td>
	</tr>
	</tbody>
	</table>
			<!-- <div class="form-group">
				<div class="form-check form-check-inline">
					<?php echo form_radio('type', 2, TRUE, array('class' => 'form-check-input', 'id' => 'inlineRadio1')); ?>
					<label class="form-check-label" for="inlineRadio1"><?= $this->lang->line('yes_no_question')?></label>
				</div>
				<div class="form-check form-check-inline">
					<?php echo form_radio('type', 1, FALSE, array('class' => 'form-check-input', 'id' => 'inlineRadio2')); ?>
					<label class="form-check-label" for="inlineRadio2"><?= $this->lang->line('text_question')?></label>
				</div>
			</div> -->



		<div id="subquestions" class="col-sm-5">
			
		</div>
		<div id="parentOption" class="col-sm-5">
			
		</div>
	</div>
	<div class="row mt-5">		
	<div class="col-sm-3">
		<div class="form-group">
			<?php
			echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
			?>
		</div>
	</div>
	<div class="col-sm-3"></div>
	<div class="col-sm-2">
		<div class="form-group">
			Subtotal: <p class="form-control" id="subtotal_cost"></p>
		</div>
	</div>
	<div class="col-sm-2">
		<div class="form-group">
			Discount: <p class="form-control" id="discount"></p>
		</div>
	</div>
		<div class="col-sm-2">
			<div class="form-group">
				Balance Due: <p class="form-control" id="discounted_cost"></p>
			</div>
		</div>
	</form>
  </div>
</div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript">


	var id = 2;
	function chooseSubquestion(val) 
	{

		var element =  document.getElementById(Number(val.id)+Number(1));
		var flag=false;
		if (element == null)
		{
		  flag = true;
		}
		if(val.value!="" && flag)
		{
		document.getElementById("items").insertAdjacentHTML('beforeend', '<tr class="roww">'+
    	 '<td>'+
				'<div class="form-group">'+
					'<input type="text" id="'+id+'" onchange="chooseSubquestion(this)" class="form-control" name="item[]"/>'+
				'</div>'+
		'</td>'+
		 '<td>'+
				'<div class="form-group">'+
					'<textarea name="description[]" class="form-control"></textarea>'+
				'</div>'+
			'</td>'+
		 '<td>'+
				'<div class="form-group">'+
					'<p class="form-control" id="unit_cost'+id+'"></p>'+
				'</div>'+
			'</td>'+
		 '<td>'+
				'<div class="form-group">'+
					'<input type="text" class="form-control" name="quantity[]"/>'+
				'</div>'+
			'</td>'+
		 '<td>'+
				'<div class="form-group">'+
					'<p class="form-control" id="line_total'+id+'"></p>'+
				'</div>'+
		'</td>'+
	'</tr>');
		id++;
	}
}
document.getElementById("client").addEventListener("change", function(){
  var optionVal = $('#client option:selected').val();
		
		console.log(optionVal)
		$.ajax({
			url:"<?php echo base_url('admin/invoices/clientbyid')?>/"+ optionVal,
			type:'post',
			success:function(response){
				var obj = $.parseJSON(response);
				console.log(obj['contacts'][0]['is_owner']);
						$("#client_name").innerHTML="Client Name: "+obj['contacts'][0]['first_name']+" "+obj['contacts'][0]['last_name'];
						$("#client_email").innerHTML="Client Email: "+obj['contacts'][0]['email'];
						$("#client_phone").innerHTML="Client phone: "+obj['contacts'][0]['phone'];
					}
				})
			

});


</script>