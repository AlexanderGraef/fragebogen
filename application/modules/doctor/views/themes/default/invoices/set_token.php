
<div class="card shadow mb-4" style="width: 350px; margin: auto; margin-top: 10%">



   <div class="card-body show">

      <form method="post">
        <div class="form-group">
          <input type="text" name="token" placeholder="Enter Token" class="form-control">
          <span style="font-size: 14px">Don't have a token ? <a target="_blank" href="https://app.invoiceninja.com">click here</a></span>
        </div>
        <div class="form-group">
          <input type="submit" class="btn-primary form-control" style="position: relative; width: auto; float:right" name="submit">
        </div>
      </form>
   
      </div>
      </div>
      <div style="width: 350px; margin: auto;">
        <ol  style="font-size: 12px">
      <li>
        Visit the link <a href="https://app.invoiceninja.com" target="_blank">https://app.invoiceninja.com</a>
      </li>
      <li>
        Sign up if you are new. or login to your invoice ninja account.
      </li>
      <li>
        Go to settings > API Tokens (Advance settings) > Add Token
      </li>
      <li>
        Copy the generated Token and paste up right there.
     </li>
      </ol>
      </div>
 