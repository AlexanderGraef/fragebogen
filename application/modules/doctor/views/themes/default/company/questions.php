<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('questions')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo $topic['topicName'] ?></h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <?php echo validation_errors('<div class="error">'); ?>
        <?php echo form_open(); ?>
        <tbody class="row_position">
        <?php
		$company = $this->AdminModel->getCompany($this->uri->segment(5));
		$userId = $company['userId'];
		
		$index = 0;
		foreach ($questions as $q) :
			if ($q->parentId == 0 || $q->parentId == NULL) :
				$answer = $this->QuestionnareModel->getAnswer($q->id, $userId);
				?>
				<tr class="question-<?php echo $q->id ?>">
					<td><?php echo $q->question ?></td>
					<td>
						<?php if ($q->type == 2) :
							$checkedYes = FALSE;
							$checkedNo = FALSE;
							$checkedUnsure = FALSE;
							if (isset($answer['answer'])) {
								if ($answer['answer'] == "Yes") {
									$checkedYes = TRUE;
								} elseif ($answer['answer'] == "No") {
									$checkedNo = TRUE;
								} elseif ($answer['answer'] == "Unsure") {
									$checkedUnsure = TRUE;
								}
							} ?>
							<div class="form-check form-check-inline">
								<?php echo form_radio('answer-' . $q->id . '[0]', 'Yes', $checkedYes, array('class' => 'form-check-input', 'id' => 'inlineRadio1', 'data-id' => $q->id)); ?>
								<label class="form-check-label" for="inlineRadio1"><?= $this->lang->line('yes')?></label>
							</div>
							<div class="form-check form-check-inline">
								<?php echo form_radio('answer-' . $q->id . '[0]', 'No', $checkedNo, array('class' => 'form-check-input', 'id' => 'inlineRadio2', 'data-id' => $q->id)); ?>
								<label class="form-check-label" for="inlineRadio2"><?= $this->lang->line('no')?></label>
							</div>
							<div class="form-check form-check-inline">
								<?php echo form_radio('answer-' . $q->id . '[0]', 'Unsure', $checkedUnsure, array('class' => 'form-check-input', 'id' => 'inlineRadio2', 'data-id' => $q->id)); ?>
								<label class="form-check-label" for="inlineRadio2"><?= $this->lang->line('unsure')?></label>
							</div>
						<?php else :
						echo form_input('answer-' . $q->id . '[0]', $answer['answer'], array('class' => 'form-control'));
					endif; ?>
					</td>
					<input name="answer-<?php echo $q->id; ?>[1]" class="form-control" type="hidden" value="<?php echo $q->id ?>" />
				</tr>
				<?php
				$index++;
			endif;
			$answerr = "";
			if (isset($answer['answer'])) {
				$answerr = $answer['answer'];
			}
            $subquestions = $this->QuestionnareModel->getQuestionByParentId($q->id, $answerr);
            if ($subquestions) :
                foreach($subquestions as $subquestion): 
				if (isset($answer['answer'])) :
					if ($answer['answer'] == "Yes" || $answer['answer'] == "No") :
						$answer = $this->QuestionnareModel->getAnswer($subquestion->id, $userId);
						?>
						<tr data-parentId="<?php echo $q->id ?>" class="question-<?php echo $subquestion->id ?>">
							<td><?php echo $subquestion->question?></td>
							<td>
								<?php if ($subquestion->type == 2) :
									$checkedYes = FALSE;
									$checkedNo = FALSE;
									$checkedUnsure = FALSE;
									if (isset($answer['answer'])) {
										if ($answer['answer'] == "Yes") {
											$checkedYes = TRUE;
										} elseif ($answer['answer'] == "No") {
											$checkedNo = TRUE;
										} elseif ($answer['answer'] == "Unsure") {
											$checkedUnsure = TRUE;
										}
									} ?>
									<div class="form-check form-check-inline">
										<?php echo form_radio('answer-' . $subquestion->id . '[0]', 'Yes', $checkedYes, array('class' => 'form-check-input', 'id' => 'inlineRadio1', 'data-id' => $subquestion->id)); ?>
										<label class="form-check-label" for="inlineRadio1"><?= $this->lang->line('yes')?></label>
									</div>
									<div class="form-check form-check-inline">
										<?php echo form_radio('answer-' . $subquestion->id . '[0]', 'No', $checkedNo, array('class' => 'form-check-input', 'id' => 'inlineRadio2', 'data-id' => $subquestion->id)); ?>
										<label class="form-check-label" for="inlineRadio2"><?= $this->lang->line('no')?></label>
									</div>
									<div class="form-check form-check-inline">
										<?php echo form_radio('answer-' . $subquestion->id . '[0]', 'Unsure', $checkedUnsure, array('class' => 'form-check-input', 'id' => 'inlineRadio2', 'data-id' => $subquestion->id)); ?>
										<label class="form-check-label" for="inlineRadio2"><?= $this->lang->line('unsure')?></label>
									</div>
								<?php else :
								echo form_input('answer-' . $subquestion->id . '[0]', $answer['answer'], array('class' => 'form-control'));
							endif; ?>
							</td>
							<input name="answer-<?php echo $subquestion->id; ?>[1]" class="form-control" type="hidden" value="<?php echo $subquestion->id ?>" />
						</tr>
                    <?php endif;
            endif;
        endforeach;
		endif;
	endforeach; ?>
        </tbody>
      </table>
      <?php echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success")); ?>
    </form>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->
