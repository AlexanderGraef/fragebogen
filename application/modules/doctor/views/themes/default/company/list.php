<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('company_list')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('company_list')?></h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?= $this->lang->line('company_name')?></th>
            <th><?= $this->lang->line('owner_name')?></th>
            <th class="text-right"><?= $this->lang->line('actions')?></th>
          </tr>
        </thead>
        <tbody>
        <?php
		$i = 1;
		foreach($companies as $c) : ?>
  
      <?php $flag = FALSE; foreach ($this->ion_auth->get_users_groups($c->userId)->result() as $group):?>
          <?php $flag = $group->id==2?TRUE:FALSE; ?><br />
               <?php endforeach?>
               <?php if($flag ==TRUE){ ?>
			<tr class="roww <?php if($c->seized == 1):?>table-danger <?php endif;?>">
				<td><?php echo $c->companyName ?></a></td>
               <td><?php echo $c->first_name.' '.$c->last_name ?></a></td>
                    <td class="text-right">
                        <a class="btn btn-success btn-sm" href="<?php echo base_url().'doctor/company/edit/'.$c->id ?>"><?= $this->lang->line('edit')?></a>
                        <a class="btn btn-danger btn-sm" id="deleteCompany" data-id="<?php echo $c->id ?>" href="<?php echo base_url().'doctor/deletecompany/'.$c->id ?>"><?= $this->lang->line('delete')?></a>
                        <a class="btn btn-primary btn-sm" href="<?php echo base_url().'doctor/company/topics/'.$c->id ?>"><?= $this->lang->line('topics')?></a>
                       <!--  <a class="btn btn-primary btn-sm" href="<?php echo base_url().'doctor/company/verfication_email/'.$c->id ?>"><?= $this->lang->line('email_reports')?></a> -->
                        
                       <form action="company/verfication_email" method="post" accept-charset="utf-8" style="float:right; margin-left: 5px">
                         <input type="hidden" name="id" value="<?php echo $c->id; ?>">
                         <input type="hidden" name="email" value="<?php echo $c->directAdresseeForOshaEmail; ?>">
                         <input type="submit" name="submit" value="Send Email" class="btn btn-primary btn-sm">                   
                        </form>

                    </td>              
			</tr>
			<?php 
			$i++;
    }
		endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->