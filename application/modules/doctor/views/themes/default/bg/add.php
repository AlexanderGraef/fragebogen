<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('add_new_bg')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('add_new_bg')?></h6>
  </div>
  <div class="card-body">
  <?php echo validation_errors('<div class="error">'); ?>
    <?php echo form_open(); ?>
    <div class="row mt-5">
        <div class="col-sm-5">
            <div class="form-group">
                <?php
                echo form_label($this->lang->line('bg'), "bgName");
                echo form_input("bg", "", array("class" => "form-control"));
                ?>
            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            <?php
            echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
            echo form_submit("saveAndBack", $this->lang->line('save_and_back'), array("class" => "btn btn-success"));
            ?>
            
        </div>
    </div>
    </form>
  </div>
</div>

</div>
<!-- /.container-fluid -->