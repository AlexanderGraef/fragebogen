<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><?= $this->lang->line('bg_list')?></h1>
    </div>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('bg_list')?></h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?= $this->lang->line('bg')?></th>
            <th class="text-right"><?= $this->lang->line('actions')?></th>
          </tr>
        </thead>
        <tbody>
        <?php
		$i = 1;
		foreach($bgs as $bg) : ?>
			<tr class="roww">
				<td><?php echo $bg->bg ?></td>
				<td class="text-right">
					<a class="btn btn-success btn-sm" href="<?php echo base_url().'practice/bg/edit/'.$bg->id ?>"><?= $this->lang->line('edit')?></a>
					<a class="btn btn-danger btn-sm" id="deleteBg" data-id="<?php echo $bg->id ?>" href="#"><?= $this->lang->line('delete')?></a>
				</td>
			</tr>
			<?php
			$i++;
		endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->