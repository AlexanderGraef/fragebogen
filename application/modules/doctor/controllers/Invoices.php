<?php

class Invoices extends Doctor_Controller {
    function __construct() {
		parent::__construct();
		$group = 'doctor';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('doctor');
        }
    }

    public function index() {  


        $this->session->set_userdata('referred_from', current_url());
     $practice_id = $this->AdminModel->members_parent($this->session->userdata('user_id'));
     $practice_id = !empty($practice_id[0]->practiceaccount_id)?$practice_id[0]->practiceaccount_id:0;

		  $token = $this->db->get_where('users_tokens',array('user_id'=>$practice_id))->row();
		  if(!empty($token->token))
		  {
	        $invoices = $this->InvoiceModel->get_invoices();
	        $data = array(
	            'unpaid_invoices' => $invoices['unpaid'],
	            'clearance_pending_invoices' => $invoices['pending'],
	            'paid_invoices' => $invoices['paid'],
              'practice_id'=>$practice_id
	        );
       	}
       // debug_array($data['unpaid_invoices']);
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "invoices/invoices";
        $this->load->view($this->_container, $data);
    } 

   //  public function set_token()
   //  {
   //  	   if($this->input->post('token')!="")
   //      {
			// $this->db->where('user_id', $this->session->userdata('user_id'));
   // 			$this->db->delete('users_tokens');

			// $this->db->insert("users_tokens",array("token"=>$this->input->post('token'),"user_id"=>$this->session->userdata('user_id')));	
   // 			$members = $this->PracticeModel->admin_users($this->session->userdata('user_id'));
   // 				//	debug_array($members);
   // 			foreach ($members as $member)
   // 			{
   // 				$bool = $this->PracticeModel->client_as_member($member->user_id);
   // 				if(!isset($bool[0]))
   // 				{
   // 					$this->PracticeModel->turn_members_into_clients($member);
   // 				}
   // 			}
   //      	redirect('practice/invoices');
   //      }
   //  	 $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "invoices/set_token";
   //      $this->load->view($this->_container, $data);
   //  }
 
  //   public function cost_management() {    
  //       //$users = $this->ion_auth->users()->result();

        
  //       if($this->input->post('white_collars_unit_cost')!="")
		// {
		// 	$para['global_price_whiteC'] = $this->input->post('white_collars_unit_cost');
		// 	$para['global_price_blueC'] = $this->input->post('blue_collars_unit_cost');
		// 	$this->AdminModel->update_global_settings($para,2);
		// 	$this->session->set_flashdata('msg', 'Rates upadted successfully');
		// }
  //       if($this->input->post('practice_name')!="")
		// {
		// 	$user_id = $this->input->post('practice_id');
		// 	$para['unit_whitecollar'] = $this->input->post('unit_whitecollar');
		// 	$para['unit_bluecollar'] = $this->input->post('unit_bluecollar');
		// 	$this->AdminModel->update_price_list($user_id,$para);
		// 	$this->session->set_flashdata('msg', 'Rates upadted successfully');
		// }
		
		// $data = array(
  //       	'members' => $this->AdminModel->practice_users($this->session->userdata('user_id')),
  //       	'global_settings'=>$this->AdminModel->global_settings(2),
  //       	'price_lists' => $this->AdminModel->price_list()

  //       	);
	 //   // debug_array($data['practices']);
  //       $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "invoices/cost_management";
  //       $this->load->view($this->_container, $data);
  //   }

    public function send_schedule_invoice()
	{
        $data['response'] = $this->InvoiceModel->send_schedule_invoice();
		$data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "invoices/send_schedule_invoice";
        $this->load->view($this->_container, $data);
       // redirect('auth', 'refresh');
	}
    public function customize_invoice($member_id=null) {  

        $this->session->set_userdata('referred_from', current_url());
      $this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('companyName', 'companyName', 'required');
		if($this->input->post('clients')!="")
		{
			$dates= explode(",",$this->input->post('dates'));
			$clients = $this->input->post('clients');
			$discount = $this->input->post('discount');
			$parameters = array("dates"=>$dates,"clients"=>$clients,"discount"=>$discount);
			if($this->input->post('items')!="")
			{
				$parameters["items"] = $this->input->post('items');
				$parameters["descriptions"] = $this->input->post('descriptions');
				$parameters["unit_costs"] = $this->input->post('unit_costs');
				$parameters["quantities"] = $this->input->post('quantities');
        $parameters["taxes"] = $this->input->post('taxes');
				$parameters["amounts"] = $this->input->post('amounts');
			}
			$response = $this->InvoiceModel->draft_auto_invoices($parameters);
			if($response['response'] == FALSE)
			{
				$this->session->set_flashdata('msg',"Invoice scheduled and sent");
			}
			else
			{
				$newArray = array_keys($response['response']);
				$msg = $response['response'][$newArray[0]][0];
				$this->session->set_flashdata('msg',$msg);
			}
		}
		   $practice_id = $this->AdminModel->members_parent($this->session->userdata('user_id'));
     $practice_id = !empty($practice_id[0]->practiceaccount_id)?$practice_id[0]->practiceaccount_id:0;

       $data['members'] = $this->DoctorModel->companies_as_clients();
       $data['practice_id'] = $practice_id;
       $data['client_id'] = $this->AdminModel->client_as_member($member_id);
       // debug_array($data['client_id']);
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "invoices/customize_invoice";
        $this->load->view($this->_container, $data);
    }
    public function generate_invoice($member_id) {    
        $this->session->set_userdata('referred_from', current_url());
        $invoice = $this->InvoiceModel->generate_invoice($member_id,array());
       // debug_array($data['unpaid_invoices']);
        $data['invoice_id'] = 1;
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "invoices/showinvoice";
        $this->load->view($this->_container, $data);
    }
    function clientbyid($id)
    {
    	$data = $this->InvoiceModel->clientbyid($id);
		echo json_encode($data);
    	
    }
   
 	function create_invoice()
 	{
 	// 	$this->load->library('form_validation');
		// $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		// $this->form_validation->set_rules('companyName', 'companyName', 'required');
		// $company = $this->AdminModel->getCompany($id);
		
		// $data = array(
		// 	"company" => $company,
		// 	"blueCollars" => $this->CompanyModel->getNumberOfBlueCollars($company['companyId']),
  //           "whiteCollars" => $this->CompanyModel->getNumberOfWhiteCollars($company['companyId']),
  //           "bgs" => $this->AdminModel->getBgs()
  //       );
 		$data['practices'] = $this->AdminModel->admin_practices();
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "Invoices/create_invoice";
		$this->load->view($this->_container, $data);
		
		
 	}

}