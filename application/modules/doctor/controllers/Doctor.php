<?php

class Doctor extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->referred_from = $this->session->userdata('referred_from');
		$group = 'doctor';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('doctor');
        }
    }

    public function index() {
        $this->session->set_userdata('referred_from', current_url());
		$data = array(
			'practices' => $this->AdminModel->getPractices()
		); 
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "questionnare/topics";
        $this->load->view($this->_container, $data);
    }
}