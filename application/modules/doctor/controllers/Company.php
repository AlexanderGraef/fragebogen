<?php

class Company extends Doctor_Controller {
    function __construct() {
		parent::__construct();
		$group = 'doctor';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be a practicing admin to view the Company page.');
            redirect('doctor');
        }
    }

    public function index() {    
        $this->session->set_userdata('referred_from', current_url());
        $data = array(
            'companies' => $this->DoctorModel->companies()
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "company/list";
        $this->load->view($this->_container, $data);
    }

    function edit($id) {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('companyName', 'companyName', 'required');
		$company = $this->DoctorModel->getCompany($id);
		
		$data = array(
			"company" => $company,
			"blueCollars" => $this->CompanyModel->getNumberOfBlueCollars($company['companyId']),
            "whiteCollars" => $this->CompanyModel->getNumberOfWhiteCollars($company['companyId']),
            "bgs" => $this->DoctorModel->getBgs()
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "company/edit";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->DoctorModel->updateCompany($id);
			$this->session->set_flashdata('msg', 'Firmendaten wurden aktualisiert.');
			if($this->input->post('submit')) {
				redirect('doctor/company/edit/'.$id, 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }
    
    function delete($id) {
		$this->DoctorModel->deleteCompany($id);
		$this->db->where('id', $id);
		$this->db->delete('company');
		$this->session->set_flashdata('msg', 'Firma wurde gel�scht.');
		redirect('doctor/company', 'refresh');
    }
    
    function topics($companyId) {
		$this->session->set_userdata('referred_from', current_url());
		$company = $this->DoctorModel->getCompany($companyId);
		$userId = $company['userId'];
		$data = array(
			'topics' => $this->QuestionnareModel->getTopics(),
			'notifications' => $this->QuestionnareModel->getNotifs($userId),
			'company' => $this->DoctorModel->getCompanyById($companyId)
  
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "company/topics";
		$this->load->view($this->_container, $data);
    }
    
    function topic($topicId, $companyId) {
		$company = $this->DoctorModel->getCompany($companyId);
		$userId = $company['userId'];
		$data = array(
			'questions' => $this->QuestionnareModel->getQuestions($topicId),
			'topic' => $this->DoctorModel->topic($topicId),
			'company' => $this->DoctorModel->getCompanyById($companyId)
		);
		$data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "company/questions";

		if (!$this->input->post())
                    {
                        $this->load->view($this->_container, $data);
                    }
                else
                    {
						foreach($this->input->post() as $i => $post) {
							if(isset($post[0]) && isset($post[0])) {
								if($post[0] != "S" && $post[0] != "") {
									$this->QuestionnareModel->setAnswers($userId, $post[0], $post[1]);
								}
							}		
							
						}
						$this->session->set_flashdata('msg', 'Frage aktualisiert.');
                        if($this->input->post('submit')) {
							redirect('doctor/company/topic/'.$topicId.'/'.$companyId);
						} elseif($this->input->post('saveAndBack')) {
							redirect($this->referred_from, 'refresh');
						}
                    }

	}

	function deleteBCollar($id) {
		$this->DoctorModel->deleteBCollar($id);
		$this->session->set_flashdata('msg', 'Collar has been deleted');
	}

	function deleteWCollar($id) {
		$this->DoctorModel->deleteWCollar($id);
		$this->session->set_flashdata('msg', 'Collar has been deleted');
	}
	function com_verfication($id,$code){
		
		if($this->DoctorModel->com_verfication($id,$code) == true)
			{
				echo "<script>alert('Company successfully verified.'); window.location.assign('".base_url()."');</script>";
			}
			else
			{
				echo "<script>alert('Link may be broken or expired verfication code is used.'); window.location.assign('".base_url()."');</script>";
			}
		
	}
	function verfication_email(){
		$id=$this->input->post('id');
		$email=$this->input->post('email');
		$response = $this->DoctorModel->verfication_email($email,$id);

		if($response == 1)
		{
			echo "<script>alert('Email Sent to this company safety person email address.'); window.location.assign('".base_url().'/doctor/company'."');</script>";
		}
		else
		{
			echo "<script>alert('Try again after few moments.'); window.location.assign('".base_url().'/doctor/company'."');</script>";
		}
	}
}