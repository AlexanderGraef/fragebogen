<?php

class Reports extends Doctor_Controller {
    function __construct() {
        parent::__construct();
		$this->referred_from = $this->session->userdata('referred_from');
		$group = 'practice';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('practice');
        }
    }

    public function index() {
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "reports/reports";
        $this->load->view($this->_container, $data);
    }

    function generateCompanyReport() {

    	
		$dateFrom = strtotime($this->input->post('dateFrom'));
		$dateTo = strtotime($this->input->post('dateTo'));
		$dateFrom = date('Y-m-d', $dateFrom);
		$dateTo = date('Y-m-d', $dateTo);
		$start    = (new DateTime($dateFrom))->modify('first day of this month');
		$end      = (new DateTime($dateTo))->modify('first day of next month');
		$interval = DateInterval::createFromDateString('1 month');
		$period   = new DatePeriod($start, $interval, $end);

		$companies = $this->PracticeModel->companies();
		$data = array();
		foreach($companies as $company) {
			$i = 0;
			$collarsB = array();
			$collarsNumber = array();
			$collarsB['company'] = $company->companyName;
			$collarsB['b/w collar'] = "Blue Collar";
			foreach ($period as $dt) {
				$month =  $dt->format("Y-m");
				
				$bCollar = $this->PracticeModel->getNumberOfBlueCollars($company->id, $month);
				
				if(!empty($bCollar) && $bCollar != NULL) {
					$collarsB[$month] = $bCollar['name'];
					$collarsNumber[] = (int)$bCollar['name'];						
				} else {
					$collarsB[$month] = "0";
				}
            }
            $collarsB['mean'] = 0;
            if(count($collarsNumber) !=0 )
            {
				$collarsB['mean'] = array_sum($collarsNumber) / count($collarsNumber);
			}


			$data[] = $collarsB;
		}

		foreach($companies as $company) {
			$i = 0;
			$collarsB = array();
			$collarsNumber = array();
			$collarsB['company'] = $company->companyName;
			$collarsB['b/w collar'] = "White Collar";
			foreach ($period as $dt) {
				$month =  $dt->format("Y-m");
				$bCollar = $this->PracticeModel->getNumberOfWhiteCollars($company->id, $month);
				if(!empty($bCollar)) {
					$collarsB[$month] = $bCollar['whiteCollarName'];
					$collarsNumber[] = (int)$bCollar['whiteCollarName'];	
				} else {
					$collarsB[$month] = "0";
				}
			}
			 $collarsB['mean'] = 0;
            if(count($collarsNumber) !=0 )
            {
				$collarsB['mean'] = array_sum($collarsNumber) / count($collarsNumber);
			}

			$data[] = $collarsB;
		}

		$this->download_send_headers("collars_report" . date("Y-m-d") . ".csv");
		
		echo $this->array2csv($data);
		die(); 
    }
    
    function download_send_headers($filename) {
		// disable caching
		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");
	
		// force download  
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
	
		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename={$filename}");
		header("Content-Transfer-Encoding: binary");
	}

	private function array2csv(array &$array)
	{
	if (count($array) == 0) {
		return null;
	}
	ob_start();
	$df = fopen("php://output", 'w');
	fputcsv($df, array_keys(reset($array)));
	foreach ($array as $row) {
		fputcsv($df, $row);
	}
	fclose($df);
	return ob_get_clean();
	}

    function company() {
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "reports/company";
        $this->load->view($this->_container, $data);
    }

    public function warningEmailsDates() {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('dateOne', 'Date One', 'required');
		$this->form_validation->set_rules('dateTwo', 'Date Two', 'required');
		$this->form_validation->set_rules('dateThree', 'Date Three', 'required');
			$data = array(
				'wDate' => $this->PracticeModel->getWdates()
            );
            $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "reports/warning_emails";
            
			if ($this->form_validation->run() === FALSE)
				{
					$this->load->view($this->_container, $data);
				}
				else
				{
					$this->PracticeModel->addWarningDates();
					$this->session->set_flashdata('msg', 'Dates have been added');
					if($this->input->post('submit')) {
						redirect('practice/reports/warningEmailsDates/', 'refresh');
				}
			}
	}
}