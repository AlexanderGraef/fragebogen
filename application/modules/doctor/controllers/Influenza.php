<?php

class Influenza extends Doctor_Controller {
    function __construct() {
		parent::__construct();
		$group = 'doctor';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be a practicing admin to view the Company page.');
            redirect('doctor');
        }
    }

  public function index() 
    {    
        $data = array(
            'influenza' => $this->PracticeModel->getInfluenza()
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "influenza/index";
        
			$this->load->view($this->_container, $data);
    }
      public function create() 
    {    
    	$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('send_date', 'send_date', 'required');
        $this->session->set_userdata('referred_from', current_url());
        $data = array(
            'influenza' => $this->PracticeModel->influenza()
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "influenza/influenza";
        if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$msg = $this->PracticeModel->schedule_influenza();
			$this->session->set_flashdata('msg', $msg);
			$this->load->view($this->_container, $data);
		}
    }
    public function response() 
    {    
    	$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('send_date', 'send_date', 'required');
        $this->session->set_userdata('referred_from', current_url());
        $data = array(
            'influenza' => $this->PracticeModel->influenza()
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "influenza/response";
        if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$msg = $this->PracticeModel->schedule_influenza();
			$this->session->set_flashdata('msg', $msg);
			$this->load->view($this->_container, $data);
		}
    }
    function cron_scheduled_influenza()
    {
    	$this->PracticeModel->cron_scheduled_influenza();
    }
    function edit($id) {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('companyName', 'companyName', 'required');
		$company = $this->PracticeModel->getCompany($id);
		
		$data = array(
			"company" => $company,
			"blueCollars" => $this->CompanyModel->getNumberOfBlueCollars($company['companyId']),
            "whiteCollars" => $this->CompanyModel->getNumberOfWhiteCollars($company['companyId']),
            "bgs" => $this->PracticeModel->getBgs()
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_practice') . "company/edit";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->PracticeModel->updateCompany($id);
			$this->session->set_flashdata('msg', 'Firmendaten wurden aktualisiert.');
			if($this->input->post('submit')) {
				redirect('practice/company/edit/'.$id, 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }
    
    function delete($id) {
		$this->PracticeModel->deleteInfluenza($id);
		$this->db->where('id', $id);
		$this->db->delete('scheduled_influenza');
		$this->session->set_flashdata('msg', 'Influenza deleted successfully.');
		redirect('practice/influenza/response', 'refresh');
    }
  
 

	function com_verfication($id,$code){
		
		if($this->PracticeModel->com_verfication($id,$code) == true)
			{
				echo "<script>alert('Company successfully verified.'); window.location.assign('".base_url()."');</script>";
			}
			else
			{
				echo "<script>alert('Link may be broken or expired verfication code is used.'); window.location.assign('".base_url()."');</script>";
			}
		
	}
	function verfication_email(){
		$id=$this->input->post('id');
		$email=$this->input->post('email');
		$response = $this->PracticeModel->verfication_email($email,$id);

		if($response == 1)
		{
			echo "<script>alert('Email Sent to this company safety person email address.'); window.location.assign('".base_url().'/practice/company'."');</script>";
		}
		else
		{
			echo "<script>alert('Try again after few moments.'); window.location.assign('".base_url().'/practice/company'."');</script>";
		}
	}
}