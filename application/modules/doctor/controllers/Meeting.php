<?php

class Meeting extends Doctor_Controller {
    function __construct() {
        parent::__construct();

        $group = 'doctor';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('doctor');
        }
    }

 public function index() {    
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $data['companies'] = $this->DoctorModel->companies($userId);
        if($this->input->post('company_id') != "" && $this->input->post('start_date') != "" && $this->input->post('meeting_duration') != "")
        {
            $this->DoctorModel->add_meeting_schedule();
        }
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "meetings/start_meeting";
        $this->load->view($this->_container, $data);
    }
  function meetings() {    
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $data['meetings'] = $this->DoctorModel->getmeetings();
        
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "meetings/meetings";
        $this->load->view($this->_container, $data);
    }

  function visits() {    
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $data['visits'] = $this->DoctorModel->getvisits();
        
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "visits/visits";
        $this->load->view($this->_container, $data);
    }

  function add_company_topic() {    
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $data['companies'] = $this->DoctorModel->companies($userId);
        $data['topics'] = $this->DoctorModel->getmeetingtopic();        
        $data['bgs'] = $this->PracticeModel->getBgs();

        if($this->input->post('meeting_topic') != '' || $this->input->post('meeting_topics') != '')
        {
            $this->DoctorModel->addMeetingTopic();
        }
       
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "meetings/add_company_topic";
        $this->load->view($this->_container, $data);
    }
  function new_visit() {
        if($this->input->post('visit_name') != '')
        {
            $this->DoctorModel->addvisit();
        }
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $data['companies'] = $this->DoctorModel->companies($userId);
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "visits/new_visit";
        $this->load->view($this->_container, $data);
    }
    function ajax_company_topics($company_id)
    {
        $data['topics'] = $this->DoctorModel->get_company_topics($company_id);
        echo json_encode($data);
    }
    function ajax_company_non_topics($company_id)
    {
        $data['topics'] = $this->DoctorModel->get_company_non_topics($company_id);
        echo json_encode($data);
    }
    function ajax_delete_company_topics($topic_id)
    {
        return $this->DoctorModel->delete_company_topics($topic_id);
    }
}