<?php

class Questionnare extends Doctor_Controller {
    function __construct() {
        parent::__construct();
		$this->referred_from = $this->session->userdata('referred_from');
		$group = 'practice';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('doctor');
        }
    }

    public function index() {
        $this->session->set_userdata('referred_from', current_url());
        $user = $this->ion_auth->user()->row();
		$user_id = $user->id;
        $practice_id = $this->AdminModel->members_parent($user_id);
        if(isset($practice_id[0]->practiceaccount_id))
        	$practice_id = $practice_id[0]->practiceaccount_id;
        else
        	$practice_id = $user_id;
		$data = array(
			'topics' => $this->QuestionnareModel->getTopics($practice_id)
		); 
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "questionnare/topics";
        $this->load->view($this->_container, $data);
    }

    function topic($topicId) {
		$this->session->set_userdata('referred_from', current_url());
		$data = array(
			'result' => $this->DoctorModel->questions($topicId),
			'topics' => $this->QuestionnareModel->getTopics(),
			'topic' => $this->DoctorModel->topic($topicId)
		);
		$data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "questionnare/questions";
        $this->load->view($this->_container, $data);
    }

    function addTopic() {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('topicName', 'Topic Name', 'required');
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "questionnare/add_topic";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$user = $this->ion_auth->user()->row();
			$user_id = $user->id;
	        $practice_id = $this->AdminModel->members_parent($user_id);
	        if(isset($practice_id[0]->practiceaccount_id))
	        	$practice_id = $practice_id[0]->practiceaccount_id;
	        else
	        	$practice_id = $user_id;

			$this->QuestionnareModel->addTopic($practice_id);
			$this->session->set_flashdata('msg', 'Thema wurde erstellt.');
			if($this->input->post('submit')) {
				redirect('doctor/questionnare/topic/add', 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
			
		}
    }
    
    function editTopic($id) {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('topicName', 'Topic Name', 'required');
		$data = array(
			'topic' => $this->DoctorModel->topic($id)
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "questionnare/edit_topic";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->QuestionnareModel->editTopic($id);
			$this->session->set_flashdata('msg', 'Thema wurde aktualisiert');
			if($this->input->post('submit')) {
				redirect('doctor/questionnare/topic/edit/'.$id);
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }
    
    function deleteTopic($id) {
		$this->DoctorModel->deleteTopic($id);
		$this->session->set_flashdata('msg', 'Thema wurde gel�scht.');
		redirect('doctor/questionnare');
    }
    
    function deleteQuestion($id) {
		$this->DoctorModel->deleteQuestion($id);
		$this->session->set_flashdata('msg', 'Frage wurde gel�scht.');
		redirect('doctor/topics');
    }
    
    function addQuestion() {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('questionName', 'Question Name', 'required');
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
        $practice_id = $this->AdminModel->members_parent($user_id);
        if(isset($practice_id[0]->practiceaccount_id))
        	$practice_id = $practice_id[0]->practiceaccount_id;
        else
        	$practice_id = $user_id;
		$data = array(
			'topics' => $this->QuestionnareModel->getTopics($practice_id)
		); 
        $users = $this->DoctorModel->getUsers();
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "questionnare/add_question";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
            $allQuestionsSort = $this->DoctorModel->getQuestionsSortIndexByTopic($this->input->post('topicId'));
            $sortI = array();
            foreach($allQuestionsSort as $sort => $val) {
                $sortI[] = (int) $val['sortIndex'];
            }
            $sortIndex = 1; 
            if(count($allQuestionsSort) > 0) {
                $sortIndex = max($sortI) + 1;
            }
            
            
			$this->DoctorModel->addQuestion($sortIndex);
			$id = $this->db->insert_id();
			$questionId = $this->db->insert_id();
			if($this->input->post('parentId') == 0) {
				foreach($users as $user) {
					$this->DoctorModel->addNotif($user->id, $questionId);
				}
			}
			$this->session->set_flashdata('msg', 'Frage wurde erstellt.');
			if($this->input->post('submit')) {
				redirect('doctor/questionnare/questions/add', 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }
    
    function editQuestion($id) {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('questionName', 'Question Name', 'required');
		$data = array(
			'topics' => $this->QuestionnareModel->getTopics(),
			'question' => $this->DoctorModel->getQuestion($id)
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_doctor') . "questionnare/edit_question";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->DoctorModel->editQuestion($id);
			$this->session->set_flashdata('msg', 'Frage wurde aktualisiert.');
			if($this->input->post('submit')) {
				redirect('doctor/questionnare/questions/edit/'.$id, 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }
    
    function questionsAjax($id) {
		$data = $this->DoctorModel->getQuestionsByTopic($id);
		echo json_encode($data);
    }

    function subquestionAjax($id, $qOption) {
		$data['q'] = $this->QuestionnareModel->getQuestionByParentId($id, $qOption);
		$this->load->view('questionnare/newQuestionRow', $data);
		//echo json_encode($data);
	}
    
    function updateQuestionsOrder() {
		$position = $_POST['position'];

		$i=1;
		foreach($position as $k=>$v){
			$this->DoctorModel->updateQuestionsOrder($i, $v);
			$i++;
		}
    }
    
    function moveToTopic() {
		$topicId = $this->input->post('topicId');
		$questionId = $this->input->post('questionId');
		$this->DoctorModel->changeQuestionTopic($topicId, $questionId);
    }
    
    function sendNewQuestionsEmail() {
		$this->load->library('email');
		$notifs = $this->DoctorModel->getNotifs();
		$users = array();
		foreach($notifs as $not) {
			if (!in_array($not->userId, $users)) {
				$questions = $this->DoctorModel->getNewQuestionsByUserId($not->userId);
				
				$config=array(
					'charset'=>'utf-8',
					'wordwrap'=> TRUE,
					'mailtype' => 'html'
					);
					
				$this->email->initialize($config);
				if(count((array)$questions) > 0) {
					$this->email->from($this->config->item('email_from'), $this->config->item('name_from'));
					$this->email->to($not->username);
					$this->email->subject($this->lang->line('new_questions_email_subject'));
					$data['questions'] = $questions;
					/* $msg = "Hier sind die noch nicht beantworteten Fragen:<br><br>";
					foreach ($questions as $question) {
						$msg .= "<br>";
						$msg .= $question->question;
					} */
					$msg = $this->load->view('emails/newQuestions', $data, TRUE);
					$this->email->message($msg);
	
					$this->email->send();
				}

				array_push($users, $not->userId);
			}

		}
		$this->session->set_flashdata('msg', 'Email wurde verschickt.');
		redirect('doctor/questionnare', 'refresh');
	}
}