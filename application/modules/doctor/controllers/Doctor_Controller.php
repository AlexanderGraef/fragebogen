<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
class Doctor_Controller extends MY_Controller {
    public $is_doctor;
    public $logged_in_name;

    function __construct() {
        parent::__construct();
        
        // Set container variable
        $this->_container = $this->config->item('ci_my_admin_template_dir_doctor') . "layout.php";
        $this->_modules = $this->config->item('modules_locations');

        $this->load->library(array('ion_auth'));

        if (!$this->ion_auth->logged_in()) {
            redirect('/auth', 'refresh');
        }

        $this->is_doctor = $this->ion_auth->is_doctor();
        $user = $this->ion_auth->user()->row();
        $this->logged_in_name = $user->first_name;

        log_message('debug', 'CI My Doctor : Doctor_Controller class loaded');
    }
}