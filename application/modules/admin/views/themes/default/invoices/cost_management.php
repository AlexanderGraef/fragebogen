<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?php echo $this->lang->line('cost_management')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo $this->lang->line('cost_management')?></h6>
  </div>
  <div class="card-body">
  <h2>
Global Rates
</h2>
  <?php echo validation_errors('<div class="error">'); ?>
  <?php echo form_open(); ?>
	<div class="row mt-5">

        <div class="col-sm-4">
			<div class="form-group">
				<?php
				echo form_label($this->lang->line('white_collars_unit_cost'), "white_collars_unit_cost");
				?>
			<input type="number" min="0" class="form-control" name="white_collars_unit_cost" value="<?php echo isset($global_settings->global_price_whiteC)?$global_settings->global_price_whiteC:''; ?>">

			</div>
		</div>
 		<div class="col-sm-4">
			<div class="form-group">
				<?php
				echo form_label($this->lang->line('blue_collars_unit_cost'), "blue_collars_unit_cost");
				?>
			<input type="number" min="0" class="form-control" name="blue_collars_unit_cost" value="<?php echo isset($global_settings->global_price_blueC)?$global_settings->global_price_blueC:''; ?>">
			</div>
		
			<div class="form-group" style="text-align: right;">
				<?php
				echo form_label(" "," ");
				echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
				?>
			</div>
	
		</div>
		
		</div>
	</form>

    <div class="table-responsive">
    <h2>
Individual Rates
</h2>
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?= $this->lang->line('practice_name')?></th>
            <th><?= $this->lang->line('blue_collars_unit_cost')?></th>
            <th><?= $this->lang->line('white_collars_unit_cost')?></th>
            <th><?= $this->lang->line('action')?></th>
          </tr>
        </thead>
    <tbody id="items">
 <?php foreach($practices as $practice) : ?>
    	<tr class="roww">
    <?php echo validation_errors('<div class="error">'); ?>
  	<?php echo form_open(); ?>
  	<?php 
  	$price_list = new stdClass;
  	foreach ($price_lists as $list)
  	{
  		if($list->user_id == $practice->user_id)
  		{
  			$price_list = $list;
  		}
  	}

  	if(empty($price_list))
  	{

  		if(!empty($global_settings))
  		{
	  		$price_list->unit_bluecollar =(object) $global_settings->global_price_blueC;
  			$price_list->unit_whitecollar =(object)  $global_settings->global_price_whiteC;	
  		}
  	}
  	?>
    	 <td>
				<div class="form-group">
					<input type="text" class="form-control" name="practice_name" value="<?php echo $practice->first_name.' '.$practice->last_name; ?>">
					<input type="hidden" class="form-control" name="practice_id" value="<?php echo $practice->user_id; ?>">
				</div>
		</td>
		 <td>
				<div class="form-group">
					<input type="number" min="0" class="form-control" name="unit_bluecollar" value="<?php echo !empty($price_list->unit_bluecollar)?$price_list->unit_bluecollar:''; ?>">
				</div>
		</td>
		<td>
				<div class="form-group">
					<input type="number" min="0" class="form-control" name="unit_whitecollar" value="<?php echo !empty($price_list->unit_whitecollar)?$price_list->unit_whitecollar:''; ?>">
				</div>
		</td>

<td>
		<div class="form-group">
			<?php
			echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
			?>
		</div>
</td>
	</form>
	</tr>
<?php 	endforeach; ?>
	</tbody>
	</table>
			

	</div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript">


	var id = 2;
	function chooseSubquestion(val) 
	{

		var element =  document.getElementById(Number(val.id)+Number(1));
		var flag=false;
		if (element == null)
		{
		  flag = true;
		}
		if(val.value!="" && flag)
		{
		document.getElementById("items").insertAdjacentHTML('beforeend', '<tr class="roww">'+
    	 '<td>'+
				'<div class="form-group">'+
					'<input type="text" id="'+id+'" onchange="chooseSubquestion(this)" class="form-control" name="item[]"/>'+
				'</div>'+
		'</td>'+
		 '<td>'+
				'<div class="form-group">'+
					'<textarea name="description[]" class="form-control"></textarea>'+
				'</div>'+
			'</td>'+
		 '<td>'+
				'<div class="form-group">'+
					'<p class="form-control" id="unit_cost'+id+'"></p>'+
				'</div>'+
			'</td>'+
		 '<td>'+
				'<div class="form-group">'+
					'<input type="text" class="form-control" name="quantity[]"/>'+
				'</div>'+
			'</td>'+
		 '<td>'+
				'<div class="form-group">'+
					'<p class="form-control" id="line_total'+id+'"></p>'+
				'</div>'+
		'</td>'+
	'</tr>');
		id++;
	}
}
document.getElementById("client").addEventListener("change", function(){
  var optionVal = $('#client option:selected').val();
		
		console.log(optionVal)
		$.ajax({
			url:"<?php echo base_url('admin/invoices/clientbyid')?>/"+ optionVal,
			type:'post',
			success:function(response){
				var obj = $.parseJSON(response);
				console.log(obj['contacts'][0]['is_owner']);
						$("#client_name").innerHTML="Client Name: "+obj['contacts'][0]['first_name']+" "+obj['contacts'][0]['last_name'];
						$("#client_email").innerHTML="Client Email: "+obj['contacts'][0]['email'];
						$("#client_phone").innerHTML="Client phone: "+obj['contacts'][0]['phone'];
					}
				})
			

});


</script>