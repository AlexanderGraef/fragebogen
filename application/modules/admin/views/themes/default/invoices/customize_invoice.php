

<!-- Begin Page Content -->
<div class="container-fluid">

<?php 
      use InvoiceNinja\Config as NinjaConfig;
      use InvoiceNinja\Models\Client;
      use InvoiceNinja\Models\Invoice;
      use InvoiceNinja\Models\TaxRate;
      $token = $this->db->get_where('users_tokens',array('user_id'=>$this->session->userdata('user_id')))->row();
  
  if($this->InvoiceModel->validate_token_url($token->token_url,$token->token))
  {

      $this->config->set_item('invoice_ninja_token', $token->token);
      $this->config->set_item('invoice_ninja_url', $token->token_url);
      NinjaConfig::setURL($this->config->item('invoice_ninja_url'));
      NinjaConfig::setToken($this->config->item('invoice_ninja_token'));
     // debug_array(Invoice::find(43));
?>

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?php echo $this->lang->line('send_invoices')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('company_list')?></h6> -->
     <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseMyTable" aria-expanded="true" aria-controls="collapseMyTable">
          <i class="fas fa-fw fa-list"></i>
          <span> <?php echo $this->lang->line('send_auto_invoices')?></span>
        </a>
  </div>

  <div class="card-body show" id="collapseMyTable">
  <?php echo validation_errors('<div class="error">'); ?>
  <?php echo form_open(); ?>
	<div class="row mt-5">

		<div class="col-sm-4">
			<div class="form-group">
			
   
				<h3>
				<?php
					echo form_label($this->lang->line('choose_client'), "clients");
				?></h3>
				 <select id='pre-selected-options' required onselect="alert(this)" multiple='multiple' name="clients[]">

				<option disabled>
					Select Client
				</option>
					<?php 
					//debug_array($client_id[0]->client_id);
					foreach($practices as $t) {
						?>
						<?php
						$checked = "";

						if(isset($client_id[0]))
						if($t->id == str_replace(' ', '', $client_id[0]->client_id))
						{
							$checked= "selected";
						}
						echo "<option $checked value='".$t->id."'>".$t->first_name.' '.$t->last_name."</option>";
					}
					 ?>
				</select>
			</div>
		</div>
		<div class="col-sm-4">
		</div>

		<div class="col-sm-4" >
		<div class="form-group">
				<h3><?php
				echo form_label($this->lang->line('discount'), "discount");
				echo form_input("discount", "", array("class" => "form-control","placeholder"=>"Ex. 5%"));
				?></h3>
			</div>
			<div class="form-group" style="max-height: 220px; overflow-x: auto;">
				<h3>Select Date (or Multiple)</h3>
				  <input autocomplete="off" class="form-control" id="multiple-date-select" name="dates" />
				<table  class="table table-bordered" id="table-data" border="1" ></table>
			</div>
		</div>
		<div class="col-sm-3">
		<div class="form-group">
			<?php
			echo form_submit("submit", $this->lang->line('send_invoices'), array("class" => "btn btn-success"));
			?>
		</div>
	</div>
	</div>
	</form>
	</div>
</div>
		<!-- <div class="col-sm-4">
			<div class="container">
				<h3>Select Date (or Multiple)</h3>
				<input type="text" class="form-control date" placeholder="Pick the multiple dates">
			</div>	
		</div> -->
	

	
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('company_list')?></h6> -->
     <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsecustomTable" aria-expanded="true" aria-controls="collapsecustomTable">
          <i class="fas fa-fw fa-list"></i>
          <span><?= $this->lang->line('send_custom_invoice')?>  </span>
        </a>
  </div>
  <div class="card-body collapse" id="collapsecustomTable">
   <?php echo validation_errors('<div class="error">'); ?>
  <?php echo form_open(); ?>
	<div class="row mt-5">
		
        <div class="col-sm-4">
			<div class="form-group">
			
   
				<h3>
				<?php
					echo form_label($this->lang->line('choose_client'), "clients");
				?></h3>
				 <select id='pree-selected-options' required onselect="alert(this)" multiple='multiple' name="clients[]">

				<option disabled>
					Select Client
				</option>
					<?php 
					foreach($practices as $t) {
						?>
						<?php
						$checked = "";

						if(isset($client_id[0]))
						if($t->id == str_replace(' ', '', $client_id[0]->client_id))
						{
							$checked= "selected";
						}
						echo "<option $checked value='".$t->id."'>".$t->first_name.' '.$t->last_name."</option>";
					}
					 ?>
				</select>
			</div>
		</div>
		<div class="col-sm-4">
		</div>

		<div class="col-sm-4" >
		<div class="form-group">
				<h3><?php
				echo form_label($this->lang->line('discount'), "discount");
				echo form_input("discount", "0", array("class" => "form-control","placeholder"=>"Ex. 5%","id"=>"discount","onchange"=>"OninputChange(this.id)","type"=>"number"));
				?></h3>
			</div>
			<div class="form-group" style="max-height: 220px; overflow-x: auto;">
				<h3>Select Date (or Multiple)</h3>
				  <input autocomplete="off" class="form-control" id="multiple-date-select-sec" name="dates" />
				<table  class="table table-bordered" id="table-data-sec" border="1" ></table>
			</div>
		</div>
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?= $this->lang->line('item')?></th>
            <th><?= $this->lang->line('description')?></th>
            <th><?= $this->lang->line('unit_cost')?></th>
            <th><?= $this->lang->line('quantity')?></th>
            <th><?= $this->lang->line('tax')?></th>
            <th><?= $this->lang->line('line_total')?></th>
          </tr>
        </thead>
    <tbody id="items">
    	<tr class="roww">
    	 <td>
				<div class="form-group">
					<input type="text" id="1" required class="form-control" onchange="chooseSubquestion(this)" name="items[]" id="item">
				</div>
		</td>
		 <td>
				<div class="form-group">
					<textarea name="descriptions[]" class="form-control"></textarea>
				</div>
			</td>
		 <td style="width: 130px">
				<div class="form-group">
					<input min="0" type="number" onchange="OninputChange(this.id)" id="unit_cost-1" class="form-control calculate" name="unit_costs[]">
				</div>
			</td>
		 <td style="width: 130px" >
				<div class="form-group">
					<input type="Number" min="0"  onchange="OninputChange(this.id)" id="quantities-1" class="form-control calculate" name="quantities[]">
				</div>
			</td>
		 <td>
				<div class="form-group">
					
					<select class="form-control calculate" name="taxes[]"  onchange="OninputChange(this.id)" id="tax-1" >
						<option></option>
						<?php $taxes = TaxRate::all(); foreach ($taxes as $tax){ 
						echo '<option value="'.$tax->name.':'.$tax->rate.'">'.$tax->name.' '.$tax->rate.'%</option>';
					 } ?>
					</select>
				</div>
		</td>
		 <td>
				<div class="form-group">
					<input type="hidden" id="amounts-1" class="form-control " name="amounts[]">
					<p class="form-control amounts" id="line_total-1"></p>
				</div>
		</td>
	</tr>
	</tbody>
	</table>

	</div>
		<div class="col-sm-3">
			<div class="form-group">
				<?php
				echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
				?>
			</div>
		</div>
		<div class="col-sm-3"></div>
		<div class="col-sm-2">
			<div class="form-group">
					<input type="hidden" name="subtotal_cost" id="subtotal_cost_in">
				Subtotal: <p class="form-control" id="subtotal_cost"></p>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
				Discount: <p class="form-control" id="discounted"></p>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
				Balance Due: <p class="form-control" id="discounted_cost"></p>
			</div>
		</div>

</div>
</form>
</div>

<!-- /.container-fluid -->
<script type="text/javascript">


	var id = 2;
	function chooseSubquestion(val) 
	{

		var element =  document.getElementById(Number(val.id)+Number(1));
		var flag=false;
		if (element == null)
		{
		  flag = true;
		}
		if(val.value!="" && flag)
		{
		document.getElementById("items").insertAdjacentHTML('beforeend', '<tr class="roww">'+
    	 '<td>'+
				'<div class="form-group">'+
					'<input type="text" id="'+id+'" onchange="chooseSubquestion(this)" class="form-control" name="items[]"/>'+
				'</div>'+
		'</td>'+
		 '<td>'+
				'<div class="form-group">'+
					'<textarea name="descriptions[]" class="form-control"></textarea>'+
				'</div>'+
			'</td>'+
		 '<td>'+
				'<div class="form-group">'+
					'<input type="number" onchange="OninputChange(this.id)" name="unit_costs[]" class="form-control" id="unit_cost-'+id+'"></p>'+
				'</div>'+
			'</td>'+
		 '<td>'+
				'<div class="form-group">'+
					'<input type="number" onchange="OninputChange(this.id)" id="quantities-'+id+'" class="form-control" name="quantities[]"/>'+
				'</div>'+
			'</td>'+
		 '<td>'+
				'<div class="form-group">'+
					'<select class="form-control calculate" name="taxes[]"  onchange="OninputChange(this.id)" id="tax-'+id+'">'+
						'<option></option>'+
						'<?php $taxes = Taxrate::all(); foreach ($taxes as $tax){ echo '<option value="'.$tax->name.':'.$tax->rate.'">'.$tax->name.' '.$tax->rate.'%</option>'; } ?>'+
					'</select>'+
				'</div>'+
		'</td><td>'+
				'<div class="form-group">'+
				'<input type="hidden" id="amounts-'+id+'" class="form-control amounts" name="amounts[]"/>'+
					'<p class="form-control" id="line_total-'+id+'"></p>'+
				'</div>'+
		'</td>'+
	'</tr>');
		id++;
	}
}

 function OninputChange (select) {
 	if(select=="discount")
 	{
 		var subtotal_cost = parseInt(document.getElementById("subtotal_cost_in").value);
 		if(subtotal_cost != "")
 		{
 			var dis = document.getElementById("discount").value;
 			if(dis == "")
 				dis = 0;
	 		var discounted = (subtotal_cost/100)*parseInt(dis);
	        document.getElementById("discounted").innerHTML = "€"+discounted.toFixed(2); 
	        document.getElementById("discounted_cost").innerHTML = '€'+(subtotal_cost-discounted).toFixed(2);
	        document.getElementById("subtotal_cost_in").textContent = subtotal_cost-discounted.toFixed(2); 
 		}
 	}
 	else
 	{
            var id = select.split("-")[1];
            var price = document.getElementById("unit_cost-"+id).value;
            var quantity = document.getElementById("quantities-"+id).value;
            var tax = document.getElementById("tax-"+id).value;
            if(tax != "")
            {
            var arr = tax.split(":");
	            tax = arr[1];
            }
            else
            {
            	tax = 0;
            }
            var discount = 0;
            if(document.getElementById("discount").value != "")
            {
	            discount = document.getElementById("discount").value;        	
            }

            if(price != "" && quantity != "")
            {
	            var a2 = parseInt(price) * parseInt(quantity);
	            a2 += a2*(tax)/100;
	            document.getElementById("amounts-"+id).value = a2;
            	document.getElementById("line_total-"+id).innerHTML = "€ "+a2;
				var amounts = document.getElementsByClassName('amounts');
				var subtotal_cost = 0;
				for(i = 0; i < amounts.length; i++)
				{
					if(document.getElementById("amounts-"+(parseInt(i)+parseInt(1))).value != "")
					{
						price = document.getElementById("amounts-"+(parseInt(i)+parseInt(1))).value;
						subtotal_cost = parseInt(price) + parseInt(subtotal_cost);
					}
					
				}
				var disc = (parseInt(subtotal_cost)/100)*parseInt(discount);
				var a3 = subtotal_cost.toFixed(2);
            	document.getElementById("subtotal_cost").innerHTML = "€"+a3;
            	document.getElementById("subtotal_cost_in").value = subtotal_cost;
            	document.getElementById("discounted").innerHTML = "€"+disc.toFixed(2);
            	var a4 = (subtotal_cost-disc).toFixed(2);
            	document.getElementById("discounted_cost").textContent = '€'+a4;
            }
            else
            {
            	document.getElementById("line_total-"+id).innerHTML = "";
            }
		}
     }
    
// document.getElementsByClassName("calculate").addEventListener("change", function(){
//   var optionVal = $(this).id;
// 		alert(optionVal);
// 		console.log(optionVal)
// 		$.ajax({
// 			url:"<?php echo base_url('admin/invoices/clientbyid')?>/"+ optionVal,
// 			type:'post',
// 			success:function(response){
// 				var obj = $.parseJSON(response);
// 				console.log(obj['contacts'][0]['is_owner']);
// 						$("#client_name").innerHTML="Client Name: "+obj['contacts'][0]['first_name']+" "+obj['contacts'][0]['last_name'];
// 						$("#client_email").innerHTML="Client Email: "+obj['contacts'][0]['email'];
// 						$("#client_phone").innerHTML="Client phone: "+obj['contacts'][0]['phone'];
// 					}
// 				})
			

// });


</script>


<?php }
else
{
redirect('admin/invoices/set_token');
}

 ?>


</div>
