<!--Begin Page Content -->
<div class="container-fluid">


<!-- Page Heading -->


<?php 
      use InvoiceNinja\Config as NinjaConfig;
      use InvoiceNinja\Models\Client;
      $token = $this->db->get_where('users_tokens',array('user_id'=>$this->session->userdata('user_id')))->row();
  
  if($this->InvoiceModel->validate_token_url($token->token_url,$token->token))
  {

      $this->config->set_item('invoice_ninja_token', $token->token);
      $this->config->set_item('invoice_ninja_url', $token->token_url);
      NinjaConfig::setURL($this->config->item('invoice_ninja_url'));
      NinjaConfig::setToken($this->config->item('invoice_ninja_token'));

?>


    <div class="d-sm-flex align-items-center justify-content-between mb-4">
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('billing_invoices')?></h1>
  <a class="btn btn-primary btn-sm" style="display: none" href="invoices/create_invoice"><?= $this->lang->line('create_new_invoice')?></a>
  <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/invoices/customize_invoice"><?= $this->lang->line('send_custom_invoice')?></a>
    </div>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('company_list')?></h6> -->
     <a class="nav-link" href="#" data-toggle="collapse" data-target="#unpaidcollapseMyTable" aria-expanded="true" aria-controls="unpaidcollapseMyTable">
          <i class="fas fa-fw fa-list"></i>
          <span><?= $this->lang->line('unpaid_invoices')?>  </span>
        </a>
  </div>
  <div class="card-body show" id="unpaidcollapseMyTable">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?= $this->lang->line('invoice_id')?></th>
            <th><?= $this->lang->line('client_name')?></th>
            <th><?= $this->lang->line('amount')?></th>
            <th><?= $this->lang->line('billing_date')?></th>
            <th><?= $this->lang->line('status')?></th>
            <th class="text-right"><?= $this->lang->line('actions')?></th>
          </tr>
        </thead>
        <tbody>
           <?php
         
		       $i = 1;
           if(count($unpaid_invoices)>0)
        		foreach($unpaid_invoices as $invoice) : ?>
        			<tr class="roww">
                <td><?php echo $invoice->id ?></td>
                <?php
                $status = "";
                switch ($invoice->invoice_status_id)
                {
                  case 1:
                    $status = "<span style='background-color: gray'> Draft </span>";
                    break; 
                    case 2:
                    $status = "<span style='background-color: lightgreen'> Sent </span>";
                    break;
                   case 3:
                    $status = "<span style='background-color: lightyellow'> viewed </span>";
                    break;
                   case 5:
                    $status = "<span style='background-color: green'> paid </span>";
                    break;
                  default:
                   
                    break;
                }
             //   debug_array($invoice);
                $client = Client::find($invoice->client_id);
                ?>
                
                <td><?php echo $client->name ?></a></td>
                <td>€ <?php echo $invoice->amount ?></a></td>
                <td><?php echo $invoice->due_date ?></a></td>
                <td><?php echo $status ?></a></td>
                   <td class="text-right">
                        
                       
                        <a class="btn btn-success btn-sm" target="_blank" href="<?php echo $invoice->invitations[0]->link; ?>"><?= $this->lang->line('view_invoice')?></a>
                       
                      <!--  <form action="invoices/send_invoice_reminder" method="post" accept-charset="utf-8" style="float:right; margin-left: 5px">
                         <input type="hidden" name="link" value="<?php echo $invoice->invitations[0]->link; ?>">
                         <input type="hidden" name="email" value="<?php echo $client->contacts[0]->email; ?>"> 
                         <input type="submit" name="submit" value="Send Reminder" class="btn btn-warning btn-sm">                   
                        </form> -->
                    <!-- <form action="invoices/mark_invoice_paid" method="post" accept-charset="utf-8" style="float:right; margin-left: 5px">
                         <input type="hidden" name="id" value="<?php echo $invoice->id; ?>">
                         <input type="submit" name="submit" value="Mark as Paid" class="btn btn-danger btn-sm">
                        </form> -->
                    </td>              
      			</tr>
      			<?php
      			$i++;
      		endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- Practice Accounts companies list -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('company_list')?></h6> -->
     <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsependingTable" aria-expanded="true" aria-controls="collapsependingTable">
          <i class="fas fa-fw fa-list"></i>
          <span><?= $this->lang->line('clearance_pending_invoices')?>  </span>
        </a>
  </div>
  <div class="card-body collapse" id="collapsependingTable">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?= $this->lang->line('invoice_id')?></th>
            <th><?= $this->lang->line('client_name')?></th>
            <th><?= $this->lang->line('amount')?></th>
            <th><?= $this->lang->line('billing_date')?></th>
            <th><?= $this->lang->line('status')?></th>
            <th class="text-right"><?= $this->lang->line('actions')?></th>
          </tr>
        </thead>
        <tbody>
           <?php
         
           $i = 1;
           if(count($clearance_pending_invoices)>0)
            foreach($clearance_pending_invoices as $invoice) : ?>
              <tr class="roww">
                <td><?php echo $invoice->id ?></a></td>
                <?php
                 $status = "";
                switch ($invoice->invoice_status_id)
                {
                  case 1:
                    $status = "<span style='background-color: gray'> Draft </span>";
                    break; 
                    case 2:
                    $status = "<span style='background-color: lightgreen'> Sent </span>";
                    break;
                   case 3:
                    $status = "<span style='background-color: lightyellow'> viewed </span>";
                    break;
                   case 5:
                    $status = "<span style='background-color: green'> paid </span>";
                    break;
                  default:
                   
                    break;
                }
             //   debug_array($invoice);
                $client = Client::find($invoice->client_id);
                ?>
                
                <td><?php echo $client->name ?></a></td>
                <td>€ <?php echo $invoice->amount ?></a></td>
                <td><?php echo $invoice->due_date ?></a></td>
                <td><?php echo $status ?></a></td>
                   <td class="text-right">
                        
                       
                        <a class="btn btn-success btn-sm" target="_blank" href="<?php echo $invoice->invitations[0]->link; ?>"><?= $this->lang->line('view_invoice')?></a>
                     <!--    <form action="invoices/send_invoice_reminder" method="post" accept-charset="utf-8" style="float:right; margin-left: 5px">
                         <input type="hidden" name="id" value="<?php echo $invoice->id; ?>">
                         <input type="hidden" name="email" value="<?php echo $client->contacts[0]->email; ?>"> 
                         <input type="submit" name="submit" value="Send Invoice" class="btn btn-primary btn-sm">                   
                        </form> -->
                   <!--     <form action="invoices/mark_invoice_paid" method="post" accept-charset="utf-8" style="float:right; margin-left: 5px">
                         <input type="hidden" name="id" value="<?php echo $invoice->id; ?>">
                         <input type="submit" name="submit" value="Mark as Paid" class="btn btn-danger btn-sm">
                        </form> -->

                    </td>              
            </tr>
            <?php
            $i++;
          endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!--End Practice Accounts companies list -->

<!-- Other companies list -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('company_list')?></h6> -->
     <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsepaidTable" aria-expanded="true" aria-controls="collapsepaidTable">
          <i class="fas fa-fw fa-list"></i>
          <span><?= $this->lang->line('paid_invoices')?>  </span>
        </a>
  </div>
  <div class="card-body collapse" id="collapsepaidTable">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
           <tr>
            <th><?= $this->lang->line('invoice_id')?></th>
            <th><?= $this->lang->line('client_name')?></th>
            <th><?= $this->lang->line('amount')?></th>
            <th><?= $this->lang->line('billing_date')?></th>
            <th><?= $this->lang->line('status')?></th>
            <th class="text-right"><?= $this->lang->line('actions')?></th>
          </tr>
        </thead>
        <tbody>
           <?php
         
           $i = 1;
           if(count($paid_invoices)>0)
            foreach($paid_invoices as $invoice) : ?>
              <tr class="roww">
                <td><?php echo $invoice->id ?></a></td>
                <?php
                 $status = "";
                switch ($invoice->invoice_status_id)
                {
                  case 1:
                    $status = "<span style='background-color: gray'> Draft </span>";
                    break; 
                    case 2:
                    $status = "<span style='background-color: lightgreen'> Sent </span>";
                    break;
                   case 3:
                    $status = "<span style='background-color: lightyellow'> viewed </span>";
                    break;
                   case 5:
                    $status = "<span style='background-color: green'> paid </span>";
                    break;
                  default:
                   
                    break;
                }
             //   debug_array($invoice);
                $client = Client::find($invoice->client_id);
                ?>
                
                <td><?php echo $client->name ?></a></td>
                <td>€ <?php echo $invoice->amount ?></a></td>
                <td><?php echo $invoice->due_date ?></a></td>
                <td><?php echo $status ?></a></td>
                   <td class="text-right">
                       
                        <a class="btn btn-success btn-sm" target="_blank" href="<?php echo $invoice->invitations[0]->link; ?>"><?= $this->lang->line('view_invoice')?></a>

                    </td>              
            </tr>
            <?php
            $i++;
          endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!--End Other companies list -->
<?php }
else
{
redirect('admin/invoices/set_token');
}

 ?>


</div>
<!-- /.container-fluid