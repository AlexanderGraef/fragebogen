<!-- Begin Page Content -->
<div class="container-fluid">
  <div class="card-body show" id="collapseMyTable">
    <div class="card shadow mb-4">
      <?php echo form_open(); ?>
        <div class="row mt-5">

        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">

          <div class="form-group">
            <select required name="practice_id">
              <option disabled>
              Select Practice
            </option>
              <?php 
              foreach($practices as $t) {
                ?>
                <?php

                  echo "<option value='".$t->id."'>".$t->first_name.' '.$t->last_name."</option>";
                
              }
               ?>
            
            </select>
          </div>
      
          <div class="form-group">
          
       
            <h3>
            <?php
              echo form_label($this->lang->line('choose_client'), "clients");
            ?></h3>
             <select id='pre-selected-options' required onselect="alert(this)" multiple='multiple' name="members[]">

            <option disabled>
              Select Client
            </option>
              <?php 
             // debug_array($members);
              foreach($members as $t) {
                ?>
                <?php

                  echo "<option value='".$t->id."'>".$t->first_name.' '.$t->last_name."</option>";
                
              }
               ?>
            </select>
          </div>
          <div class="form-group">
            <?php
              echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
            ?>
        </div>
        </div>   
        </form>
    </div>
  </div>
</div>  

