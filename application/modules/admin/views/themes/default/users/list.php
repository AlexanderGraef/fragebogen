<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><?php echo lang('index_heading');?></h1>
    </div>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
<!--     <h6 class="m-0 font-weight-bold text-primary"><?php echo lang('index_subheading');?></h6>
 --> 
 <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseMyTable" aria-expanded="true" aria-controls="collapseMyTable">
          <i class="fas fa-fw fa-list"></i>
          <span><?php echo lang('index_subheading');?> </span>
        </a>
  </div>
  <div class="card-body show" id="collapseMyTable">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?php echo lang('index_fname_th');?></th>
            <th><?php echo lang('index_lname_th');?></th>
            <th><?php echo lang('index_email_th');?></th>
            <th><?php echo lang('index_groups_th');?></th>
            <th><?php echo lang('index_status_th');?></th>
            <th><?php echo lang('index_action_th');?></th>
          </tr>
        </thead>
        <tbody>
        <?php
         foreach ($users as $user):
        if($user->is_deleted == 0) 
        { ?>
		<tr>
            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
			<td>
				<?php foreach ($this->ion_auth->get_users_groups($user->user_id)->result() as $group):?>
					<?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ; ?><br />
                <?php endforeach?>
			</td>
			<td><?php if($user->is_deleted == 0){ echo ($user->active) ? anchor("auth/deactivate/".$user->user_id, lang('index_active_link')) : anchor("auth/activate/". $user->user_id, lang('index_inactive_link')); } else { echo "Deleted"; }?></td>
			<td><?php if($user->is_deleted == 0) {echo anchor("auth/edit_user/".$user->user_id, 'Edit').' / '; } ?> <?php echo anchor("auth/delete_user/".$user->user_id, 'Delete') ;?></td>
		</tr>
	    <?php  }
       endforeach;?>
        </tbody>
      </table>
    </div>
  </div>
</div>



<!-- Practice Accounts Users -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><?php echo lang('index_practice_heading');?></h1>
    </div>

    

<!-- Admin Users and Practice  -->
<?php if($admin_users == TRUE){ ?>
<div class="card shadow mb-4">
  <div class="card-header py-3">
 <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTable" aria-expanded="true" aria-controls="collapseTable">
          <i class="fas fa-fw fa-list"></i>
          <span><?php echo lang('index_subheading_practice');?> </span>
        </a>
  </div>
  <div class="card-body collapse" id="collapseTable">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?php echo lang('index_fname_th');?></th>
            <th><?php echo lang('index_lname_th');?></th>
            <th><?php echo lang('index_email_th');?></th>
            <th><?php echo lang('index_groups_th');?></th>
            <th><?php echo lang('index_status_th');?></th>
            <th><?php echo lang('index_action_th');?></th>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($practices as $user):?>
    <tr>
            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
      <td>
        <?php foreach ($this->ion_auth->get_users_groups($user->user_id)->result() as $group):?>
          <?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
                <?php endforeach?>
      </td>
      <td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->user_id, lang('index_active_link')) : anchor("auth/activate/". $user->user_id, lang('index_inactive_link'));?></td>
      <td><?php echo anchor("auth/edit_user/".$user->user_id, 'Edit').' | '.anchor("admin/invoices/customize_invoice/".$user->user_id, 'Generate Invoice') ;?></td>
    </tr>
      <?php endforeach;?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<?php } ?>

<!-- End Admin Users and Practice  -->





    <?php if($admin_users == FALSE){ ?>
<?php foreach ($practices as $practice):?>
  <?php foreach ($this->ion_auth->get_users_groups($practice->user_id)->result() as $group):?>
  <?php if($group->id == 3){?>
<div class="card shadow mb-4">
  <div class="card-header py-3">
 <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseTable<?php echo $practice->user_id; ?>" aria-expanded="true" aria-controls="collapseTable<?php echo $practice->user_id; ?>">
          <i class="fas fa-fw fa-list"></i>
          <span><?php echo $practice->first_name.' '.$practice->last_name;?> </span>
        </a>
  </div>
  <div class="card-body show" id="collapseTable<?php echo $practice->user_id; ?>">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?php echo lang('index_fname_th');?></th>
            <th><?php echo lang('index_lname_th');?></th>
            <th><?php echo lang('index_email_th');?></th>
            <th><?php echo lang('index_groups_th');?></th>
            <th><?php echo lang('index_status_th');?></th>
            <th><?php echo lang('index_action_th');?></th>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($this->AdminModel->practice_users($practice->user_id) as $user):?>

    <tr>
            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
      <td>
        <?php foreach ($this->ion_auth->get_users_groups($user->user_id)->result() as $group):?>
          <?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8');?><br />
                <?php endforeach?>
      </td>
      <td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->user_id, lang('index_active_link')) : anchor("auth/activate/". $user->user_id, lang('index_inactive_link'));?></td>
      <td><?php echo anchor("auth/edit_user/".$user->user_id, 'Edit') ;?></td>
    </tr>
      <?php endforeach;?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php 
}
endforeach;?>
<?php endforeach; }?>

<!-- End Practice Accounts Users -->

</div>