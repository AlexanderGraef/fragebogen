<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><?= $this->lang->line("warning_email_dates_set")?></h1>
      </div>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line("warning_email_dates_set")?></h6>
  </div>
  <div class="card-body">
  <?php echo validation_errors('<div class="error">'); ?>
    <?php echo form_open(); ?>
    <div class="row mt-5">
        <div class="col-sm-5">
            <div class="form-group">
                <?php
                echo form_label($this->lang->line("date_one"), "dateOne");
                ?>
                <input type="date" name="dateOne" class="form-control" value="<?php echo $wDate['dateOne']?>" />
            </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group">
                <?php
                echo form_label($this->lang->line("date_two"), "dateTwo");
                ?>
                <input type="date" name="dateTwo" class="form-control" value="<?php echo $wDate['dateTwo']?>" />
            </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group">
                <?php
                echo form_label($this->lang->line("date_three"), "dateThree");
                ?>
                <input type="date" name="dateThree" class="form-control" value="<?php echo $wDate['dateThree']?>" />
            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            <?php
            echo form_submit("submit", $this->lang->line("save"), array("class" => "btn btn-success"));
            ?>
            
        </div>
    </div>
    </form>
  </div>
</div>

</div>
<!-- /.container-fluid -->