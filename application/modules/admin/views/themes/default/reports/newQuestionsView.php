<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><?= $this->lang->line("new_questions_email") ?></h1>
      </div>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line("new_questions_email") ?></h6>
  </div>
  <div class="card-body">
  <div class="row mt-5">
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
                echo form_label($this->lang->line('emails_to_send'), "emailsToSend");
			?>
            <?php
            $users = array();
            foreach($notifs as $not) {
              if (!in_array($not->userId, $users)) {
                $questions = $this->AdminModel->getNewQuestionsByUserId($not->userId);
                
                if(count((array)$questions) > 0) { 
                $company = $this->AdminModel->getCompanyByUserId($not->userId); 
                if(!empty($company)) { ?>
                <h3><?php echo $this->lang->line("new_questions_by_company"); ?> - <?php echo $company[0]->companyName; ?> </h3>
                <div class="alert alert-primary">
                <?php
                  foreach ($questions as $question) {
                    $msg = "<br>";
                      $msg .= $question->question;
                      echo $msg;
                  } ?>
                  </div> 
                  <?php
                array_push($users, $not->userId);
              }
            } 
          } 
        } ?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
		    <a href="<?= base_url() ?>admin/questionnare/sendNewQuestionsEmail" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-envelope fa-sm text-white-50"></i> <?= $this->lang->line("new_questions_email")?></a>  
	  	</div>
    </div>
	</div>
</form>
  </div>
</div>

</div>