<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><?php echo $this->lang->line('company_report_generate')?></h1>
      </div>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo $this->lang->line('company_report_generate')?></h6>
  </div>
  <div class="card-body">
  <?php echo validation_errors('<div class="error">'); ?>
    <?php echo form_open('admin/reports/generateCompanyReport'); ?>
    <div class="row mt-5">
        <div class="col-sm-5">
            <div class="form-group">
                <?php
                echo form_label($this->lang->line('from_date'), "fromDate");	
                ?>
                <input type="date" required name="dateFrom" class="form-control">
            </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group">
                <?php
                echo form_label($this->lang->line('to_date'), "toDate");
                ?>
                <input type="date" required name="dateTo" class="form-control">
            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            <?php
            echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
            ?>
            
        </div>
    </div>
    </form>
  </div>
</div>

</div>
<!-- /.container-fluid -->