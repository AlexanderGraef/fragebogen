<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><?= $this->lang->line("reports")?></h1>
      </div>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line("reports")?></h6>
  </div>
  <div class="card-body">
        <a href="<?= base_url() ?>admin/reports/newQuestionsEmailView" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-envelope fa-sm text-white-50"></i> <?= $this->lang->line("new_questions_email")?></a>
        <a href="<?= base_url() ?>admin/reports/company" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> <?= $this->lang->line("company_report_generate")?></a>
        <a href="<?= base_url() ?>admin/reports/warningEmailsDates" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> <?= $this->lang->line("warning_email_dates")?></a>
  </div>
</div>

</div>
<!-- /.container-fluid -->