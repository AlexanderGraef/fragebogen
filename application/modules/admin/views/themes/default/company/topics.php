<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><?php echo $company['companyName']?></h1>
    </div>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo $this->lang->line('choose_one_topic')?></h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th><?php echo $this->lang->line('topic')?></th>
            <th><?php echo $this->lang->line('progress')?></th>
          </tr>
        </thead>
        <tbody>
        <tbody>
            <?php
                $company = $this->AdminModel->getCompany($this->uri->segment(4));
                $userId = $company['userId'];
                
                $i = 1;
                foreach($topics as $topic) :
                $questionsByTopic = $this->QuestionnareModel->getQuestions($topic->id);
                $answeredN = 0;
                foreach ($questionsByTopic as $qt) {
                    $answer = $this->QuestionnareModel->getAnswer($qt->id, $userId);
                    if(isset($answer)) {
                        $answeredN++;
                    }
                }
                $answeredQuestions = $this->QuestionnareModel->answeredQuestionsNumber($topic->id, $this->session->userdata('id'));
                $questionsNumber = $this->QuestionnareModel->numberOfQuestions($topic->id);
                ?>
            <tr>
                <th scope="row"><?php echo $i ?></th>
                <td>    
                    <a href="<?php echo base_url().'admin/company/topic/'.$topic->id.'/'.$company['companyId'] ?>"><?php echo $topic->topicName ?></a> 
                <?php
                    foreach ($notifications as $n) {
                        if($topic->id == $n->topicId) {
                            echo '<span class="badge badge-success">'.$this->lang->line("new").'</span>';
                            break;
                        }			
                    }
                    ?></td>
                <td><?php echo $answeredN. ' / '. $questionsNumber ?></td>
            </tr>
            <?php
            $i++;
            endforeach; ?>
        </tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->