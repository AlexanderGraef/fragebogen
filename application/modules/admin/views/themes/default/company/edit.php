<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('edit_company')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $company['companyName']?></h6>
  </div>
  <div class="card-body">
  <?php echo validation_errors('<div class="error">'); ?>
  <?php echo form_open(); ?>
	<div class="row mt-5">
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_name'), "companyName");
				echo form_input("companyName", $company['companyName'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_street'), "companyStreet");
				echo form_input("companyStreet", $company['companyStreet'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_house_number'), "companyHouseNumber");
				echo form_input("companyHouseNumber", $company['companyHouseNumber'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_zip'), "companyZip");
				echo form_input("companyZip", $company['companyZip'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_city'), "companyCity");
				echo form_input("companyCity", $company['companyCity'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_phone'), "companyPhone");
				echo form_input("companyPhone", $company['companyPhone'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_website'), "companyWebsite");
				echo form_input("companyWebsite", $company['companyWebsite'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php
				echo form_label($this->lang->line('latest_number_of_blue_collars'), "numberOfBlueCollar");
				echo form_input("numberOfBlueCollar", $company['name'], array("class" => "form-control", "id" => "numberOfBlueCollar"));
			?>
			<!-- <input class="blueCollar" type="hidden" name="numberOfBlueCollar" value="" /> -->
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php

				echo form_label($this->lang->line('latest_number_of_white_collars'), "numberOfWhiteCollar");
				echo form_input("numberOfWhiteCollar", $company['whiteCollarName'], array("class" => "form-control", "id" => "numberOfWhiteCollar"));
			?>
			<!-- <input class="whiteCollar" type="hidden" name="numberOfWhiteCollar" value="" /> -->
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php
            $options = array();
            foreach($bgs as $bg) {
                $options[$bg->id] = $bg->bg;
            }
				echo form_label($this->lang->line('number_of_bg'), "numberOfBg");
				echo form_dropdown("numberOfBg", $options, $company['numberOfBg'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('safety_person_name'), "safetyPersonName");
				echo form_input("safetyPersonName", $company['safetyPersonName'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('safety_person_email'), "safetyPersonEmail");
				echo form_input("safetyPersonEmail", $company['safetyPersonEmail'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('safety_person_phone'), "safetyPersonPhone");
				echo form_input("safetyPersonPhone", $company['safetyPersonPhone'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('direct_adresse_for_osha_names'), "directAdresseeForOshaNames");
				echo form_input("directAdresseeForOshaNames", $company['directAdresseeForOshaNames'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
     <div class="col-sm-10">
    	
		<div class="form-group">
			<label class="label form_label">
					Extra Details
			</label>
			<textarea name="extra_text" class="form-control"><?php echo $company['extra_text']; ?></textarea>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('direct_adresse_for_osha_email'), "directAdresseeForOshaEmail");
				echo form_input("directAdresseeForOshaEmail", $company['directAdresseeForOshaEmail'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('direct_adresse_for_osha_number'), "directAdresseeForOshaNumber");
				echo form_input("directAdresseeForOshaNumber", $company['directAdresseeForOshaNumber'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php
				echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
			?>
	  	</div>
    </div>
	</div>
</form>

<div class="col-sm-5">
		<div class="form-group">
		<?php
		$popupChecked = "";
		if($company['extraQuestionPopupChecked'] == 1) {
			$popupChecked = "Yes";
		} elseif($company['extraQuestionPopupChecked'] == 2) {
			$popupChecked = "No";
		} else {
			$popupChecked = "Not Checked";
		}?>
			<?= $this->lang->line('less_than_twenty_notice_choosen')?>: <strong><?php echo $popupChecked; ?></strong>
	  	</div>
    </div>
<h4 class="mt-5"><?php echo $this->lang->line('number_of_blue_collars_in_past_time')?></h4>
<table class="table">
	<thead>
	<tr>
		<th scope="col"><?php echo $this->lang->line('number_of_blue_collar')?></th>
		<th scope="col"><?php echo $this->lang->line('date_added')?></th>
	</tr>
	</thead>
	<tbody>
<?php
foreach ($blueCollars as $bCollar) : ?>
	<tr class="roww">
		<td><?php echo $bCollar->name ?></td>
		<td><?php echo $bCollar->year ?></td>
		<td>
		<a class="btn btn-danger btn-sm" id="deleteBCollar" data-id="<?php echo $bCollar->id ?>" href="#">Delete</a>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
</table>
<h4 class="mt-5"><?php echo $this->lang->line('number_of_white_collars_in_past_time')?></h4>
<table class="table">
	<thead>
	<tr>
		<th scope="col"><?php echo $this->lang->line('number_of_white_collar')?></th>
		<th scope="col"><?php echo $this->lang->line('date_added')?></th>
	</tr>
	</thead>
	<tbody>
	<?php
	foreach ($whiteCollars as $wCollar) : ?>
		<tr class="roww">
			<td><?php echo $wCollar->whiteCollarName ?></td>
			<td><?php echo $wCollar->whiteCollarYear ?></td>
		    <td>
				<a class="btn btn-danger btn-sm" id="deleteWCollar" data-id="<?php echo $wCollar->id ?>" href="#"><?= $this->lang->line('delete')?></a>
		    </td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
  </div>
</div>

</div>
<!-- /.container-fluid -->