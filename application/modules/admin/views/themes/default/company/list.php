<!--Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('company_list')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('company_list')?></h6> -->
     <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseMyTable" aria-expanded="true" aria-controls="collapseMyTable">
          <i class="fas fa-fw fa-list"></i>
          <span><?= $this->lang->line('my_company_list')?>  </span>
        </a>
  </div>
  <div class="card-body show" id="collapseMyTable">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?= $this->lang->line('company_name')?></th>
            <th><?= $this->lang->line('owner_name')?></th>
            <th class="text-right"><?= $this->lang->line('actions')?></th>
          </tr>
        </thead>
        <tbody>
           <?php

		       $i = 1;
        		foreach($companies as $c) : ?>
        			<tr class="roww <?php if($c->seized == 1):?>table-danger <?php endif;?>">
                <td><?php echo $c->companyName ?></td>
        				<td><?php echo $c->first_name.' '.$c->last_name ?></td>
                   <td class="text-right">
                        <a class="btn btn-success btn-sm" href="<?php echo base_url().'admin/company/edit/'.$c->companyid ?>"><?= $this->lang->line('edit')?></a>
                        <a class="btn btn-danger btn-sm" id="deleteCompany" data-id="<?php echo $c->companyid ?>" href="<?php echo base_url().'admin/deletecompany/'.$c->companyid ?>"><?= $this->lang->line('delete')?></a>
                        <a class="btn btn-primary btn-sm" href="<?php echo base_url().'admin/company/topics/'.$c->companyid ?>"><?= $this->lang->line('topics')?></a>
                       <!--  <a class="btn btn-primary btn-sm" href="<?php echo base_url().'admin/company/verfication_email/'.$c->id ?>"><?= $this->lang->line('email_reports')?></a> -->
                        
                       <form action="company/verfication_email" method="post" accept-charset="utf-8" style="float:right; margin-left: 5px">
                         <input type="hidden" name="id" value="<?php echo $c->companyid; ?>">
                         <input type="hidden" name="email" value="<?php echo $c->directAdresseeForOshaEmail; ?>"> 
                         <input type="submit" name="submit" value="Send Email" class="btn btn-primary btn-sm">                   
                        </form>

                    </td>              
      			</tr>
      			<?php
      			$i++;
      		endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- Practice Accounts companies list -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('company_list')?></h6> -->
     <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePracticingTable" aria-expanded="true" aria-controls="collapsePracticingTable">
          <i class="fas fa-fw fa-list"></i>
          <span><?= $this->lang->line('practice_company_list')?>  </span>
        </a>
  </div>
  <div class="card-body collapse" id="collapsePracticingTable">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?= $this->lang->line('company_name')?></th>
            <th><?= $this->lang->line('owner_name')?></th>
            <th><?= $this->lang->line('created_by')?></th>
            <th class="text-right"><?= $this->lang->line('actions')?></th>
          </tr>
        </thead>
        <tbody>
           <?php
           $i = 1;
            foreach($practicingcompanies as $c) : ?>
            	
              <tr class="roww <?php if($c->seized == 1):?>table-danger <?php endif;?>">
                <td><?php echo $c->companyName ?></a></td>
                  <td><?php echo $c->first_name.' '.$c->last_name ?></a></td>
                   <?php $practice_user = $this->AdminModel->getUserById($c->user_id); ?>

                 <?php $practiceaccount_id = $this->db->get_where('practicing_members',array("createduser_id"=>$practice_user['id']))->result(); ?>
                 <?php $practice_owner = $this->AdminModel->getUserById($practiceaccount_id[0]->practiceaccount_id); ?>

                <td><?php echo $practice_owner['first_name'].' '.$practice_owner['last_name']; ?></a></td>
                      <td class="text-right">
                        <a class="btn btn-success btn-sm" href="<?php echo base_url().'admin/company/edit/'.$c->companyid ?>"><?= $this->lang->line('edit')?></a>
                        <a class="btn btn-danger btn-sm" id="deleteCompany" data-id="<?php echo $c->companyid ?>" href="<?php echo base_url().'admin/deletecompany/'.$c->companyid ?>"><?= $this->lang->line('delete')?></a>
                        <a class="btn btn-primary btn-sm" href="<?php echo base_url().'admin/company/topics/'.$c->companyid ?>"><?= $this->lang->line('topics')?></a>
                       <!--  <a class="btn btn-primary btn-sm" href="<?php echo base_url().'admin/company/verfication_email/'.$c->id ?>"><?= $this->lang->line('email_reports')?></a> -->
                        
                       <form action="company/verfication_email" method="post" accept-charset="utf-8" style="float:right; margin-left: 5px">
                         <input type="hidden" name="id" value="<?php echo $c->companyid; ?>">
                         <input type="hidden" name="email" value="<?php echo $c->directAdresseeForOshaEmail; ?>">                          <input type="submit" name="submit" value="Send Email" class="btn btn-primary btn-sm">                   
                        </form>

                    </td>          
            </tr>
            <?php
            $i++;
          endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!--End Practice Accounts companies list -->

<!-- Other companies list -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('company_list')?></h6> -->
     <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOtherTable" aria-expanded="true" aria-controls="collapseOtherTable">
          <i class="fas fa-fw fa-list"></i>
          <span><?= $this->lang->line('other_company_list')?>  </span>
        </a>
  </div>
  <div class="card-body collapse" id="collapseOtherTable">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?= $this->lang->line('company_name')?></th>
            <th><?= $this->lang->line('owner_name')?></th>
            <th class="text-right"><?= $this->lang->line('actions')?></th>
          </tr>
        </thead>
        <tbody>
           <?php
           $i = 1;
            foreach($othercompanies as $c) : ?>
              <tr class="roww <?php if($c->seized == 1):?>table-danger <?php endif;?>">
                <td><?php echo $c->companyName ?></a></td>
                  <td><?php echo $c->first_name.' '.$c->last_name ?></a></td>
                       <td class="text-right">
                        <a class="btn btn-success btn-sm" href="<?php echo base_url().'admin/company/edit/'.$c->companyid ?>"><?= $this->lang->line('edit')?></a>
                        <a class="btn btn-danger btn-sm" id="deleteCompany" data-id="<?php echo $c->companyid ?>" href="<?php echo base_url().'admin/deletecompany/'.$c->companyid ?>"><?= $this->lang->line('delete')?></a>
                        <a class="btn btn-primary btn-sm" href="<?php echo base_url().'admin/company/topics/'.$c->companyid ?>"><?= $this->lang->line('topics')?></a>
                       <!--  <a class="btn btn-primary btn-sm" href="<?php echo base_url().'admin/company/verfication_email/'.$c->id ?>"><?= $this->lang->line('email_reports')?></a> -->
                        
                       <form action="company/verfication_email" method="post" accept-charset="utf-8" style="float:right; margin-left: 5px">
                         <input type="hidden" name="id" value="<?php echo $c->companyid; ?>">
                         <input type="hidden" name="email" value="<?php echo $c->directAdresseeForOshaEmail; ?>">                          <input type="submit" name="submit" value="Send Email" class="btn btn-primary btn-sm">                   
                        </form>

                    </td>              
            </tr>
            <?php
            $i++;
          endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!--End Other companies list -->

</div>
<!-- /.container-fluid