<div class="container-fluid">
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><h1><?php echo lang('settings');?></h1></h6>
  </div>
  <div class="card-body">

<?php 

?>
<?php echo form_open();?>
<div class="row">
    <div class="col-md-5">
          <div class="form-group">

                <?php echo lang('create_user_fname_label', 'first_name');?> <br />
                <?php echo form_input($first_name);?>
                
          </div>
    </div>
    <div class="col-md-5">
          <div class="form-group">

                <?php echo lang('create_user_lname_label', 'last_name');?> <br />
                <?php echo form_input($last_name);?>
                
          </div>
    </div>
</div>
  <div class="row">
    <div class="col-md-5">
          <div class="form-group">
            <?php echo lang('create_user_email_label', 'email');?> <br />
            <?php echo form_input($email);?>
          </div>
        </div>
   
    <div class="col-md-5">
        <div class="form-group">
          <?php echo lang('create_user_phone_label', 'phone');?> <br />
          <?php echo form_input($phone);?>
        </div>
      </div>
</div>
<div class="row">
  <div class="col-md-10">

    <div id="address">
      <h4>Address</h4>
      <div class="form-group row">
        <div class="col-sm-6 col-md-2 mb-3 mb-sm-0">
          <label>House / Suite / Bldg. No</label>
          <?php echo form_input($house_no);?>
        </div>
        <div class="col-sm-6 col-md-4 ">
          <label>Street No</label>
          <?php echo form_input($street_no);?>
        </div>
        <div class="col-sm-6 col-md-4 ">
        <label>City</label>
          <?php echo form_input($city);?>
        </div>
    
        <div class="col-sm-6 col-md-2 mb-3 mb-sm-0">
         <label>ZIP code</label>
          <?php echo form_input($zip_code);?>
        </div>
      </div>
    </div>
  </div>
</div>
      <div class="row">
        <div class="col-md-5 ">
          <div class="form-group">
            <?php echo lang('create_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <?php echo lang('create_user_password_confirm_label','password_confirm');?><br/>
            <?php echo form_input($password_confirm);?>
          </div>
        </div>
      </div>
     
      <div class="row">
 
       
        <div class="col-md-10" style="padding-top: 40px; text-align: right;">
          <div class="form-group">
       
             <input type="submit" value="<?php echo lang('edit_user_submit_btn'); ?>" name="submit" class="btn btn-primary">
          </div>
        </div>
   </div>
     

<?php echo form_close();?>

</div></div></div>