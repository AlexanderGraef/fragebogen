<!--Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('joiningrequests')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('joiningrequests')?></h6> -->
     <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseMyTable" aria-expanded="true" aria-controls="collapseMyTable">
          <i class="fas fa-fw fa-list"></i>
          <span><?= $this->lang->line('joiningrequests')?>  </span>
        </a>
  </div>
  <div class="card-body show" id="collapseMyTable">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th><?= $this->lang->line('member_name')?></th>
            <th><?= $this->lang->line('email')?></th>
            <th><?= $this->lang->line('address')?></th>
            <th><?= $this->lang->line('requested_on')?></th>
            <th><?= $this->lang->line('status')?></th>
            <th class="text-right"><?= $this->lang->line('actions')?></th>
          </tr>
        </thead>
        <tbody>
           <?php

		       $i = 1;
        		foreach($requests as $c) : ?>
            <?php 
            //debug_array(array("sql"=>$c->requested_on,"php"=>date("Y-m-d h:i:sa")));
            if($c->status == 0){ ?>
        			<tr class="roww">
                <td><?php echo $c->first_name.' '.$c->last_name; ?></a></td>
                <td><?php echo $c->email; ?></a></td>
                <td><?php echo $c->adr_house_no.','.$c->adr_street_no.','.$c->adr_city.','.$c->adr_zip_code; ?></a></td>
                <td><?php echo get_time_ago($c->created_on); ?></a></td>
        				<td class="table-warning"><?php echo "Pending" ?></a></td>
                   <td class="text-right">
                       <form action="requests/update_admin_practice" method="post" accept-charset="utf-8" style="float:right; margin-left: 5px">
                         <input type="hidden" name="request_id" value="<?php echo $c->id; ?>">
                         <input type="hidden" name="action" value="1"> 
                         <input type="submit" name="submit" value="Approve" class="btn btn-success btn-sm">                   
                        </form> 
                       <form action="requests/update_admin_practice" method="post" accept-charset="utf-8" style="margin-left: 5px">
                         <input type="hidden" name="request_id" value="<?php echo $c->id; ?>">
                         <input type="hidden" name="action" value="2"> 
                         <input type="submit" name="submit" value="Reject" class="btn btn-danger btn-sm">                   
                        </form>

                    </td>              
      			</tr>
      			<?php
      			$i++;
          }
      		endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- Practice Accounts companies list -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('company_list')?></h6> -->
     <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePracticingTable" aria-expanded="true" aria-controls="collapsePracticingTable">
          <i class="fas fa-fw fa-list"></i>
          <span><?= $this->lang->line('approved_requests')?>  </span>
        </a>
  </div>
  <div class="card-body collapse" id="collapsePracticingTable">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
         <thead>
          <tr>
            <th><?= $this->lang->line('member_name')?></th>
            <th><?= $this->lang->line('email')?></th>
            <th><?= $this->lang->line('address')?></th>
            <th><?= $this->lang->line('requested_on')?></th>
            <th><?= $this->lang->line('status')?></th>
          </tr>
        </thead>
        <tbody>
           <?php

           $i = 1;
            foreach($requests as $c) : ?>
            <?php if($c->status == 1){ ?>
              <tr class="roww">
                <td><?php echo $c->first_name.' '.$c->last_name ?></a></td>
                <td><?php echo $c->email ?></a></td>
                <td><?php echo $c->adr_house_no.','.$c->adr_street_no.','.$c->adr_city.','.$c->adr_zip_code; ?></a></td>
                <td><?php echo get_time_ago($c->created_on) ?></a></td>
                <td class="table-success"><?php echo "Approved" ?></a></td>
                      
            </tr>
            <?php
            $i++;
          }
          endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!--End Practice Accounts companies list -->

<!-- Other companies list -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('company_list')?></h6> -->
     <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOtherTable" aria-expanded="true" aria-controls="collapseOtherTable">
          <i class="fas fa-fw fa-list"></i>
          <span><?= $this->lang->line('rejected_requests')?>  </span>
        </a>
  </div>
  <div class="card-body collapse" id="collapseOtherTable">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
     <thead>
          <tr>
            <th><?= $this->lang->line('member_name')?></th>
            <th><?= $this->lang->line('email')?></th>
            <th><?= $this->lang->line('address')?></th>
            <th><?= $this->lang->line('requested_on')?></th>
            <th><?= $this->lang->line('status')?></th>
          </tr>
        </thead>
        <tbody>
           <?php

           $i = 1;
            foreach($requests as $c) : ?>
            <?php if($c->status == 2){ ?>
              <tr class="roww">
                <td><?php echo $c->first_name.' '.$c->last_name ?></a></td>
                <td><?php echo $c->email ?></a></td>
                <td><?php echo $c->adr_house_no.','.$c->adr_street_no.','.$c->adr_city.','.$c->adr_zip_code; ?></a></td>
                <td><?php echo get_time_ago($c->created_on) ?></a></td>
                <td class="table-danger"><?php echo "Rejected" ?></a></td>
                      
            </tr>
            <?php
            $i++;
          }
          endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!--End Other companies list -->

</div>
<!-- /.container-fluid