<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('manage_question') ?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?= $this->lang->line('manage_question') ?></h6>
  </div>
  <div class="card-body">
     <?php 
        $user = $this->ion_auth->user()->row();
        $admin = $user->id;
        $this->db->select('*')
                ->from("practicing_topics as pt")
                ->join("questions as q","q.topicId = pt.topic_id")
                ->where("pt.practiceaccount_id",$admin)
                ->where("q.id",$question['id']);
                $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            redirect(base_url()."admin/questionnare/topic/".$query->row()->topic_id);
        }
        ?>

  <?php echo validation_errors('<div class="error">'); ?>
  <?php echo form_open(); ?>
<div class="row mt-5">
    <div class="col-sm-5">
        <div class="form-group">
            <?php
            echo form_label($this->lang->line('question_name'), "questionName");
            echo form_input("questionName", $question['question'], array("class" => "form-control"));
            ?>
        </div>
    </div>
    <div class="col-sm-5">
            <div class="form-group">
                <?php
                echo form_label($this->lang->line('less_than_20_notice'), "employeesNotice");
                echo form_input("employeesNotice", $question['employeesNotice'], array("class" => "form-control"));
                ?>
            </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group">
                <?php
                $options = array(
                    "Yes" => "Yes",
                    "No" => "No"
                );
                echo form_label($this->lang->line('show_notice_on'), "showNotice");
                echo form_dropdown("showNotice", $options, $question['showNotice'], array("class" => "form-control"));
                ?>
            </div>
        </div>
    <?php
    $checkedYes = TRUE;
    $checkedText = FALSE;
    if(isset($question['type'])) {
        if ($question['type'] == 2) {
            $checkedYes = TRUE;
        } elseif ($question['type'] == 1) {
            $checkedText = TRUE;
        }
    }?>
    <div class="col-sm-5">
        <div class="form-group">
            <div class="form-check form-check-inline">
                <?php echo form_radio('type', 2, $checkedYes, array('class' => 'form-check-input', 'id' => 'inlineRadio1')); ?>
                <label class="form-check-label" for="inlineRadio1"><?= $this->lang->line('yes_no_question')?></label>
            </div>
            <div class="form-check form-check-inline">
                <?php echo form_radio('type', 1, $checkedText, array('class' => 'form-check-input', 'id' => 'inlineRadio2')); ?>
                <label class="form-check-label" for="inlineRadio2"><?= $this->lang->line('text_question')?></label>
            </div>
        </div>
    
    </div>

    <div class="col-sm-5">
        <div class="form-group">
            <?php
            $options = array();
            foreach($topics as $t) {
                $options[$t->id] = $t->topicName;
            }
            echo form_label($this->lang->line('choose_topic'), "topic");
            echo form_dropdown("topicId", $options, $question['topicId'], array("id" => "chooseTopic", "class" => "form-control", "onchange" => "chooseSubquestion()"));
            ?>
        </div>
    </div>
  
    <div id="subquestions" class="col-sm-5">
            
        </div>
        <div id="parentOption" class="col-sm-5">
            
        </div>
</div>

<div class="col-sm-5">
    <div class="form-group">
        <?php
        echo form_submit("submit", $this->lang->line('submit'), array("class" => "btn btn-success"));
        ?>
    </div>
</div>
</form>

  </div>
</div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript">
    $(document).ready(function() {
        var parentOption = "<?php echo $question['parentOption'] ?>";
        chooseSubquestion()
        chooseParentOption(parentOption)
    })

    function chooseSubquestion() {
        var optionVal = $('#chooseTopic option:selected').val();
        var parentId = <?php echo $question['parentId'] ?>;
        console.log(optionVal)
        $.ajax({
            url:"<?php echo base_url('admin/questionnare/questionsAjax')?>/"+ optionVal,
            type:'post',
            success:function(response){
                var obj = $.parseJSON(response);
                
                $("#subquestions").html('<div class="form-group"><label>Choose parent question</label><select class="form-control" name="parentId" onchange="chooseParentOption()" id="subquestion"></select></div>');
                if(parentId == 0) {
                    $("#subquestion").append('<option value="0" selected="selected">No parent question</option>')
                }
                console.log(obj)
                $.each(obj, function (index, object) {
                    if(object['type'] == 2) {
                        if(object['id'] == parentId) {
                            $("#subquestion").append('<option selected="selected" value="'+ object['id'] +'">'+ object['question'] +'</option>')
                        } else {
                            $("#subquestion").append('<option value="'+ object['id'] +'">'+ object['question'] +'</option>')
                        }
                        
                    }
                })
                
            }
        })
    
}

function chooseParentOption(parentOption = '') {
    
    $("#parentOption").html('<div class="form-group"><label>Choose parent option</label><select class="form-control" name="parentOption" id="parentO"></select></div>');
    if(parentOption != '') {
        var selectedYes = "";
        var selectedNo = "";
        if(parentOption == "Yes") {
            selectedYes = 'selected="selected"';
        } else {
            selectedNo = 'selected="selected"';
        }
        $("#parentO").append('<option value="Yes" '+ selectedYes +'>Yes</option>')
        $("#parentO").append('<option value="No" '+ selectedNo +'>No</option>')
            
    } else {
        $("#parentO").append('<option value="" selected="selected"></option>')
        $("#parentO").append('<option value="Yes">Yes</option>')
        $("#parentO").append('<option value="No">No</option>')
    }
    
    
    
    var optionVal = $('#subquestion option:selected').val();
    if(optional == 0) {
        $("#parentOption").html()
    }

}

</script>