<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $this->lang->line('aws_configuration')?></h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-body">
  <?php echo validation_errors('<div class="error">'); ?>
  <?php echo form_open(); ?>
	<div class="row mt-5">
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('aws_config_file'), "awsConfig");
				echo form_textarea("awsConfig", $text, array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php
				echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
			?>
	  	</div>
    </div>
	</div>
</form>

  </div>
</div>

</div>
<!-- /.container-fluid -->