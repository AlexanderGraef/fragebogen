<?php

class Dashboard extends Admin_Controller {
    function __construct() {
        parent::__construct();

        $group = 'admin';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('admin');
        }
    }

 public function index() {    
       $data['counts'] = array("companies"=> count($this->AdminModel->companies()), "companies"=> count($this->AdminModel->companies()), 'topics' => count($this->QuestionnareModel->getTopics()), "users"=>count($this->ion_auth->users(null,TRUE)));
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "dashboard";
        $this->load->view($this->_container, $data);
    }
}