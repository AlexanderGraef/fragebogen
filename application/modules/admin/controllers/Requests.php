<?php

class Requests extends Admin_Controller {
    function __construct() {
		parent::__construct();
		$group = 'admin';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be a admin to view the Company page.');
            redirect('admin');
        }
    }

    public function index() {    
        $this->session->set_userdata('referred_from', current_url());
        $data = array(
            'requests' => $this->AdminModel->getrequests()
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "requests/list";
        $this->load->view($this->_container, $data);
    }

  
    function update_admin_practice()
	{
		$data['request_id'] = $this->input->post('request_id');
		$data['action'] = $this->input->post('action');
		$data['user_id'] = $this->input->post('user_id');
		$this->AdminModel->update_admin_practice($data);
		redirect(base_url()."admin/requests", 'refresh');
	}
  
   
}