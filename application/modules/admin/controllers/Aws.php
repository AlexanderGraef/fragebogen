<?php

class Aws extends Admin_Controller {
    function __construct() {
		parent::__construct();
		
        $this->referred_from = $this->session->userdata('referred_from');
    }

    public function index() {    
        $file = '/root/.s3cfg';
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('awsConfig', 'Aws Config', 'required');
        $data['text'] = file_get_contents($file);
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "aws/edit";
        if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			file_put_contents($file, $this->input->post('awsConfig'));
			redirect('admin/aws/', 'refresh');
		}
    }
    
}