<?php

class Bg extends Admin_Controller {
    function __construct() {
		parent::__construct();
		
        $this->referred_from = $this->session->userdata('referred_from');
    }

    public function index() {    
        $this->session->set_userdata('referred_from', current_url());
		$data = array(
			'bgs' => $this->AdminModel->getBgs()
		);
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "bg/list";
        $this->load->view($this->_container, $data);
    }

    public function add() {
        $this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('bg', 'Name der BG', 'required');
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "bg/add";
        
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->AdminModel->addBg();
			$this->session->set_flashdata('msg', 'BG wurde hinzugef�gt.');
			if($this->input->post('submit')) {
				redirect('admin/bg/add', 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }

    function edit($id) {
        $this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('bg', 'BG', 'required');
    
		$data = array(
			'bg' => $this->AdminModel->getBg($id)
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "bg/edit";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->AdminModel->editBg($id);
			$this->session->set_flashdata('msg', 'BG Edited');
			if($this->input->post('submit')) {
				redirect('admin/bg/edit/'.$id, 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }

    function delete($id) {
		$this->db->update('numberofbg',array("is_deleted"=>1),array("id"=>$id));

	}
}