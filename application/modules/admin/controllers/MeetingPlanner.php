<?php

class MeetingPlanner extends Admin_Controller {
    function __construct() {
        parent::__construct();
		$this->referred_from = $this->session->userdata('referred_from');
		$group = 'admin';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('admin');
        }
    }

    public function index() {
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "planner/month";
        $this->load->view($this->_container, $data);
    }
}