<?php

class Questionnare extends Admin_Controller {
    function __construct() {
        parent::__construct();
		$this->referred_from = $this->session->userdata('referred_from');
		$group = 'admin';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('admin');
        }
    }

    public function index() {
        $this->session->set_userdata('referred_from', current_url());
		$data = array(
			'topics' => $this->AdminModel->getTopics(),
			'admin_topics' => $this->AdminModel->getadminTopics()
		); 
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "questionnare/topics";
        $this->load->view($this->_container, $data);
    }
  //    function manage_topics() {
  //       $this->session->set_userdata('referred_from', current_url());
		// $data = array(
		// 	'practices' => $this->AdminModel->admin_practices(),
		// 	'questions' => $this->AdminModel->questions()
		// ); 
		// if(($this->input->post("topics") || $this->input->post("questions")) && $this->input->post("practices"))
		// {
		// 	$para['practices'] = $this->input->post("practices");
		// 	$para['questions'] = $this->input->post("questions");
		// 	$response = $this->AdminModel->manage_topics($para);
		// }

  //       $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "questionnare/manage_topics";
  //       $this->load->view($this->_container, $data);
  //   }
    function manage_topics($id=null) {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('questionName', 'Question Name', 'required');
		$data = array(
			'topics' => $this->AdminModel->getTopics(),
			'question' => $this->AdminModel->getQuestion($id)
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "questionnare/manage_topics";
        
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$total_practices = $this->AdminModel->manage_topics();

			$this->session->set_flashdata('msg', 'This question added in '.$total_practices.' practice accounts.');
			if($this->input->post('submit')) {
				redirect('admin/questionnare/manage_topics/'.$id, 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }
    function topic($topicId) {
		$this->session->set_userdata('referred_from', current_url());
		$data = array(
			'result' => $this->AdminModel->questions($topicId),
			'topics' => $this->AdminModel->getadminTopics(),
			'topic' => $this->AdminModel->topic($topicId)
		);
		$data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "questionnare/questions";
        $this->load->view($this->_container, $data);
    }

    function addTopic() {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('topicName', 'Topic Name', 'required');
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "questionnare/add_topic";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->AdminModel->addTopic();
			$this->session->set_flashdata('msg', 'Thema wurde erstellt.');
			if($this->input->post('submit')) {
				redirect('admin/questionnare/topic/add', 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
			
		}
    }
    
    function editTopic($id) {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('topicName', 'Topic Name', 'required');
		$data = array(
			'topic' => $this->AdminModel->topic($id)
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "questionnare/edit_topic";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->AdminModel->editTopic($id);
			$this->session->set_flashdata('msg', 'Thema wurde aktualisiert');
			if($this->input->post('submit')) {
				redirect('admin/topic/edit/'.$id);
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }
    
    function deleteTopic($id) {
		$this->AdminModel->deleteTopic($id);
		$this->session->set_flashdata('msg', 'Thema wurde gel�scht.');
		redirect('admin/questionnare');
    }
    
    function deleteQuestion($id) {
		$this->AdminModel->deleteQuestion($id);
		$this->session->set_flashdata('msg', 'Frage wurde gel�scht.');
		redirect('admin/topics');
    }
    
    function addQuestion() {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('questionName', 'Question Name', 'required');
		$data = array(
			'topics' => $this->AdminModel->getTopics(),
			'bgs' => $this->AdminModel->getBgs()
		);
        $users = $this->AdminModel->getUsers();
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "questionnare/add_question";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
            $allQuestionsSort = $this->AdminModel->getQuestionsSortIndexByTopic($this->input->post('topicId'));
            $sortI = array();
            foreach($allQuestionsSort as $sort => $val) {
                $sortI[] = (int) $val['sortIndex'];
            }
            $sortIndex = 1; 
            if(count($allQuestionsSort) > 0) {
                $sortIndex = max($sortI) + 1;
            }
            
            
			$this->AdminModel->addQuestion($sortIndex);
			$id = $this->db->insert_id();
			$questionId = $this->db->insert_id();
			if($this->input->post('parentId') == 0) {
				foreach($users as $user) {
					$this->AdminModel->addNotif($user->id, $questionId);
				}
			}
			$this->session->set_flashdata('msg', 'Frage wurde erstellt.');
			if($this->input->post('submit')) {
				redirect('admin/questionnare/questions/add', 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }
    
    function editQuestion($id) {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('questionName', 'Question Name', 'required');
		$data = array(
			'topics' => $this->AdminModel->getTopics(),
			'question' => $this->AdminModel->getQuestion($id)
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "questionnare/edit_question";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->AdminModel->editQuestion($id);
			$this->session->set_flashdata('msg', 'Frage wurde aktualisiert.');
			if($this->input->post('submit')) {
				redirect('admin/questionnare/questions/edit/'.$id, 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }
    
    function questionsAjax($id) {
		$data = $this->AdminModel->getQuestionsByTopic($id);
		echo json_encode($data);
    }

    function subquestionAjax($id, $qOption) {
		$data['q'] = $this->QuestionnareModel->getQuestionByParentId($id, $qOption);
		$this->load->view('questionnare/newQuestionRow', $data);
		//echo json_encode($data);
	}

    
    function updateQuestionsOrder() {
		$position = $_POST['position'];

		$i=1;
		foreach($position as $k=>$v){
			$this->AdminModel->updateQuestionsOrder($i, $v);
			$i++;
		}
    }
    
    function moveToTopic() {
		$topicId = $this->input->post('topicId');
		$questionId = $this->input->post('questionId');
		$this->AdminModel->changeQuestionTopic($topicId, $questionId);
    }
    
    function sendNewQuestionsEmail() {
		$this->load->library('email');
		$notifs = $this->AdminModel->getNotifs();
		$users = array();
		foreach($notifs as $not) {
			if (!in_array($not->userId, $users)) {
				$questions = $this->AdminModel->getNewQuestionsByUserId($not->userId);
				
				$config=array(
					'charset'=>'utf-8',
					'wordwrap'=> TRUE,
					'mailtype' => 'html'
					);
					
				$this->email->initialize($config);
				if(count((array)$questions) > 0) {
					$this->email->from($this->config->item('email_from'), $this->config->item('name_from'));
					$this->email->to($not->email);
					$this->email->subject($this->lang->line('new_questions_email_subject'));
					$data['questions'] = $questions;
					/* $msg = "Hier sind die noch nicht beantworteten Fragen:<br><br>";
					foreach ($questions as $question) {
						$msg .= "<br>";
						$msg .= $question->question;
					} */
					$msg = $this->load->view('emails/newQuestions', $data, TRUE);
					$this->email->message($msg);
	
					$this->email->send();
				}

				array_push($users, $not->userId);
			}

		}
		$this->session->set_flashdata('msg', 'Email wurde verschickt.');
		redirect('admin/questionnare', 'refresh');
	}
}