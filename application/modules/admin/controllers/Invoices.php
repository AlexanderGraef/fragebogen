<?php
 use InvoiceNinja\Config as NinjaConfig;
   		 use InvoiceNinja\Models\Invoice;
   		  use InvoiceNinja\Models\Client;
   		

class Invoices extends Admin_Controller {
    function __construct() {
		parent::__construct();
		$group = 'admin';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('admin');
        }
        $token = $this->db->get_where('users_tokens',array('user_id'=>$this->session->userdata('user_id')))->row();
      if(isset($token->token) && isset($token->token_url))
      {
        $this->config->set_item('invoice_ninja_token', $token->token);
        $this->config->set_item('invoice_ninja_url', $token->token_url);
          NinjaConfig::setURL($this->config->item('invoice_ninja_url'));
          NinjaConfig::setToken($this->config->item('invoice_ninja_token'));  
      }
    }

    public function index() {  


        $this->session->set_userdata('referred_from', current_url());
     
		  $token = $this->db->get_where('users_tokens',array('user_id'=>$this->session->userdata('user_id')))->row();
			if(!empty($token->token) && !empty($token->token_url))
			{
		        $invoices = $this->InvoiceModel->get_invoices();
		        $data = array(
		            'unpaid_invoices' => $invoices['unpaid'],
		            'clearance_pending_invoices' => $invoices['pending'],
		            'paid_invoices' => $invoices['paid']
		        );
       		}

       // debug_array($data['unpaid_invoices']);
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "invoices/invoices";
        $this->load->view($this->_container, $data);
    } 
 
  	public function ajax_check_token_creds()
    {
      $token  = $this->input->post("token");
      $url =  $this->input->post("url");
      if(!empty($url) && !empty($token))
      {
	      try
	      {
	          NinjaConfig::setURL($url);
	          NinjaConfig::setToken($token); 
	          print_r(Client::find(0));
	      }
	      catch(Exception $ex)
	      {

	        if(!empty(json_decode($ex->getMessage())->message))
	        {

	          if(json_decode($ex->getMessage())->message  === "record does not exist")
	           {

	             $this->settoken_ajax();
	              echo 1;
	           }

	        }
	        else if (!empty(json_decode($ex->getMessage())->error))
	        {

	           if(json_decode($ex->getMessage())->error === "Record not found")
	          {

	            $this->settoken_ajax();
	            echo 1;
	          }
	          
	        }
	        else
	        {
	          return false;
	        }
      	}
      }
    }
    public function settoken_ajax()
    {
  	   if($this->input->post('token')!="")
      {
		$this->db->where('user_id', $this->session->userdata('user_id'));
			$this->db->delete('users_tokens');
		$this->db->insert("users_tokens",array("token"=>$this->input->post('token'),"token_url"=>$this->input->post("url"),"user_id"=>$this->session->userdata('user_id')));	
		$members = $this->AdminModel->admin_practices();
		foreach ($members as $member)
		{
			$bool = $this->AdminModel->client_as_member($member->user_id);
			if(!isset($bool[0]))
			{
				$this->AdminModel->turn_members_into_clients($member);
			}
		}
      }
    }
    public function set_token()
    {
    	$token = $this->db->get_where('users_tokens',array('user_id'=>$this->session->userdata('user_id')))->row();
    	$data['token'] = $token;
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "invoices/set_token";
        $this->load->view($this->_container, $data);
    }
 
    public function cost_management() {    
        //$users = $this->ion_auth->users()->result();

        
        if($this->input->post('white_collars_unit_cost')!="")
		{
			$para['global_price_whiteC'] = $this->input->post('white_collars_unit_cost');
			$para['global_price_blueC'] = $this->input->post('blue_collars_unit_cost');
			$this->AdminModel->update_global_settings($para,3);
			$this->session->set_flashdata('msg', 'Rates upadted successfully');
		}
        if($this->input->post('practice_name')!="")
		{
			$user_id = $this->input->post('practice_id');
			$para['unit_whitecollar'] = $this->input->post('unit_whitecollar');
			$para['unit_bluecollar'] = $this->input->post('unit_bluecollar');
			$this->AdminModel->update_price_list($user_id,$para);
			$this->session->set_flashdata('msg', 'Rates upadted successfully');
		}
		$data = array(
        	'practices' => $this->AdminModel->admin_practices_as_clients(),
        	'global_settings'=>$this->AdminModel->global_settings(3),
        	'price_lists' => $this->AdminModel->price_list()

        	);
	   // debug_array($data['practices']);
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "invoices/cost_management";
        $this->load->view($this->_container, $data);
    }

    public function send_schedule_invoice()
	{
        $data['response'] = $this->InvoiceModel->send_schedule_invoice();
		$data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "invoices/send_schedule_invoice";
        $this->load->view($this->_container, $data);
       // redirect('auth', 'refresh');
	}
    public function customize_invoice($member_id=null) {  

        $this->session->set_userdata('referred_from', current_url());
      $this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('companyName', 'companyName', 'required');
		if($this->input->post('clients')!="")
		{
			$dates= explode(",",$this->input->post('dates'));
			$clients = $this->input->post('clients');
			$discount = $this->input->post('discount');
			$parameters = array("dates"=>$dates,"clients"=>$clients,"discount"=>$discount);
			if($this->input->post('items')!="")
			{
				$parameters["items"] = $this->input->post('items');
				$parameters["descriptions"] = $this->input->post('descriptions');
				$parameters["unit_costs"] = $this->input->post('unit_costs');
				$parameters["quantities"] = $this->input->post('quantities');
				$parameters["taxes"] = $this->input->post('taxes');
				$parameters["amounts"] = $this->input->post('amounts');
			}
			$response = $this->InvoiceModel->draft_auto_invoices($parameters);
			if($response['response'] == FALSE)
			{
				$this->session->set_flashdata('msg',"Invoice scheduled and sent");
			}
			else
			{
				$newArray = array_keys($response['response']);
				$msg = $response['response'][$newArray[0]][0];
				$this->session->set_flashdata('msg',$msg);
			}
		}
		
       $data['practices'] = $this->AdminModel->admin_practices_as_clients();
       $data['client_id'] = $this->AdminModel->client_as_member($member_id);
       // debug_array($data['client_id']);
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "invoices/customize_invoice";
        $this->load->view($this->_container, $data);
    }
    public function generate_invoice($member_id) {    
        $this->session->set_userdata('referred_from', current_url());
        $invoice = $this->InvoiceModel->generate_invoice($member_id,array());
       // debug_array($data['unpaid_invoices']);
        $data['invoice_id'] = 1;
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "invoices/showinvoice";
        $this->load->view($this->_container, $data);
    }
    function clientbyid($id)
    {
    	$data = $this->InvoiceModel->clientbyid($id);
		echo json_encode($data);
    	
    }
   
 	function create_invoice()
 	{
 	// 	$this->load->library('form_validation');
		// $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		// $this->form_validation->set_rules('companyName', 'companyName', 'required');
		// $company = $this->AdminModel->getCompany($id);
		
		// $data = array(
		// 	"company" => $company,
		// 	"blueCollars" => $this->CompanyModel->getNumberOfBlueCollars($company['companyId']),
  //           "whiteCollars" => $this->CompanyModel->getNumberOfWhiteCollars($company['companyId']),
  //           "bgs" => $this->AdminModel->getBgs()
  //       );
 		$data['practices'] = $this->AdminModel->admin_practices();
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "Invoices/create_invoice";
		$this->load->view($this->_container, $data);
		
		
 	}
   
	function send_invoice_reminder(){
		$link=$this->input->post('link');
		$email=$this->input->post('email');
		$response = $this->AdminModel->send_invoice_reminder($email,$link);

		if($response == 1)
		{
			echo "<script>alert('Email Sent to this company safety person email address.'); window.location.assign('".base_url().'/admin/company'."');</script>";
		}
		else
		{
			echo "<script>alert('Try again after few moments.'); window.location.assign('".base_url().'/admin/company'."');</script>";
		}
	}
}