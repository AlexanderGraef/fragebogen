<?php

class Company extends Admin_Controller {
    function __construct() {
		parent::__construct();
		$group = 'admin';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('admin');
        }
    }

    public function index() {    
        $this->session->set_userdata('referred_from', current_url());
        $data = array(
            'companies' => $this->AdminModel->companies(),
            'practicingcompanies' => $this->AdminModel->practicingcompanies(),
            'othercompanies' => $this->AdminModel->othercompanies()
        );
      // debug_array($data['practicingcompanies']);
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "company/list";
        $this->load->view($this->_container, $data);
    }

    function edit($id) {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('companyName', 'companyName', 'required');
		$company = $this->AdminModel->getCompany($id);
		
		$data = array(
			"company" => $company,
			"blueCollars" => $this->CompanyModel->getNumberOfBlueCollars($company['companyId']),
            "whiteCollars" => $this->CompanyModel->getNumberOfWhiteCollars($company['companyId']),
            "bgs" => $this->AdminModel->getBgs()
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "company/edit";
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view($this->_container, $data);
		}
		else
		{
			$this->AdminModel->updateCompany($id);
			$this->session->set_flashdata('msg', 'Firmendaten wurden aktualisiert.');
			if($this->input->post('submit')) {
				redirect('admin/company/edit/'.$id, 'refresh');
			} elseif($this->input->post('saveAndBack')) {
				redirect($this->referred_from, 'refresh');
			}
		}
    }
    
    function delete($id) {
		$this->AdminModel->deleteCompany($id);
		$this->session->set_flashdata('msg', 'Firma wurde gel�scht.');
		redirect('admin/company', 'refresh');
    }
    
    function topics($companyId) {
		$this->session->set_userdata('referred_from', current_url());
		$company = $this->AdminModel->getCompany($companyId);
		$userId = $company['userId'];
	
		$practice_user = $this->AdminModel->getUserById($userId);
		if(count($practice_user)>0)
        $practiceaccount_id = $this->db->get_where('practicing_members',array("createduser_id"=>$practice_user['id']))->result();
        if(count($practiceaccount_id)>0)
        $practice_owner = $this->AdminModel->getUserById($practiceaccount_id[0]->practiceaccount_id);
       $user_id = !empty($practice_owner['id'])?$practice_owner['id']:NULL;
		$data = array(
			'topics' => $this->QuestionnareModel->getTopics($user_id),
			'notifications' => $this->QuestionnareModel->getNotifs($userId),
			'company' => $this->AdminModel->getCompanyById($companyId)
  
        );
        $data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "company/topics";
		$this->load->view($this->_container, $data);
    }
    
    function topic($topicId, $companyId) {
		$company = $this->AdminModel->getCompany($companyId);
		$userId = $company['userId'];
		$data = array(
			'questions' => $this->QuestionnareModel->getQuestions($topicId),
			'topic' => $this->AdminModel->topic($topicId),
			'company' => $this->AdminModel->getCompanyById($companyId)
		);
		$data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "company/questions";

		if (!$this->input->post())
                    {
                        $this->load->view($this->_container, $data);
                    }
                else
                    {
						foreach($this->input->post() as $i => $post) {
							if(isset($post[0]) && isset($post[0])) {
								if($post[0] != "S" && $post[0] != "") {
									$this->QuestionnareModel->setAnswers($userId, $post[0], $post[1]);
								}
							}		
							
						}
						$this->session->set_flashdata('msg', 'Frage aktualisiert.');
                        if($this->input->post('submit')) {
							redirect('admin/company/topic/'.$topicId.'/'.$companyId);
						} elseif($this->input->post('saveAndBack')) {
							redirect($this->referred_from, 'refresh');
						}
                    }

	}

	function deleteBCollar($id) {
		$this->AdminModel->deleteBCollar($id);
		$this->session->set_flashdata('msg', 'Collar has been deleted');
	}

	function deleteWCollar($id) {
		$this->AdminModel->deleteWCollar($id);
		$this->session->set_flashdata('msg', 'Collar has been deleted');
	}
	function com_verfication($id,$code){
		
		if($this->AdminModel->com_verfication($id,$code) == true)
			{
				echo "<script>alert('Company successfully verified.'); window.location.assign('".base_url()."');</script>";
			}
			else
			{
				echo "<script>alert('Link may be broken or expired verfication code is used.'); window.location.assign('".base_url()."');</script>";
			}
		
	}
	function verfication_email(){
		$id=$this->input->post('id');
		$email=$this->input->post('email');
		$response = $this->AdminModel->verfication_email($email,$id);

		if($response == 1)
		{
			echo "<script>alert('Email Sent to this company safety person email address.'); window.location.assign('".base_url().'/admin/company'."');</script>";
		}
		else
		{
			echo "<script>alert('Try again after few moments.'); window.location.assign('".base_url().'/admin/company'."');</script>";
		}
	}
}