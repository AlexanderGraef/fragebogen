<?php

class Settings extends Admin_Controller {
    function __construct() {
        parent::__construct();

        $group = 'admin';

        if (!$this->ion_auth->in_group($group))
        {
            $this->session->set_flashdata('message', 'You must be an administrator to view the users page.');
            redirect('admin');
        }
    }

 public function index() {    
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;

     
            if($this->input->post('first_name'))
            {
                $para['email'] = $user->email;
                if($para['email'] != $this->input->post('email') & $this->input->post('email') !="")
                {
                    if(empty($this->db->get_where("users",array("email"=>$this->input->post('email')))->row()))
                    {   
                        $email_hash = md5(strtotime(date('y-m-d h:i:s'))+12451);
                        $e = mail($this->input->post('email'), "Change email request", "Click on the link if you requested to change your email address <br><a href='".base_url()."auth/confirm_email?email=".$this->input->post('email')."&hash=".$email_hash."'>Confirm Email</");
                        $para['email_hash'] = $email_hash.'&'.$this->input->post('email');

                    }
                    else
                    {
                        echo "<script>alert('Email already in use'); window.location.assign('".base_url()."')</script>";
                    }
                }
                $user = $this->ion_auth->user()->row();
                $user_id = $user->id;
                $para['first_name'] = $this->input->post('first_name');
                $para['last_name'] = $this->input->post('last_name');
                $para['phone'] = $this->input->post('phone');
                $para['password'] = $this->input->post('password');
                $para['adr_house_no'] = $this->input->post('house_no');
                $para['adr_street_no'] = $this->input->post('street_no');
                $para['adr_city'] = $this->input->post('city');
                $para['adr_zip_code'] = $this->input->post('zip_code');


                if($this->ion_auth->update_profile($user_id, $para))
                {

                     $this->data['user'] =(Object) $this->AdminModel->getUserById($userId);
          $this->data['first_name'] = [
                'name' => 'first_name',
                'id' => 'first_name',
                'type' => 'text',
                'class'=>'form-control',
                'value' => $this->data['user']->first_name,
            ];
            $this->data['last_name'] = [
                'name' => 'last_name',
                'id' => 'last_name',
                'type' => 'text',
                'class'=>'form-control',
                'value' => $this->data['user']->last_name,
            ];
            
            
            $this->data['email'] = [
                'name' => 'email',
                'id' => 'email',
                'class'=>'form-control',
                'value' => $this->data['user']->email,
            ];
            $this->data['phone'] = [
                'name' => 'phone',
                'id' => 'phone',
                'type' => 'text',
                'class'=>'form-control',
                'value' =>$this->data['user']->phone,
            ];
            $this->data['house_no'] = [
                'name' => 'house_no',
                'id' => 'house_no',
                'type' => 'text',
                'class'=>'form-control',
                'value' =>$this->data['user']->adr_house_no,
            ];
            $this->data['street_no'] = [
                'name' => 'street_no',
                'id' => 'street_no',
                'type' => 'text',
                'class'=>'form-control',
                'value' =>$this->data['user']->adr_street_no,
            ];
            $this->data['city'] = [
                'name' => 'city',
                'id' => 'city',
                'type' => 'text',
                'class'=>'form-control',
                'value' =>$this->data['user']->adr_city,
            ];
            $this->data['zip_code'] = [
                'name' => 'zip_code',
                'id' => 'zip_code',
                'type' => 'text',
                'class'=>'form-control',
                'value' =>$this->data['user']->adr_zip_code,
            ];
            $this->data['password'] = [
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'class'=>'form-control',
                'value' =>'',
            ];
            $this->data['password_confirm'] = [
                'name' => 'password_confirm',
                'id' => 'password_confirm',
                'type' => 'password',
                'class'=>'form-control',
                'value' => '',
            ];
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect(base_url().'admin/settings', 'refresh');
            
                }

            }
            // check to see if we are creating the user
            // redirect them back to the admin page
        $this->data['user'] =(Object) $this->AdminModel->getUserById($userId);
          $this->data['first_name'] = [
                'name' => 'first_name',
                'id' => 'first_name',
                'type' => 'text',
                'class'=>'form-control',
                'value' => $this->data['user']->first_name,
            ];
            $this->data['last_name'] = [
                'name' => 'last_name',
                'id' => 'last_name',
                'type' => 'text',
                'class'=>'form-control',
                'value' => $this->data['user']->last_name,
            ];
            $this->data['email'] = [
                'name' => 'email',
                'id' => 'email',
                'class'=>'form-control',
                'value' => $this->data['user']->email,
            ];
            $this->data['phone'] = [
                'name' => 'phone',
                'id' => 'phone',
                'type' => 'text',
                'class'=>'form-control',
                'value' =>$this->data['user']->phone,
            ];
             $this->data['house_no'] = [
                'name' => 'house_no',
                'id' => 'house_no',
                'type' => 'text',
                'class'=>'form-control',
                'value' =>$this->data['user']->adr_house_no,
            ];
            $this->data['street_no'] = [
                'name' => 'street_no',
                'id' => 'street_no',
                'type' => 'text',
                'class'=>'form-control',
                'value' =>$this->data['user']->adr_street_no,
            ];
            $this->data['city'] = [
                'name' => 'city',
                'id' => 'city',
                'type' => 'text',
                'class'=>'form-control',
                'value' =>$this->data['user']->adr_city,
            ];
            $this->data['zip_code'] = [
                'name' => 'zip_code',
                'id' => 'zip_code',
                'type' => 'text',
                'class'=>'form-control',
                'value' =>$this->data['user']->adr_zip_code,
            ];
            $this->data['password'] = [
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'class'=>'form-control',
                'value' =>'',
            ];
            $this->data['password_confirm'] = [
                'name' => 'password_confirm',
                'id' => 'password_confirm',
                'type' => 'password',
                'class'=>'form-control',
                'value' => '',
            ];
        $this->data['page'] = $this->config->item('ci_my_admin_template_dir_admin') . "settings/info";
        $this->load->view($this->_container, $this->data);
    }
 
}