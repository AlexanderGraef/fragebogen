
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Register</title>
  <!-- Custom fonts for this template-->
  <link href="<?= base_url(); ?>assets/admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  <!-- Custom styles for this template-->
  <link href="<?= base_url(); ?>assets/admin/css/sb-admin-2.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">
<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?php echo lang('create_user_heading');?></h1>
              </div>
              <div id="infoMessage"><?php echo $message;?></div>
                <?php 
                $attributes = array('class' => 'user');
                echo form_open("auth/register", $attributes);?>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <?php echo form_input($first_name);?>
                  </div>
                  <div class="col-sm-6">
                      <?php echo form_input($last_name);?>
                    </div>
                </div>
                 <div class="form-group row">
                 	<div class="col-sm-12">
                 		
	                  <select name="groups[]" onchange="active_address(this.value)" required class="form-control">
	                    <option selected disabled> 
	                      Register As
	                    </option>
	                    <option value="3">
	                      Practice
	                    </option>
	                    <option value="2">
	                        Member
	                    </option>
	                  </select>
              		</div>
                </div>
                <?php if($identity_column!=='email') {
                  echo '<p>';
                  echo lang('create_user_identity_label', 'identity');
                  echo '<br />';
                  echo form_error('identity');
                  echo form_input($identity);
                  echo '</p>';
              } ?>
                <div class="form-group">
                    <?php echo form_input($email);?>
                   </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                      <?php echo form_input($password);?>
                     </div>
                  <div class="col-sm-6">
                      <?php echo form_input($password_confirm);?>
                       </div>
                </div>
                <div id="address">
                	
                </div>
               <div class="form-control-user">
                <?php echo $recaptcha ?></div>
                  <button type="submit" value="<?= lang('create_user_submit_btn')?>" class="btn btn-primary btn-user btn-block"><?= lang('create_user_submit_btn')?></button>
            </a>
              <?php echo form_close();?>
              <hr>
              <div class="text-center">
                <a class="small" href="<?= base_url('auth/forgot_password')?>"><?= lang('login_forgot_password')?></a>
              </div>
              <div class="text-center">
                <a class="small" href="<?= base_url('auth/login')?>"><?= lang('create_user_already_have_account')?></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url(); ?>assets/admin/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url(); ?>assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url(); ?>assets/admin/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url(); ?>assets/admin/js/sb-admin-2.min.js"></script>

</body>

<script type="text/javascript">
  function active_address(selected)
  {
    document.getElementById("address").innerHTML = "";
    if(selected == 3)
    {
      document.getElementById("address").innerHTML = 
      				'<h4>Address</h4><div class="form-group row">'+
                  		'<div class="col-sm-6 col-md-3 mb-3 mb-sm-0">'+
                     	 	'<input type="text" placeholder="House No" name="house_no" class="form-control form-control-user">'+
                     '</div>'+
                  		'<div class="col-sm-6 col-md-4 ">'+
                      		'<input type="text" placeholder="Street No" name="street_no" class="form-control form-control-user">'+
                       '</div>'+
                       	'<div class="col-sm-6 col-md-5 ">'+
                      		'<input type="text" placeholder="City" autocomplete="off" name="city" class="form-control form-control-user">'+
                       	'</div>'+
                	'</div>'+
                	'<div class="form-group row">'+
                  		'<div class="col-sm-6 col-md-3 mb-3 mb-sm-0">'+
                     	 	'<input type="text" placeholder="zip code" name="zip_code" class="form-control form-control-user">'+
                     	'</div>'+
                  		'<div class="col-sm-6 col-md-9 ">'+
                      		'<input type="text" placeholder="Phone No" name="phone_no" class="form-control form-control-user">'+
                       	'</div>'+
                	'</div>';
    }
  }
</script>
</html>