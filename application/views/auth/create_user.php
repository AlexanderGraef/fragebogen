<div class="container-fluid">
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><h1><?php echo lang('create_user_heading');?></h1></h6>
  </div>
  <div class="card-body">

<?php 

?>
<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/create_user");?>
<div class="row">
    <div class="col-md-5">
          <div class="form-group">

                <?php echo lang('create_user_fname_label', 'first_name');?> <br />
                <?php echo form_input($first_name);?>
                
          </div>
    </div>
    <div class="col-md-5">
          <div class="form-group">

                <?php echo lang('create_user_lname_label', 'last_name');?> <br />
                <?php echo form_input($last_name);?>
                
          </div>
    </div>
</div>
  <div class="row">
    
  <div class="col-md-5">
          <div class="form-group">
            <?php echo lang('create_user_company_label', 'company');?> <br />
            <?php echo form_input($company);?>
                
          </div>
    </div>
    <div class="col-md-5">
        <div class="form-group">
          <?php echo lang('create_user_phone_label', 'phone');?> <br />
          <?php echo form_input($phone);?>
        </div>
      </div>
</div>

      <div class="row">
        <div class="col-md-5">
          <div class="form-group">
            <?php echo lang('create_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <?php echo lang('create_user_password_confirm_label','password_confirm');?><br/>
            <?php echo form_input($password_confirm);?>
          </div>
        </div>
      </div>
      <div class="row">
         <?php
      if($identity_column!=='email') {
         
          ?>

    <div class="col-md-5">
          <div class="form-group">
<?php 
        
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
?> 
               
          </div>
    </div>
  
        <?php
      }
      ?>
      <div class="col-md-5">
        <div class="row">
          <div class="col-md-12">
             <div class="form-group">
              <?php echo lang('create_user_email_label', 'email');?> <br />
             <?php echo form_input($email);?>
           </div>
          </div>
         
        </div>
        <div class="row"> 
          <div class="col-md-12">
            <div class="form-group">
               <?php if ($this->ion_auth->is_admin()): ?>
              <h3><?php echo lang('edit_user_groups_heading');?></h3>
              <select  onchange="active_address(this.value)" name="groups[]" required class="form-control">
                <option selected disabled></option>
                <option value="3">
                    Practice
                </option>
                <option value="2">
                    Member
                </option>
              </select>
                <?php endif ?>
                <?php if ($this->ion_auth->is_practice()): ?>
                    <h3><?php echo lang('edit_user_groups_heading');?></h3>
                    <select name="groups[]" required class="form-control">
                      <option value="2">
                          Member
                      </option>
                    </select>
                <?php endif ?>
              </div>
          </div>
        </div>
      </div>
      <div id="address" class="col-md-5">
           <h4>Address</h4><div class="form-group row">
                      <div class="col-sm-6 col-md-3 mb-3 mb-sm-0">
                        <input type="text" disabled style="opacity: 0.3" class="form-control form-control-user">
                     </div>
                      <div class="col-sm-6 col-md-4 ">
                          <input type="text" disabled style="opacity: 0.3" class="form-control form-control-user">
                       </div>
                        <div class="col-sm-6 col-md-5 ">
                         <input type="text" disabled style="opacity: 0.3" class="form-control form-control-user">
                        </div>
                  </div>
                  <div class="form-group row">
                      <div class="col-sm-6 col-md-3 mb-3 mb-sm-0">
                       <input type="text" disabled style="opacity: 0.3" class="form-control form-control-user">
                      </div>
                      <div class="col-sm-6 col-md-9 ">
                         <input type="text" disabled style="opacity: 0.3" class="form-control form-control-user">
                        </div>
                  </div>
       </div>
    
        
        <div class="col-md-10" style="text-align: right;">
          <div class="form-group">
       
             <input type="submit" value="<?php echo lang('create_user_submit_btn'); ?>" name="submit" class="btn btn-primary">
          </div>
        </div>
      </div>
    
     

<?php echo form_close();?>

</div></div></div>
<script type="text/javascript">
  function active_address(selected)
  {
    document.getElementById("address").innerHTML = "";
    if(selected == 3)
    {
      document.getElementById("address").innerHTML = 
              '<h4>Address</h4><div class="form-group row">'+
                      '<div class="col-sm-6 col-md-3 mb-3 mb-sm-0">'+
                        '<input type="text" placeholder="House No" name="house_no" class="form-control form-control-user">'+
                     '</div>'+
                      '<div class="col-sm-6 col-md-4 ">'+
                          '<input type="text" placeholder="Street No" name="street_no" class="form-control form-control-user">'+
                       '</div>'+
                        '<div class="col-sm-6 col-md-5 ">'+
                          '<input type="text" placeholder="City" autocomplete="off" name="city" class="form-control form-control-user">'+
                        '</div>'+
                  '</div>'+
                  '<div class="form-group row">'+
                      '<div class="col-sm-6 col-md-3 mb-3 mb-sm-0">'+
                        '<input type="text" placeholder="zip code" name="zip_code" class="form-control form-control-user">'+
                      '</div>'+
                      '<div class="col-sm-6 col-md-9 ">'+
                          '<input type="text" placeholder="Phone No" name="phone_no" class="form-control form-control-user">'+
                        '</div>'+
                  '</div>';
    }
    else
    {
      document.getElementById("address").innerHTML = 
              '<h4>Address</h4><div class="form-group row">'+
                      '<div class="col-sm-6 col-md-3 mb-3 mb-sm-0">'+
                        '<input type="text" disabled style="opacity: 0.3" class="form-control form-control-user">'+
                     '</div>'+
                      '<div class="col-sm-6 col-md-4 ">'+
                          '<input type="text" disabled style="opacity: 0.3" class="form-control form-control-user">'+
                       '</div>'+
                        '<div class="col-sm-6 col-md-5 ">'+
                          '<input type="text" disabled style="opacity: 0.3" class="form-control form-control-user">'+
                        '</div>'+
                  '</div>'+
                  '<div class="form-group row">'+
                      '<div class="col-sm-6 col-md-3 mb-3 mb-sm-0">'+
                        '<input type="text" disabled style="opacity: 0.3" class="form-control form-control-user">'+
                      '</div>'+
                      '<div class="col-sm-6 col-md-9 ">'+
                          '<input type="text" disabled style="opacity: 0.3" class="form-control form-control-user">'+
                        '</div>'+
                  '</div>';
      
    }
  }
</script>