<?php $this->load->view('header'); ?>
<div class="row mt-5 mb-5">
	<div class="col-md-6 offset-md-9">
		<a class="btn btn-primary" href="<?php echo base_url('jobs/addOccupation');?>" role="button">Add Occupation</a>
	</div>
</div>
<table class="table">
	<thead>
	<tr>
		<th scope="col">#</th>
		<th scope="col">Occupation</th>
	</tr>
	</thead>
	<tbody>

	<?php
	$i = 1;
	foreach($occupations as $o) : ?>
		<tr>
			<th scope="row"><?php echo $i ?></th>
			<td><?php echo $o->occupationName ?></td>
			<td></td>
			<td>
				<a class="btn btn-success btn-sm" href="<?php echo base_url().'jobs/editoccupation/'.$o->id ?>">Edit</a>
				<a class="btn btn-danger btn-sm" href="<?php echo base_url().'jobs/deleteoccupation/'.$o->id ?>">Delete</a>
			</td>
		</tr>
		<?php
		$i++;
	endforeach; ?>
	</tbody>
</table>

<?php $this->load->view('footer'); ?>
