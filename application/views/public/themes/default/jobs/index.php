<?php $this->load->view('header'); ?>
<div class="row mt-5 mb-5">
	<div class="col-md-6 offset-md-9">
		<a class="btn btn-primary" href="<?php echo base_url('jobs/addJob');?>" role="button">Add Job</a>
	</div>
</div>
<table class="table">
	<thead>
	<tr>
		<th scope="col">#</th>
		<th scope="col">Job Name</th>
	</tr>
	</thead>
	<tbody>

	<?php
	$i = 1;
	foreach($jobs as $j) : ?>
		<tr>
			<th scope="row"><?php echo $i ?></th>
			<td><?php echo $j->jobName ?></td>
			<td></td>
			<td>
				<a class="btn btn-success btn-sm" href="<?php echo base_url().'admin/editjob/'.$j->id ?>">Edit</a>
				<a class="btn btn-danger btn-sm" href="<?php echo base_url().'admin/deletejob/'.$j->id ?>">Delete</a>
			</td>
		</tr>
		<?php
		$i++;
	endforeach; ?>
	</tbody>
</table>

<?php $this->load->view('footer'); ?>
