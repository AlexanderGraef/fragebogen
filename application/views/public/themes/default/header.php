<?php 
?>
<!DOCTYPE html>
<html>
<head>

	<?php header('Content-Type: text/html; charset=utf-8');?>
	<title>CRM</title>
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/main.css">
</head>
<body>
<div class="container">
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<a class="navbar-brand" href="<?php echo base_url() ?>">CRM</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item <?php if($this->uri->segment(1) == "company" || $this->uri->segment(1) == "") { echo "active"; } ?>">
					<a class="nav-link" href="<?php echo base_url() ?>"><?php echo $this->lang->line('our_company');?></a>
				</li>
				<li class="nav-item <?php if($this->uri->segment(1) == "questionnare") { echo "active"; } ?>">
					<a class="nav-link" href="<?php echo base_url("questionnare");?>"><?php echo $this->lang->line('questionnare');?></a>
				</li>
				<style type="text/css">
					.notification {
  position: relative;
  display: inline-block;
}

.notification .badge {
  position: absolute;
  top: -5px;
  right: -10px;
  padding: 5% 5%;
  border-radius: 10%;
  background: red;
  color: white;
}
				</style>
				<?php 
			 $total_influenza = isset($this->InfluenzaModel->influenza()->send_date)?$this->InfluenzaModel->influenza():array();

				?>
				<li class="nav-item <?php if($this->uri->segment(1) == "influenza") { echo "active"; } ?>">
				
					<a class="nav-link notification" href="<?php echo base_url("influenza");?>"><?php echo $this->lang->line('influenza');?><?php echo count($total_influenza) > 0?'<span class="badge">'.count($total_influenza).'</span>':''; ?>
</a>
				</li>
				<!-- <li class="nav-item">
					<a class="nav-link" href="#">Employees</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Jobs</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Ocupations</a>
				</li> -->
			</ul>
		</div>
    <div class="navbar-collapse collapse order-3 dual-collapse2">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('member/user/changeLanguage')?>/english">EN</a>
				</li>
        		<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('member/user/changeLanguage')?>/german">DE</a>
				</li>
			</ul>
		</div>
		<div class="navbar-collapse collapse order-3 dual-collapse2">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('auth/logout')?>"><?= $this->lang->line('logout')?></a>
				</li>
			</ul>
		</div>
	</nav>
	<?php if($this->session->flashdata('msg')){ ?>
		<div class="alert alert-success mt-3" role="alert">
			<?php echo $this->session->flashdata('msg'); ?>
		</div>
	<?php }
        ?>
