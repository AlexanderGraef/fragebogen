<?php $this->load->view('header'); 
if($this->uri->segment(1) == "admin") { ?>
<h3 class="mt-5 mb-5"><?php echo $company['companyName']?></h3>
<?php } ?>
<h2 class="mt-5"><?php echo $this->lang->line('choose_one_topic')?></h2>
<table class="table">
  <thead>
    <tr>
      	<th scope="col">#</th>
      	<th scope="col"><?php echo $this->lang->line('topic')?></th>
		<th scope="col"><?php echo $this->lang->line('progress')?></th>
    </tr>
  </thead>
  <tbody>

  	<?php
		$company = $this->AdminModel->getCompany($this->uri->segment(3));
		if($this->uri->segment(1) == "admin") {
			$userId = $company['userId'];
		} else {
			$userId = $this->session->userdata('id');
		}
  	$i = 1;
		foreach($topics as $topic) :
		$questionsByTopic = $this->QuestionnareModel->getQuestions($topic->id);
		$answeredN = 0;
		foreach ($questionsByTopic as $qt) {
			$answer = $this->QuestionnareModel->getAnswer($qt->id, $userId);
			if(isset($answer)) {
				 $answeredN++;
			}
		}
		$answeredQuestions = $this->QuestionnareModel->answeredQuestionsNumber($topic->id, $this->session->userdata('id'));
  	$questionsNumber = $this->QuestionnareModel->numberOfQuestions($topic->id);
  	?>
    <tr>
      	<th scope="row"><?php echo $i ?></th>
      	<td>
				<?php if($this->uri->segment(1) == "admin"): ?>
					<a href="<?php echo base_url().'admin/companytopic/'.$topic->id.'/'.$this->uri->segment(3) ?>"><?php echo $topic->topicName ?></a> 
				<?php else: ?>
				<a href="<?php echo base_url().'questionnare/topic/'.$topic->id; ?>"><?php echo $topic->topicName ?></a> 
	<?php endif;
			foreach ($notifications as $n) {
				if($topic->id == $n->topicId) {
					echo '<span class="badge badge-success">'.$this->lang->line("new").'</span>';
					break;
				}			
			}
			?></td>
		<td><?php echo $answeredN. ' / '. $questionsNumber ?></td>
    </tr>
    <?php
    $i++;
    endforeach; ?>
  </tbody>
</table>

<?php $this->load->view('footer'); ?>
