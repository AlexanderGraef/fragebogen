<?php $this->load->view('header');
if ($this->uri->segment(1) == "admin") { ?>
	<h3 class="mt-5 mb-5"><?php echo $company['companyName'] ?></h3>
<?php } ?>
<h2 class="mt-5"><?php echo $topic['topicName'] ?></h2>
<?php echo $topic['description']  ?>
<?php echo ("\n"); ?>
<?php echo validation_errors('<div class="error">'); ?>
<?php echo form_open(); ?>
<table class="table">
	<tbody>
		<?php
		$company = $this->AdminModel->getCompany($this->uri->segment(4));
		if ($this->uri->segment(1) == "admin") {
			$userId = $company['userId'];
		} else {
			$userId = $this->session->userdata('id');
		}

		$index = 0;
		foreach ($questions as $q) :
			if ($q->parentId == 0 || $q->parentId == NULL) :
				$answer = $this->QuestionnareModel->getAnswer($q->id, $userId);
				?>
				<tr class="question-<?php echo $q->id ?>">
					<td><?php echo $q->question ?><br/>
                    <h5><span class="text-danger showNotice">
                        <?php if($userCompany['extraQuestionPopupChecked'] == 2 && $q->showNotice != NULL && $answer['answer'] == $q->showNotice): ?>
                            <?php echo $q->employeesNotice; ?>
                        <?php endif;?>
                    </span></h5>
                    </td>
					<td>
						<?php if ($q->type == 2) :
							$checkedYes = FALSE;
							$checkedNo = FALSE;
							$checkedUnsure = FALSE;
							if (isset($answer['answer'])) {
								if ($answer['answer'] == "Yes") {
									$checkedYes = TRUE;
								} elseif ($answer['answer'] == "No") {
									$checkedNo = TRUE;
								} elseif ($answer['answer'] == "Unsure") {
									$checkedUnsure = TRUE;
								}
							} ?>
							<div class="form-check form-check-inline">
								<?php echo form_radio('answer-' . $q->id . '[0]', 'Yes', $checkedYes, array('class' => 'form-check-input', 'id' => 'inlineRadio1', 'data-id' => $q->id)); ?>
								<label class="form-check-label" for="inlineRadio1">Ja</label>
							</div>
							<div class="form-check form-check-inline">
								<?php echo form_radio('answer-' . $q->id . '[0]', 'No', $checkedNo, array('class' => 'form-check-input', 'id' => 'inlineRadio2', 'data-id' => $q->id)); ?>
								<label class="form-check-label" for="inlineRadio2">Nein</label>
							</div>
							<div class="form-check form-check-inline">
								<?php echo form_radio('answer-' . $q->id . '[0]', 'Unsure', $checkedUnsure, array('class' => 'form-check-input', 'id' => 'inlineRadio2', 'data-id' => $q->id)); ?>
								<label class="form-check-label" for="inlineRadio2">Unsicher</label>
							</div>
						<?php else :
						echo form_input('answer-' . $q->id . '[0]', $answer['answer'], array('class' => 'form-control'));
					endif; ?>
					</td>
					<input name="answer-<?php echo $q->id; ?>[1]" class="form-control" type="hidden" value="<?php echo $q->id ?>" />
				</tr>
				<?php
				$index++;
			endif;
			$answerr = "";
			if (isset($answer['answer'])) {
				$answerr = $answer['answer'];
			}
			$subquestions = $this->QuestionnareModel->getQuestionByParentId($q->id, $answerr);
            if ($subquestions) :
                foreach($subquestions as $subquestion): 
				if (isset($answer['answer'])) :
					if ($answer['answer'] == "Yes" || $answer['answer'] == "No") :
						$answer = $this->QuestionnareModel->getAnswer($subquestion->id, $userId);
						?>
						<tr data-parentId="<?php echo $q->id ?>" class="question-<?php echo $subquestion->id ?>">
							<td><?php echo $subquestion->question?></td>
							<td>
								<?php if ($subquestion->type == 2) :
									$checkedYes = FALSE;
									$checkedNo = FALSE;
									$checkedUnsure = FALSE;
									if (isset($answer['answer'])) {
										if ($answer['answer'] == "Yes") {
											$checkedYes = TRUE;
										} elseif ($answer['answer'] == "No") {
											$checkedNo = TRUE;
										} elseif ($answer['answer'] == "Unsure") {
											$checkedUnsure = TRUE;
										}
									} ?>
									<div class="form-check form-check-inline">
										<?php echo form_radio('answer-' . $subquestion->id . '[0]', 'Yes', $checkedYes, array('class' => 'form-check-input', 'id' => 'inlineRadio1', 'data-id' => $subquestion->id)); ?>
										<label class="form-check-label" for="inlineRadio1">Ja</label>
									</div>
									<div class="form-check form-check-inline">
										<?php echo form_radio('answer-' . $subquestion->id . '[0]', 'No', $checkedNo, array('class' => 'form-check-input', 'id' => 'inlineRadio2', 'data-id' => $subquestion->id)); ?>
										<label class="form-check-label" for="inlineRadio2">Nein</label>
									</div>
									<div class="form-check form-check-inline">
										<?php echo form_radio('answer-' . $subquestion->id . '[0]', 'Unsure', $checkedUnsure, array('class' => 'form-check-input', 'id' => 'inlineRadio2', 'data-id' => $subquestion->id)); ?>
										<label class="form-check-label" for="inlineRadio2">Unsicher</label>
									</div>
								<?php else :
								echo form_input('answer-' . $subquestion->id . '[0]', $answer['answer'], array('class' => 'form-control'));
							endif; ?>
							</td>
							<input name="answer-<?php echo $subquestion->id; ?>[1]" class="form-control" type="hidden" value="<?php echo $subquestion->id ?>" />
						</tr>
                    <?php endif;
            endif;
        endforeach;
		endif;
	endforeach; ?>
	</tbody>
</table>
<?php echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success")); ?>
</form>
<script>
	$(document).on('change', 'input:radio[class="form-check-input"]', function() {

		if ($(this).is(':checked')) {
			var id = $(this).data('id')
			$('tr[data-parentId="' + id + '"]').remove();
			if ($(this).val() == 'Yes' || $(this).val() == 'No') {
				$.ajax({
					url: "<?php echo base_url('questionnare/subquestionAjax') ?>/" + id + "/" + $(this).val(),
					type: 'get',
					success: function(response) {
						console.log(response)
						if (response) {
							$(".question-" + id).after(response)
						}
					}
				})
                $('.question-' + id + ' span.showNotice').html("")
                var showNotice = <?php echo $userCompany['extraQuestionPopupChecked'];?>;
                if(showNotice == 2) {
                    $.ajax({
					url: "<?php echo base_url('questionnare/questionNoticeAjax') ?>/" + id + "/" + $(this).val(),
					type: 'get',
                    dataType: "json",
					success: function(response) {
						if (response) {
                            console.log(response.employeesNotice)
                            $('.question-' + id + ' span.showNotice').html("* " + response.employeesNotice)
					    } else {
                            $('.question-' + id + ' span.showNotice').html()
                        }
                    }
				})
                }
                
			}
		}
	})
</script>
<?php $this->load->view('footer'); ?>