</div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"><?php echo $this->lang->line("ready_to_leave")?></h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body"><?php echo $this->lang->line("select_logout_to_end_session")?></div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal"><?= $this->lang->line('cancel') ?></button>
          <a class="btn btn-primary" href="<?= base_url() ?>/auth/logout"><?= $this->lang->line('logout') ?></a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script>var base_url = '<?php echo base_url() ?>';</script>
  <script src="<?= base_url() ?>assets/admin/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url() ?>assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url() ?>assets/admin/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url() ?>assets/admin/js/sb-admin-2.min.js"></script>

  <script src="<?= base_url() ?>assets/admin/js/sb-practice-2.js"></script>
  <script src="<?= base_url() ?>assets/admin/js/jquery.multi-select.js"></script>

  <!-- Page level plugins -->
  <script src="<?= base_url() ?>assets/admin/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url() ?>assets/admin/js/demo/chart-area-demo.js"></script>
  <script src="<?= base_url() ?>assets/admin/js/demo/chart-pie-demo.js"></script>
  <script src="<?= base_url() ?>assets/admin/js/listbox.js"></script>
  <script src="<?= base_url() ?>assets/admin/js/toolbar.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.js"></script>
  <script>
	$(document).on('change', 'input:radio[class="form-check-input"]', function() {

		if ($(this).is(':checked')) {
			var id = $(this).data('id')
			$('tr[data-parentId="' + id + '"]').remove();
			if ($(this).val() == 'Yes' || $(this).val() == 'No') {
				$.ajax({
					url: "<?php echo base_url('admin/questionnare/subquestionAjax') ?>/" + id + "/" + $(this).val(),
					type: 'get',
					success: function(response) {
						console.log(response)
						if (response) {
							$(".question-" + id).after(response)
						}
					}
				})
			}
		}
	})





  
  // run pre selected options
  $('#pre-selected-options').multiSelect();
  $('#pree-selected-options').multiSelect();



  var arr = [];
    $('#multiple-date-select').multiDatesPicker({onSelect:function(datetext){

        if(arr.includes(datetext)){
            var table = document.getElementById('table-data');
            var data = document.getElementById(datetext);
            data.remove(); 
            arr.splice(datetext,1)
        }else{
            arr.push(datetext)
            var table = document.getElementById('table-data');
            var row = document.createElement('tr');
            var col = document.createElement('td');
            row.setAttribute('id',datetext);
            col.innerHTML = datetext;
            row.appendChild(col);
            table.appendChild(row);         
        }   
    }});
  var arr_sec = [];
    $('#multiple-date-select-sec').multiDatesPicker({onSelect:function(datetext){

        if(arr_sec.includes(datetext)){
            var table = document.getElementById('table-data-sec');
            var data = document.getElementById(datetext);
            data.remove(); 
            arr_sec.splice(datetext,1)
        }else{
            arr_sec.push(datetext)
            var table = document.getElementById('table-data-sec');
            var row = document.createElement('tr');
            var col = document.createElement('td');
            row.setAttribute('id',datetext);
            col.innerHTML = datetext;
            row.appendChild(col);
            table.appendChild(row);         
        }   
    }});
</script>
</body>

</html>
