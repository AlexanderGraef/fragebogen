<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin Dashboard</title>
 
  <!-- Custom fonts for this template-->
  <link href="<?= base_url() ?>assets/admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url() ?>assets/admin/css/sb-admin-2.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/admin/css/multi-select.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/admin/css/bootstrap-multiselect.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/pepper-grinder/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.css">
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url() ?>admin">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"><?= $this->lang->line('admin_dashboard') ?></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url() ?>admin">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span><?= $this->lang->line('dashboard') ?></span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Nav Item - Charts -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url() ?>admin/company">
          <i class="fas fa-fw fa-building"></i>
          <span><?= $this->lang->line('company') ?></span></a>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?= base_url() ?>admin/questionnare" data-toggle="collapse" data-target="#collapseQuestionnare" aria-expanded="true" aria-controls="collapseQuestionnare">
          <i class="fas fa-fw fa-question"></i>
          <span><?= $this->lang->line('questionnare') ?></span>
        </a>
        <div id="collapseQuestionnare" class="collapse" aria-labelledby="headingQuestionnare" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"><?= $this->lang->line('questions') ?>:</h6>
            <a class="collapse-item" href="<?= base_url() ?>admin/questionnare"><?= $this->lang->line('topics') ?></a>
            <a class="collapse-item" href="<?= base_url() ?>admin/questionnare/topic/add"><?= $this->lang->line('add_topic') ?></a>
            <a class="collapse-item" href="<?= base_url() ?>admin/questionnare/questions/add"><?= $this->lang->line('add_question') ?></a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url() ?>admin/reports">
          <i class="fas fa-fw fa-table"></i>
          <span><?= $this->lang->line('reports') ?></span></a>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBgs" aria-expanded="true" aria-controls="collapseBgs">
          <i class="fas fa-fw fa-list"></i>
          <span><?= $this->lang->line('bgs') ?></span>
        </a>
        <div id="collapseBgs" class="collapse" aria-labelledby="headingBgs" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"><?= $this->lang->line('bgs') ?>:</h6>
            <a class="collapse-item" href="<?= base_url() ?>admin/bg"><?= $this->lang->line('list') ?></a>
            <a class="collapse-item" href="<?= base_url() ?>admin/bg/add"><?= $this->lang->line('add_bg') ?></a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-users"></i>
          <span><?= $this->lang->line('users') ?></span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"><?= $this->lang->line('users') ?>:</h6>
            <a class="collapse-item" href="<?= base_url() ?>admin/users"><?= $this->lang->line('users') ?></a>
            <a class="collapse-item" href="<?= base_url() ?>admin/myusers"><?= $this->lang->line('myusers') ?></a>
            <a class="collapse-item" href="<?= base_url() ?>admin/users/create"><?= $this->lang->line('add_user') ?></a>
          </div>
        </div>
      </li>
            <!-- Nav Item - Utilities Collapse Menu -->
            <?php 
          $requests = $this->AdminModel->getrequests();
          $i=0;
          foreach($requests as $c)
          if($c->status == 0)
            $i++;
            ?>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url() ?>admin/requests">
          <i class="fas fa-fw fa-user-clock"></i>
          <span><?= $this->lang->line('joiningrequests') ?><?php if($i > 0)
             echo ' <span style="color: white; border-radius: 50px;background-color:#0000ff94;"> &nbsp; '.$i.' &nbsp; </span>'; ?></span></a>
      </li>
        <!-- Nav Item - Utilities Collapse Menu -->

 <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtility" aria-expanded="true" aria-controls="collapseUtility">
          <i class="fas fa-fw fa-file-invoice-dollar"></i>
          <span><?= $this->lang->line('invoices') ?></span>
        </a>
        <div id="collapseUtility" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"><?= $this->lang->line('invoices') ?>:</h6>
            <a class="collapse-item" href="<?= base_url() ?>admin/invoices"><?= $this->lang->line('invoices') ?></a>
            <a class="collapse-item" href="<?= base_url() ?>admin/invoices/cost_management"><?= $this->lang->line('cost_management') ?></a>
           
          </div>
        </div>
      </li>
     <li class="nav-item">
       <a class="nav-link" href="<?= base_url() ?>admin/aws">
       <i class="fas fa-fw fa-user-clock"></i>
          <span><?= $this->lang->line('aws_configuration') ?></span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
            </li>
            <ul class="navbar-nav ml-auto">
				      <li class="nav-item">
					  <a class="nav-link" href="<?php echo base_url('member/user/changeLanguageAdmin')?>/english">EN</a>
				    </li>
        		<li class="nav-item">
					    <a class="nav-link" href="<?php echo base_url('member/user/changeLanguageAdmin')?>/german">DE</a>
				    </li>
			      </ul>
            <div class="topbar-divider d-none d-sm-block"></div>
            <?php
            $user = $this->ion_auth->user()->row();
            ?>
            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $user->first_name ?> <?= $user->last_name ?></span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  <?= $this->lang->line('logout') ?>
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?php echo base_url().'admin/settings' ?>">
                  <i class="fas fa fa-cog fa-fw fa-fw mr-2 text-gray-400"></i>
                  <?= $this->lang->line('settings') ?>
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->
        <div class="container-fluid">
            <?php if ($this->session->flashdata('msg')): ?>
                <div class="alert alert-success fade show" role="alert">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <?= $this->session->flashdata('msg') ?>
                </div>
            <?php endif; ?>
        </div>