<?php $this->load->view("header"); 
?>
<h2 class="mt-5"><?php echo $company['companyName']?></h2>
<?php echo validation_errors('<div class="error">'); ?>
<?php echo form_open(); ?>
	<div class="row mt-5">
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_name'), "companyName");
				echo form_input("companyName", $company['companyName'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_street'), "companyStreet");
				echo form_input("companyStreet", $company['companyStreet'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_house_number'), "companyHouseNumber");
				echo form_input("companyHouseNumber", $company['companyHouseNumber'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_zip'), "companyZip");
				echo form_input("companyZip", $company['companyZip'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_city'), "companyCity");
				echo form_input("companyCity", $company['companyCity'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_phone'), "companyPhone");
				echo form_input("companyPhone", $company['companyPhone'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('company_website'), "companyWebsite");
				echo form_input("companyWebsite", $company['companyWebsite'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php
				echo form_label($this->lang->line('latest_number_of_blue_collars'), "numberOfBlueCollar");
				echo form_input("numberOfBlueCollar", $company['name'], array("class" => "form-control", "id" => "numberOfBlueCollar"));
			?>
			<!-- <input class="blueCollar" type="hidden" name="numberOfBlueCollar" value="" /> -->
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php

				echo form_label($this->lang->line('latest_number_of_white_collars'), "numberOfWhiteCollar");
				echo form_input("numberOfWhiteCollar", $company['whiteCollarName'], array("class" => "form-control", "id" => "numberOfWhiteCollar"));
			?>
			<!-- <input class="whiteCollar" type="hidden" name="numberOfWhiteCollar" value="" /> -->
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php
            $options = array();
            foreach($bgs as $bg) {
                $options[$bg->id] = $bg->bg;
            }
				echo form_label($this->lang->line('number_of_bg'), "numberOfBg");
				echo form_dropdown("numberOfBg", $options, $company['numberOfBg'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('safety_person_name'), "safetyPersonName");
				echo form_input("safetyPersonName", $company['safetyPersonName'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('safety_person_email'), "safetyPersonEmail");
				echo form_input("safetyPersonEmail", $company['safetyPersonEmail'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('safety_person_phone'), "safetyPersonPhone");
				echo form_input("safetyPersonPhone", $company['safetyPersonPhone'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('direct_adresse_for_osha_names'), "directAdresseeForOshaNames");
				echo form_input("directAdresseeForOshaNames", $company['directAdresseeForOshaNames'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('direct_adresse_for_osha_email'), "directAdresseeForOshaEmail");
				echo form_input("directAdresseeForOshaEmail", $company['directAdresseeForOshaEmail'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php 
				echo form_label($this->lang->line('direct_adresse_for_osha_number'), "directAdresseeForOshaNumber");
				echo form_input("directAdresseeForOshaNumber", $company['directAdresseeForOshaNumber'], array("class" => "form-control"));
			?>
	  	</div>
    </div>
    <div class="col-sm-5">
		<div class="form-group">
			<?php
				echo form_submit("submit", $this->lang->line('save'), array("class" => "btn btn-success"));
			?>
	  	</div>
    </div>
	</div>
</form>
<h4 class="mt-5"><?php echo $this->lang->line('number_of_blue_collars_in_past_time')?></h4>
<table class="table">
	<thead>
	<tr>
		<th scope="col"><?php echo $this->lang->line('number_of_blue_collar')?></th>
		<th scope="col"><?php echo $this->lang->line('date_added')?></th>
	</tr>
	</thead>
	<tbody>
<?php
foreach ($blueCollars as $bCollar) : ?>
	<tr class="roww">
		<td><?php echo $bCollar->name ?></td>
		<td><?php echo $bCollar->year ?></td>
		<?php if($this->uri->segment(1) == 'admin'): ?>
		<td>
				<a class="btn btn-danger btn-sm" id="deleteBCollar" data-id="<?php echo $bCollar->id ?>" href="#">Delete</a>
		</td>
			<?php endif; ?>
	</tr>
<?php endforeach; ?>
	</tbody>
</table>
<h4 class="mt-5"><?php echo $this->lang->line('number_of_white_collars_in_past_time')?></h4>
<table class="table">
	<thead>
	<tr>
		<th scope="col"><?php echo $this->lang->line('number_of_white_collar')?></th>
		<th scope="col"><?php echo $this->lang->line('date_added')?></th>
	</tr>
	</thead>
	<tbody>
	<?php
	foreach ($whiteCollars as $wCollar) : ?>
		<tr class="roww">
			<td><?php echo $wCollar->whiteCollarName ?></td>
			<td><?php echo $wCollar->whiteCollarYear ?></td>
			<?php if($this->uri->segment(1) == 'admin'): ?>
		<td>
				<a class="btn btn-danger btn-sm" id="deleteWCollar" data-id="<?php echo $wCollar->id ?>" href="#">Delete</a>
		</td>
			<?php endif; ?>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<!-- Modal -->
<div class="modal fade" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="noticeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="noticeModalLabel">Notice</h5>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Do you have more than 20 whole time employees?
      </div>
      <div class="modal-footer">
        <a href="<?php echo base_url('company/noticeChecked/'.$company["companyId"]).'/2'?>" class="btn btn-secondary">No</a>
        <a href="<?php echo base_url('company/noticeChecked/'.$company["companyId"]).'/1'?>" class="btn btn-primary checkNotice">Yes</a>
      </div>
    </div>
  </div>
</div>
<?php if($showModal):?>
    <script> 
        $('#noticeModal').modal('show')
    </script>
    <?php $this->session->set_userdata('first_time', 'shown');?>
<?php endif;?>
<script>

		// $("#numberOfBlueCollar").keyup(function() {
		// 	var bcVal = $("#numberOfBlueCollar").val();
		// 	$(".blueCollar").val(bcVal);
		// })
		// $("#numberOfWhiteCollar").keyup(function() {
		// 	var wcVal = $("#numberOfWhiteCollar").val();
		// 	$(".whiteCollar").val(wcVal);
		// })
        

</script>
<?php $this->load->view("footer"); ?>
