<?php
		use InvoiceNinja\Config as NinjaConfig;
   		use InvoiceNinja\Models\Invoice;
   		use InvoiceNinja\Models\Client;
   		//use InvoiceNinja\Models\Taxrate;
   		

class InvoiceModel extends CI_Model {

	function __construct()
	{
		$token = array();
		if(isset($this->AdminModel->get_member_group($this->session->userdata('user_id'))[0]->group_id))
		if($this->AdminModel->get_member_group($this->session->userdata('user_id'))[0]->group_id == 4)
		{
			$practice_id = $this->AdminModel->members_parent($this->session->userdata('user_id'));
     		$practice_id = !empty($practice_id[0]->practiceaccount_id)?$practice_id[0]->practiceaccount_id:0;
			$token = $this->db->get_where('users_tokens',array('user_id'=>$practice_id))->row();
			if(isset($token->token) && isset($token->token_url))
			{
	     		$this->config->set_item('invoice_ninja_token', $token->token);
				$this->config->set_item('invoice_ninja_url', $token->token_url);
			}
		}
		else
		{
			$token = $this->db->get_where('users_tokens',array('user_id'=>$this->session->userdata('user_id')))->row();
			if(isset($token->token) && isset($token->token_url))
			{
				$this->config->set_item('invoice_ninja_token', $token->token);
				$this->config->set_item('invoice_ninja_url', $token->token_url);
			}
		}
		if(isset($token->token) && isset($token->token_url))
		{
	   		NinjaConfig::setURL($this->config->item('invoice_ninja_url'));
	     	NinjaConfig::setToken($this->config->item('invoice_ninja_token'));	
		}
	     	 // $tax = new Taxrate();
	     	 // $tax->name = "GST";
	     	 // $tax->rate = 5;
	     	 // $tax->account_key = "5vnhd1aiklpa36vgh6fbg35ntu5lhqci";
	     	 // $tax->is_inclusive = 1;
	     	 // $tax->save();
	     	 // $this->session->userdata('user_id');
	     	 // debug_array(Taxrate::all());
	
	}

  function invoices()
  {
	$invoices = Invoice::all();
	$i=0;
	foreach($invoices as $invoice)
	{ 
		if($invoice->is_deleted == 1)
		{
			unset($invoices[$i]);
		}
		$i++;
	}

	return $invoices;
  }
  function generate_invoice($invoice_data)
  {

  		//debug_array(Invoice::find(43));
  		$client = Client::find($invoice_data['client_id']);
  		// ,array("tax_name1"=>$item['tax_name1'],"tax_rate1"=>$item['tax_rate1'])
  		$invoice = $client->createInvoice();
  		$i = 0;
  		foreach ($invoice_data['items'] as $item)
  		{
	    	$invoice->addInvoiceItem($item['items'],$item['descriptions'],$item['amounts'],$item['quantities']);
	    	if(!empty($item['tax_name1']))
	    	{
		    	$invoice->invoice_items[$i]->tax_name1 = $item['tax_name1'];
		    	$invoice->invoice_items[$i]->tax_rate1 = $item['tax_rate1'];
	    	}
	    	$i++;
  		}
		$invoice->discount = $invoice_data['discount'];
		$invoice->invoice_date = date('Y-m-d');
		$invoice->due_date = date("Y-m-d",strtotime('+15 days',strtotime(date('Y-m-d'))));
	   	$invoice->email_invoice = true;
	   	$invoice->save();
	   	$a =$this->db->update('schedule_invoices',array('is_sent'=>1),array('send_date <= '=>date("Y-m-d")));
	  
	   	//debug_array($d);
	  // 	$email = $invoice->sendEmail();
	  //	$pdf = $invoice->download();
	    //debug_array($pdf);
		//return $invoice;
  }
 
  	function send_schedule_invoice()
	{
		$flag = false;
		$in = 0;
		$this->db->select('*');
		$this->db->from("schedule_invoices as si");
		$this->db->join("draft_invoices as di","di.id = si.invoice_id");
		$this->db->where(array("si.send_date <= "=>date("Y-m-d"),"si.is_sent"=>0));
		$this->db->order_by("created_on","DESC");
		$query = $this->db->get();
		$invoices = $query->result();
		foreach ($invoices as $invoice)
		{
			$flag = false;
			$data['client_id'] = $invoice->client_id;
			$data['discount'] = $invoice->invoice_discount;
			if($invoice->invoice_data == "")
			{
				$member = $this->members_as_clients($invoice->client_id);
				$member_id = isset($member[0])?$member[0]->member_id:0;
				$group = $this->AdminModel->get_member_group($member_id);
				if(empty($member_id))
				{
					$error['client_id_is_not_member'][$in] = "This Client is not registered as a member in CRM.";
					$flag = true;
				}
				else
				{
					if($group[0]->group_id == 3)
					{
						$members = $this->AdminModel->practice_users($member_id);
						$nob = 0;
						$now = 0;
						$blue = 0;
						$white = 0;
						$bluecollars = 0;
						$whitecollars = 0;
						//
						
						
						if(empty($members))
						{
							$error['practice_has_no_active_members'][$in] = "This Practice ID ".$member_id." has no active company.";
							$flag = true;
						}
						else
						{
							foreach ($members as $member)
							{
								$company = $this->AdminModel->getCompanyByUserId($member->user_id);
								if(empty($company[0]))
								{
									$error['no_company'][$in] = "No company found to this member: ".$member->user_id;
									$flag = true;
								}
								else
								{
									$company = $company[0];
									$nob = $this->AdminModel->getNumberOfBlueCollars($company->id,date("Y-m",strtotime($invoice->send_date)));
									$now = $this->AdminModel->getNumberOfWhiteCollars($company->id,date("Y-m",strtotime($invoice->send_date)));
									$blue += $nob['name'];
									$white += $now['whiteCollarName'];
									$price_list_g = array();
									$price_list = $this->db->get_where("price_list",array('user_id' => $member_id))->result();
									$price_list = isset($price_list[0])?$price_list[0]:array();
									if(empty($price_list))
									{
										$price_list_g = $this->db->get_where("global_settings",array("rates_for"=>$group[0]->group_id))->result();
										$price_list_g = isset($price_list_g[0])?$price_list_g[0]:array();
										if(empty($price_list_g))
										{
											$error['no_price_list'][$in] = "Company ID ".$company->id." has no blue collars and white collars set prices.";
											$flag = true;
										}

									}
									if($this->db->affected_rows()>0)
									{
										if(count($price_list)>0)
										{
											$bluecollars += $blue * $price_list->unit_bluecollar;					
											$whitecollars += $white * $price_list->unit_whitecollar;
											$description[0] = "Total number of Blue Collars in your all companies are ".$blue.". Unit cost of Blue Collar is $".$price_list->unit_bluecollar." .";
										$description[1] = "Total number of White Collars in your all companies are ".$white.". Unit cost of White Collar is $".$price_list->unit_whitecollar.".";
										$data['items'][0] = array("items"=>"Total Number of Blue Collars","descriptions"=>$description[0],"amounts"=>$bluecollars,"quantities"=>$blue);
										$data['items'][1] = array("items"=>"Total Number of White Collars","descriptions"=>$description[1],"amounts"=>$whitecollars,"quantities"=>$white);
										}
										else if(count($price_list_g)>0)
										{
										$bluecollars += $blue * $price_list_g->global_price_blueC;
											$whitecollars += $white * $price_list_g->global_price_whiteC;
											$description[0] = "Total number of Blue Collars in your all companies are ".$blue.". Unit cost of Blue Collar is $".$price_list_g->global_price_blueC." .";
										$description[1] = "Total number of White Collars in your all companies are ".$white.". Unit cost of White Collar is $".$price_list_g->global_price_whiteC.".";
										$data['items'][0] = array("items"=>"Total Number of Blue Collars","descriptions"=>$description[0],"amounts"=>$bluecollars,"quantities"=>$blue);
										$data['items'][1] = array("items"=>"Total Number of White Collars","descriptions"=>$description[1],"amounts"=>$whitecollars,"quantities"=>$white);
										}
										
										
									}
								}
							}
						}
					}
					else if($group[0]->group_id == 2)
					{
						$nob = 0;
						$now = 0;
						$blue = 0;
						$white = 0;
						$bluecollars = 0;
						$whitecollars = 0;
					
								$company = $this->AdminModel->getCompanyByUserId($member_id);
								if(empty($company[0]))
								{
									$error['no_company'][$in] = "No company found to this member: ".$member_id;
									$flag = true;
								}
								else
								{
									$company = $company[0];
									$nob = $this->AdminModel->getNumberOfBlueCollars($company->id,date("Y-m",strtotime($invoice->send_date)));
									$now = $this->AdminModel->getNumberOfWhiteCollars($company->id,date("Y-m",strtotime($invoice->send_date)));
									$blue += $nob['name'];
									$white += $now['whiteCollarName'];
									$price_list_g = array();
									$price_list = $this->db->get_where("price_list",array('user_id' => $member_id))->result();
									$price_list = isset($price_list[0])?$price_list[0]:array();
									if(empty($price_list))
									{
										$price_list_g = $this->db->get_where("global_settings",array("rates_for"=>$group[0]->group_id))->result();
										$price_list_g = isset($price_list_g[0])?$price_list_g[0]:array();
										if(empty($price_list_g))
										{
											$error['no_price_list'][$in] = "Company ID ".$company->id." has no blue collars and white collars set prices.";
											$flag = true;
										}

									}
									if($this->db->affected_rows()>0)
									{
										if(count($price_list)>0)
										{
											$bluecollars += $blue * $price_list->unit_bluecollar;					
											$whitecollars += $white * $price_list->unit_whitecollar;
											$description[0] = "Total number of Blue Collars in your company is: ".$blue.". Unit cost of Blue Collar is $".$price_list->unit_bluecollar." .";
										$description[1] = "Total number of White Collars in your company: ".$white.". Unit cost of White Collar is $".$price_list->unit_whitecollar.".";
										$data['items'][0] = array("items"=>"Total Number of Blue Collars","descriptions"=>$description[0],"amounts"=>$bluecollars,"quantities"=>$blue);
										$data['items'][1] = array("items"=>"Total Number of White Collars","descriptions"=>$description[1],"amounts"=>$whitecollars,"quantities"=>$white);
										}
										else if(count($price_list_g)>0)
										{
										$bluecollars += $blue * $price_list_g->global_price_blueC;
											$whitecollars += $white * $price_list_g->global_price_whiteC;
											$description[0] = "Total number of Blue Collars in your all companies are ".$blue.". Unit cost of Blue Collar is $".$price_list_g->global_price_blueC." .";
										$description[1] = "Total number of White Collars in your all companies are ".$white.". Unit cost of White Collar is $".$price_list_g->global_price_whiteC.".";
										$data['items'][0] = array("items"=>"Total Number of Blue Collars","descriptions"=>$description[0],"amounts"=>$bluecollars,"quantities"=>$blue);
										$data['items'][1] = array("items"=>"Total Number of White Collars","descriptions"=>$description[1],"amounts"=>$whitecollars,"quantities"=>$white);
										}
										
										
									}
								}
							
						
					}

				}
			}
			else
			{
				$data['items'] = array();
				$items = json_decode($invoice->invoice_data);
				$i=0;
				foreach ($items as $item)
				{
			  		$b = $item;
			  		$tax_arr = "";
					$tax_perc = "";
					if(!empty($b->taxes))
					{	
						$tax_arr = explode(":",$b->taxes);
						$tax_perc = $tax_arr[1];
						$tax_arr = $tax_arr[0];
					}
					
					$data['items'][$i] = array("items"=>$item->items,"descriptions"=>$item->descriptions,"amounts"=>$item->amounts,"quantities"=>$item->quantities,"tax_name1"=>$tax_arr,"tax_rate1"=>$tax_perc);
					$i++;
				}
			}
			if($flag==false)
			{
				$this->generate_invoice($data);	
				return true;		
			}
			
			$in++;
		}
		if($flag)
		{
			$error['extra'][$in] = "Some invoice failed";
			return $error;
		}
		else
		{
			return "No pending Invoice for today or earlier.";
		}
	}
	function confirm_client_member($client_id,$is_practice=null)
	{
		$in=0;
		$flag = FALSE;
		$error = array();
		$member = $this->members_as_clients($client_id);
		$member_id = isset($member[0])?$member[0]->member_id:0;
		if($is_practice)
		{
			if($member_id == 0)
			{
				$error['client_id_is_not_member'][$in] = "This Client is not registered as a practice in CRM.";
				$flag = true;
				return $error;
			}
			else
			{
				return FALSE;
			}
			
		}
		$members = $this->AdminModel->practice_users($member_id);
		if(empty($members))
		{
			$error['practice_has_no_active_members'][$in] = "This Practice ID ".$member_id." has no active member.";
			$flag = true;
		}
		else
		{
			foreach ($members as $member)
			{
				$company = $this->AdminModel->getCompanyByUserId($member->user_id);
				if(empty($company[0]))
				{
					$error['no_company'][$in] = "No company found to this member: ".$member->user_id;
					$flag = true;
				}
				else
				{
					if($this->db->affected_rows()<=0)
					{
						$error['extra'][$in] = "Some invoice failed";
						$flag = true;
					}
				}
				$in++;
			}
		}
		if($flag)
		{
			return $error;
		}
		else
		{
			return FALSE;
		}
	}
  function draft_auto_invoices($data)
  {
  	$confirmation = array();
  	if(isset($data['items']))
	{
	  	foreach ($data['clients'] as $client_id)
	  	{
		  	$confirmation['response'] = $this->confirm_client_member($client_id,1);
		  	if($confirmation['response'] === FALSE)
		  	{
			  	$draft_invoice = $this->draft_invoice($data);  		
			  	break;
		  	}
		  	else
		  	{
		  		return $confirmation;
		  	}
	  	}
	}
	else
	{
		foreach ($data['clients'] as $client_id)
	  	{
	  		$member_id = $this->AdminModel->members_as_clients($client_id);
			$group_id = $this->get_member_group($member_id[0]->member_id);
			if($group_id[0]->group_id == 3)
			{
	  			$confirmation['response'] = $this->confirm_client_member($client_id);
			}
			else if($group_id[0]->group_id == 2)
			{
	  			$confirmation['response'] = $this->confirm_client_member($client_id,1);
			}
		  	if($confirmation['response'] === FALSE)
		  	{
			  	$draft_invoice = $this->draft_invoice($data);  		
			  	break;
		  	}
		  	else
		  	{
		  		return $confirmation;
		  	}
	  	}
	}

	foreach ($data['dates'] as $date)
	{
		foreach ($data['clients'] as $client_id)
		{		
			$flag = TRUE;
			$member_id = $this->AdminModel->members_as_clients($client_id);
			if(isset($member_id[0]))
			$mem = $this->db->get_where("invoice_preferences",array("member_id"=>$member_id[0]->member_id))->row();
			if(!empty($mem->member_id))
			{
				if($mem->receive_invoices != 1)
					$flag = FALSE;
			}
				if($flag == TRUE)
				{
					if(isset($data['items']))
					{		
						$confirmation['response'] = $this->confirm_client_member($client_id,1);
					  	if($confirmation['response'] === FALSE)
					  	{
							$para = array(
							'send_date' => date("Y-m-d",strtotime($date)),
							'client_id' => $client_id,
							'invoice_id' => $draft_invoice[0]->id
								);
							$this->db->insert('schedule_invoices', $para);
						}
						else
						{
				  			return $confirmation;
						}
					}
					else
					{
						$member_id = $this->AdminModel->members_as_clients($client_id);
						$group_id = $this->get_member_group($member_id[0]->member_id);
						if($group_id[0]->group_id == 3)
						{
				  			$confirmation['response'] = $this->confirm_client_member($client_id);
						}
						else if($group_id[0]->group_id == 2)
						{
				  			$confirmation['response'] = $this->confirm_client_member($client_id,1);
						}
					  	if($confirmation['response'] === FALSE)
					  	{
							$para = array(
							'send_date' => date("Y-m-d",strtotime($date)),
							'client_id' => $client_id,
							'invoice_id' => $draft_invoice[0]->id
								);
							$this->db->insert('schedule_invoices', $para);
						}
						else
						{
				  			return $confirmation;
						}
					}
				}
			
		}
	  }
		$confirmation['schedule_run'] = $this->send_schedule_invoice();
		return FALSE;
	}
  function draft_invoice($data)
  {

  	if(!empty($data['items']))
  	{
		$items = array_filter($data['items']);
		$parameters = array();
		for ($i=0; $i <count($items); $i++)
		{ 
			$parameters[$i] = array("items"=>$data['items'][$i],"descriptions"=>$data['descriptions'][$i],"unit_costs"=>$data['unit_costs'][$i],"quantities"=>$data['quantities'][$i],"amounts"=>$data['amounts'][$i],"taxes"=>$data['taxes'][$i]);
		}
  		$para['invoice_data']=json_encode($parameters);
  		
  	}
		$para['invoice_discount']=$data['discount'];
		$para['drafted_on']=strtotime(date("Y-m-d G:i:s"));
		$this->db->insert('draft_invoices', $para);

		$this->db->select('*');
		$this->db->from("draft_invoices");
		$this->db->order_by("drafted_on","DESC");
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}
  function auto_cron_invoices()
  {

  			// $client = Client::find($client_id);
	  		// $member = $this->members_as_clients($client_id)[0]->member_id;
	  		// $invoice = $client->createInvoice();
		   //  $invoice->addInvoiceItem("Item","description",10);
		   //  $invoice->email_invoice = true;
		   // 	$invoice->save();
		   // 	$result += $invoice;
		  	//$pdf = $invoice->download();
  }
  function practices_members_as_clients() {

		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;	
		$this->db->select("u.first_name,u.last_name,u.id as user_id,mac.client_id as id")
				->from("practicing_members as pm")
				->join("users as u","u.id=pm.createduser_id")
				->join("members_as_clients as mac","pm.createduser_id = mac.member_id")
				->where("pm.practiceaccount_id",$user_id)
				->order_by("u.created","DESC");
		$query = $this->db->get();
		//debug_array($this->db->queries);


		return $query->result();
	}
  function client_as_member($user_id)
  {
  		$this->db->select('mac.client_id');
		$this->db->from("members_as_clients as mac");
		$this->db->where('mac.member_id',$user_id);

		$query = $this->db->get();
		
		return $query->result();
  }
  function get_schedule_invoices()
  {
  		$this->db->select('*');
		$this->db->from("schedule_invoices as si");
		$this->db->where('si.send_date',date("Y-m-d"));

		$query = $this->db->get();
		//debug_array($query->result());
		return $query->result();
  }
  function members_as_clients($user_id)
  {
  		$this->db->select('mac.member_id');
		$this->db->from("members_as_clients as mac");
		$this->db->where('mac.client_id',$user_id);

		$query = $this->db->get();
		
		return $query->result();
  }
  function get_member_group($user_id)
  {
  		$this->db->select('ug.group_id');
		$this->db->from("users_groups as ug");
		$this->db->where('ug.user_id',$user_id);

		$query = $this->db->get();
		
		return $query->result();
  }
     function validate_token_url($url,$token)
    {
      if(!empty($url) && !empty($token))
      {
	      try
	      {
	          NinjaConfig::setURL($url);
	          NinjaConfig::setToken($token); 
	          print_r(Client::find(0));
	      }
	      catch(Exception $ex)
	      {

	        if(!empty(json_decode($ex->getMessage())->message))
	        {

	          if(json_decode($ex->getMessage())->message  === "record does not exist")
	           {
	              return true;
	           }

	        }
	        else if (!empty(json_decode($ex->getMessage())->error))
	        {

	           if(json_decode($ex->getMessage())->error === "Record not found")
	          {
	             return true;
	          }
	          
	        }
	        else
	        {
	          return false;
	        }
      	}
      }
      else
      {
      	return false;
      }
    }
	function get_invoices() 
	{ 

	   
		$unpaid=array();
		$pending=array();
		$paid=array();
		$invoices = $this->invoices();

		foreach($invoices as $invoice)
		{	
			if($invoice->invoice_status_id == 2)
			{
				$member_id = $this->members_as_clients($invoice->client_id);
				if(isset($member_id[0]->member_id))
				{
					$unpaid = array_merge($unpaid,array($invoice));
					// $group_id = $this->get_member_group($member_id[0]->member_id);
					// if($group_id[0]->group_id == 3)
					// {
					// 	$unpaid = array_merge($unpaid,array($invoice));
					// }
				}
			}
			
			if($invoice->invoice_status_id < 5 & $invoice->invoice_status_id > 2)
			{
				$member_id = $this->members_as_clients($invoice->client_id);
				if(isset($member_id[0]->member_id))
				{
						$pending = array_merge($pending,array($invoice));
					// $group_id = $this->get_member_group($member_id[0]->member_id);
					
					// if($group_id[0]->group_id == 3)
					// {
					// 	$pending = array_merge($pending,array($invoice));
					// }
				}
			}
			if($invoice->invoice_status_id == 5)
			{
				$member_id = $this->members_as_clients($invoice->client_id);
				if(isset($member_id[0]->member_id))
				{
						$paid = array_merge($paid,array($invoice));
					// $group_id = $this->get_member_group($member_id[0]->member_id);
					
					// if($group_id[0]->group_id == 3)
					// {
					// 	$paid = array_merge($paid,array($invoice));
					// }
				}
			}
		}
		$result['unpaid'] = $unpaid;
		$result['pending'] = $pending;
		$result['paid'] = $paid;
 		return $result;
	}

	function clearance_pending_invoices() {
	
		$result=array();
		$invoices = $this->invoices();

		
	}
	function paid_invoices() {
		
	}
	function clientbyid($id)
	{
		$client = Client::find($id);
		return $client;
	}
	
}
