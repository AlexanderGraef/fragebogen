<?php

class QuestionnareModel extends CI_Model {

	public function getTopics($user_id = NULL) {

		if($user_id == NULL)
		{
			$user = $this->ion_auth->user()->row();
			$user_id = $user->id;
		}
		$admin = $this->db->get_where("users_groups",array("group_id"=>1))->row();
		$this->db->select('t.*,pt.topic_id,pt.created_on');
		$this->db->from("practicing_topics as pt");
		$this->db->join("topics as t","pt.topic_id = t.id");
		$this->db->where('(pt.practiceaccount_id = '.$user_id.' or pt.practiceaccount_id ='.$admin->user_id.') and t.is_deleted = ', 0);
		$this->db->order_by('t.id', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	public function getQuestions($topicId) {


		$this->db->select('*');
		$this->db->from('questions');
		$this->db->where('topicId', $topicId);
		$this->db->where('is_deleted', 0);
		$this->db->order_by('questions.sortIndex', 'ASC');
		$query = $this->db->get();
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->select('q.*');
		$this->db->from("questions as q");
		$this->db->where("FIND_IN_SET(".$user_id.",q.is_viewed_by) and q.topicId = ".$topicId);
		$q = $this->db->get();
		if($q->num_rows()<=0)
		{
			$res = $this->db->get_where("questions",array("topicId"=>$topicId))->result();
			if(!empty($res))
			{
				foreach ($res as $r1) 
				{	
					$this->db->set("is_viewed_by","CONCAT(is_viewed_by,',',".$user_id.")",FALSE);
					$this->db->where(array("id"=>$r1->id));
					$this->db->update("questions");
				}
			}
		}
		return $query->result();
	}

	public function getAnswer($questionId, $userId) {
		$this->db->where('questionId', $questionId);
		$this->db->where('userId', $userId);
		$query = $this->db->get('answers');
		return $query->row_array();
	}

	function getAllAnswers() {
		$query = $this->db->get('answers');
		return $query->result();
	}

	public function setAnswers($userId, $answer, $questionId) {
		$this->db->where('userId', $userId);
		$this->db->where('questionId', $questionId);
    	$query = $this->db->get("answers");

		$data = array(
			'answer' => $answer,
			'userId' => $userId,
			'questionId' => $questionId
		);

		if($query->num_rows() == 1) {
			$this->db->where('userId', $userId);
			$this->db->where('questionId', $questionId);
			if($answer == "") {
				$this->db->delete('answers');
			} else {
				$this->db->update('answers', $data);
			}
            
		} else {
			$this->db->insert('answers', $data);
		}
	}

	public function answeredQuestionsNumber($topicId, $userId) {
		$this->db->select('questions.id, questions.question, questions.topicId, questions.type, answers.answer, answers.questionId, answers.userId');
		$this->db->from('questions');
		$this->db->join('answers', 'answers.questionId = questions.id', 'right');
		$this->db->where('answers.userId', $userId);
		$this->db->where('questions.topicId', $topicId);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function numberOfQuestions($topicId) {
		$this->db->where('topicId', $topicId);
		$query = $this->db->get('questions');
		return $query->num_rows();
	}

	public function getNotifs($userId) {
		$this->db->where('userId', $userId);
		$query = $this->db->get('newquestionsnotif');
		return $query->result();
	}

	public function deleteNotif($userId, $topicId) {
		$this->db->where('userId', $userId);
		$this->db->where('topicId', $topicId);
		$this->db->delete('newquestionsnotif');

	}

	function getQuestionByParentId($id, $qOption) {
		$this->db->where('parentId', $id);
		$this->db->where('parentOption', $qOption);
		$query = $this->db->get('questions');
		return $query->result();
    }
    
    function getQuestionNotice($id, $qOption) {
		$this->db->where('id', $id);
		$this->db->where('showNotice', $qOption);
		$query = $this->db->get('questions');
		return $query->row_array();
	}

	function getTopic($id) {
		$this->db->where('id', $id);
		$this->db->where('is_deleted', 0);
		$query = $this->db->get('topics');
		return $query->row_array();
	}

	function addTopic($user_id = null) {
		$data = array(
			'topicName' => $this->input->post('topicName'),
			'description' => $this->input->post('description')
		);
		$this->db->insert('topics', $data);
		if($user_id == NULL)
		{
			$user = $this->ion_auth->user()->row();
			$user_id = $user->id;
		}
		$data = array(
			'topic_id' => $this->lasttopic()['id'],
			'practiceaccount_id' => $user_id
		);
		
		$this->db->insert('practicing_topics', $data);
	}
	function lasttopic() {
		
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get('topics');
		return $query->row_array();
	}

	function editTopic($id) {
		$data = array(
			'topicName' => $this->input->post('topicName'),
			'description' => $this->input->post('description')
		);
		$this->db->where('id', $id);
		$this->db->update('topics', $data);
	}
}
