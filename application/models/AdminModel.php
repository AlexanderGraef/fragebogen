<?php
		 use InvoiceNinja\Config as NinjaConfig;
   		 use InvoiceNinja\Models\Invoice;
   		  use InvoiceNinja\Models\Client;
   		

class AdminModel extends CI_Model {

	
	function __construct()
	{
		$token = $this->db->get_where('users_tokens',array('user_id'=>$this->session->userdata('user_id')))->row();
		if(isset($token->token) && isset($token->token_url))
		{
			$this->config->set_item('invoice_ninja_token', $token->token);
			$this->config->set_item('invoice_ninja_url', $token->token_url);
	   		NinjaConfig::setURL($this->config->item('invoice_ninja_url'));
	     	NinjaConfig::setToken($this->config->item('invoice_ninja_token'));	
		}
	
	}

	function login($username, $password)
	   {
		   $this->db->select('id, username, password, admin, confirmed');
		   $this->db->from('users');
		   $this->db->where('username', $username);
		   $this->db->where('password', sha1(salt.$password));
		   $this->db->limit(1);

		   $query = $this->db->get();

	   if($query->num_rows() == 1)
	   {
	     	return $query->result();
	   }
	   else
	   {
	     	return false;
	   }
	  }

	function practice_accounted_terminated($user_id) {
		$this->db->select("pm.practiceaccount_id,u.is_deleted");
		$this->db->from("practicing_members as pm");
		$this->db->join("users as u","u.id = pm.practiceaccount_id");
		$this->db->where("pm.createduser_id",$user_id);
		$query = $this->db->get();
		return $query->row();
  }
	function addUser($token) {
	$password = sha1(salt.$this->input->post('password'));
    $data = array(
      'username' => $this->input->post('username'),
      'confirmToken' => $token,
      'confirmed' => 0,
      'password' => $password
      );

    $this->db->insert('users', $data);
  }
  function invoices()
  {
		$invoices = Invoice::all();
		return $invoices;
  }
  function generate_invoice($user_id,$invoice_data)
  {
  		// $client_id = $this->client_as_member($user_id);
  		// $client = Client::find($client_id[0]->client_id);
  		
  		// $invoice = $client->createInvoice();
	   //  $invoice->addInvoiceItem("Item","description",10);
	   // $invoice->save();
	  	// $pdf = $invoice->download();
	    //debug_array($pdf);
		//return $invoice;
  }
  function turn_members_into_clients($user = NULL)
  {
  	if($user != NULL)
  	{
  		$client = new Client($user->email);
		$client->name = $user->first_name.' '.$user->last_name;
		$client->work_phone = $user->phone;
		$client->save();
		$this->db->insert("members_as_clients",array("member_id"=>$user->user_id,"client_id"=>$client->id));
  	}
  	else
  	{
  		$users = $this->users();
  		foreach ($users as $user)
  		{
  			$bool = $this->client_as_member($user->id);
     		if(!isset($bool[0]))
     		{
	  			$client = new Client($user->email);
	  			$client->name = $user->first_name.' '.$user->last_name;
	  			$client->work_phone = $user->phone;
	   			$client->save();
	   			$this->db->insert("members_as_clients",array("member_id"=>$user->user_id,"client_id"=>$client->id));
	   		}
  		}
	}
		return Client::all();
  }
  function set_members_under_practice($data)
  {
  		foreach ($data['members'] as $member)
  		{
  			//debug_array($data['practice_id']);
   			$this->db->insert("practicing_members",array("createduser_id"=>$member,"practiceaccount_id"=>$data['practice_id']));
  			if($this->db->affected_rows()>0)
  			{
  				return true;
  			}
  			else
  			{
  				return false;
  			}
  		}	
  }
  function client_as_member($user_id)
  {
  		$this->db->select('mac.client_id');
		$this->db->from("members_as_clients as mac");
		$this->db->where('mac.member_id',$user_id);

		$query = $this->db->get();
		
		return $query->result();
  }
  function global_settings($rates_for)
  {
		$query = $this->db->get_where("global_settings",array("rates_for"=>$rates_for));
		$query = !empty($query->result())?$query->result()[0]: new stdClass;
		return $query;
  }
  function update_global_settings($para,$rates_for)
  {
  		$exist = $this->db->get_where("global_settings",array('rates_for'=>$rates_for));
  		if($this->db->affected_rows()>0)
  		{
	  		$this->db->update('global_settings',$para,array('rates_for'=>$rates_for));
	  	}		
	  	else
	  	{
	  		$para['rates_for'] = $rates_for;
  			$q = $this->db->insert("global_settings",$para);
	  	}
		return true;
  }
  function update_price_list($user_id,$para)
  {
  		$exist = $this->db->get_where("price_list",array("user_id"=>$user_id));

  		if($this->db->affected_rows()>0)
  		{
	  		$this->db->update('price_list',$para,array('user_id'=>$user_id));		
  		}
  		else
  		{
  			$para['user_id'] = $user_id;
  			$q = $this->db->insert("price_list",$para);
  		}
		return true;
  }
  function price_list()
  {
		$query = $this->db->get("price_list");
		return $query->result();
  }
  function members_as_clients($user_id)
  {
  		$this->db->select('mac.member_id');
		$this->db->from("members_as_clients as mac");
		$this->db->where('mac.client_id',$user_id);

		$query = $this->db->get();
		
		return $query->result();
  }
  function get_member_group($user_id)
  {
  		$this->db->select('ug.group_id');
		$this->db->from("users_groups as ug");
		$this->db->where('ug.user_id',$user_id);

		$query = $this->db->get();
		
		return $query->result();
  }
	function unpaid_invoices() 
	{ 

	   
		$result=array();
		$invoices = $this->invoices();

		foreach($invoices as $invoice)
		{	
			if($invoice->invoice_status_id == 2)
			{
				$member_id = $this->members_as_clients($invoice->client_id);
			
				$group_id = $this->get_member_group($member_id[0]->member_id);
				
				if($group_id[0]->group_id == 3)
				{
					$result = array_merge($result,array($invoice));
				}

			}
		
		}

		//return $result;
	}

	function clearance_pending_invoices() {
	
	}
	function paid_invoices() {
		
	}
	function clientbyid($id)
	{
		$client = Client::find($id);
		return $client;
	}
	function getrequests() {
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->select('ap.*,u.*,ap.id as id');
		$this->db->from("admin_practices as ap");
		$this->db->join("users as u","u.id = ap.practice_id");
		$this->db->order_by('ap.created_on', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}
	function update_admin_practice($data)
	{
		if($data['request_id'] != "" && $data['action'] == 1)
		{			
			$this->db->update('admin_practices', array("status"=>1), array("id"=>$data['request_id']));
			$this->db->select("u.*,ap.id as apid,ap.practice_id");
			$this->db->from("admin_practices as ap");
			$this->db->join("users as u","u.id = ap.practice_id");
			$this->db->where("ap.id",$data['request_id']);
			$user = $this->db->get()->row();
			$client = new Client($user->email);
	    		$client->name = $user->first_name.' '.$user->last_name;
	    		$client->first_name = $user->first_name;
	    		$client->last_name = $user->last_name;
	    		$client->save();
	    		mail($user->email, $this->lang->line('admin_approval'), "You request to become a practice account under admin assistance has been accepted. Now you can login to your practice account using your registration credentials. <a target='_blank' href='".base_url()."'></a> Thank you.");
	    		$this->db->insert("members_as_clients", array("client_id"=>$client->id,"member_id"=>$user->id));
			return TRUE;
		}
		else if($data['request_id'] != "" && $data['action'] == 2)
		{			
			$this->db->update('admin_practices', array("status"=>2), array("id"=>$data['request_id']));
			$this->db->select("u.*,ap.id as apid,ap.practice_id");
			$this->db->from("admin_practices as ap");
			$this->db->join("users as u","u.id = ap.practice_id");
			$this->db->where("ap.id",$data['request_id']);
			$user = $this->db->get()->row();
			mail($user->email, $this->lang->line('admin_approval'), "You request to become a practice account under admin assistance has been <b>rejected</b>.You can register for new a practice account using new credentials. <a target='_blank' href='".base_url()."/auth/register'></a> Thank you.");
			
			return TRUE;
		}
		else
		{
			$this->session->set_flashdata('message', 'You can not accept requests this time.');
			return FALSE;
		}
	}
  function addPasswordResetToken($token) {
  		$data = array(
      'resetPasswordToken' => $token
      );

  	$this->db->where('username', $this->input->post('username'));
  	$this->db->update('users', $data);
  }

  function checkToken($token) {
		  $this->db->select('id, username, password', 'resetPasswordToken');
		  $this->db->from('users');
		  $this->db->where('resetPasswordToken', $token);
		  $this->db->limit(1);

		  $query = $this->db->get();

		  if($query->num_rows() == 1)
		  {
			  return $query->result();
		  }
		  else
		  {
			  return false;
		  }
  }

	function checkConfirmed($username) {
		$this->db->where('username', $username);
		$this->db->where('confirmed', 1);
		$this->db->limit(1);

		$query = $this->db->get('users');

		if($query->num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function addConfirmationToken($token) {
		$data = array(
			'confirmToken' => $token
		);

		$this->db->where('username', $this->input->post('username'));
		$this->db->update('users', $data);
	}

	function checkConfirmToken($token) {
		$this->db->select('id, username, password', 'confirmToken');
		$this->db->from('users');
		$this->db->where('confirmToken', $token);
		$this->db->limit(1);

		$query = $this->db->get();

		if($query->num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function updateUserStatus($token) {
		$data = array(
			'confirmed' => 1
		);
		$this->db->where('confirmToken', $token);
		$this->db->update('users', $data);
	}

	function addTokenToUser($token, $email) {
		$data = array(
			'confirmToken' => $token
		);

		$this->db->where('username', $email);
		$this->db->update('users', $data);
	}

	function updatePassword() {
		$data = array(
			'password' => sha1(salt.$this->input->post('password'))
		);
		$this->db->where('resetPasswordToken', $this->input->post('token'));
		$this->db->update('users', $data);
	}

	function getTopics() {
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->select("t.*,u.first_name,u.last_name")
		->from('topics as t')
		->join("practicing_topics as pt","pt.topic_id = t.id")
		->join("users as u","u.id = pt.practiceaccount_id")
		->where("pt.practiceaccount_id!=",$user_id)
		->where("t.is_deleted",0);
		$query = $this->db->get();
		return $query->result();

	}
	function getadminTopics() {
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->select("t.*,u.first_name,u.last_name")
		->from('practicing_topics as pt')
		->join("users as u","pt.practiceaccount_id = u.id")
		->join("topics as t","t.id = pt.topic_id")
		->where("t.is_deleted",0)
		->where("pt.practiceaccount_id",$user_id);
		$query = $this->db->get();
		return $query->result();
	}

	function addTopic() {
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$data = array(
			'topicName' => $this->input->post('topicName'),
			'description' => $this->input->post('description')
		);
		$this->db->insert('topics', $data);
		$data=array(
			'practiceaccount_id' => $user_id,
			'topic_id'=> $this->last_topic()['id']
			);
		$this->db->insert('practicing_topics', $data);
	}
	function manage_topics() {
	$data = array(
			'question' => $this->input->post('questionName'),
			'topicId' => $this->input->post('topicId'),
			'type' => $this->input->post('type'),
			'parentId' => $this->input->post('parentId'),
			'parentOption' => $this->input->post('parentOption'),
			'employeesNotice' => $this->input->post('employeesNotice'),
            'showNotice' => $this->input->post('showNotice')
		);
		if(isset($data['question']))
		{
			$i = 0 ;
			$practices = $this->admin_practices();
			foreach ($practices as $practice)
			{
				$flag = true;
				$data['topicId'] = $this->input->post('topicId');
				
				$topic = $this->db->get_where("topics",array("id"=>$data['topicId']))->row();
				$get = $this->db->get_where("practicing_topics",array("practiceaccount_id"=>$practice->user_id,"topic_id"=>$data['topicId']));
		
				if($this->db->affected_rows() <= 0)
				{
					if(isset($topic->topicName))
					{
						$topic_name = $this->db->get_where("topics",array("topicName"=>$topic->topicName));
					$topic_name = $topic_name->result();
					
					foreach ($topic_name as $one_by_one)
					{	
					$tn = $this->db->get_where("practicing_topics",array("practiceaccount_id"=>$practice->user_id,"topic_id"=>$one_by_one->id))->result();
						if($this->db->affected_rows() > 0)
						{
							$flag = false;
							
							$data['topicId'] = $one_by_one->id;
						}
					}
					}
					
				}
				
				if($this->db->affected_rows() <= 0 && $flag)
				{
					$topic->id = '';
						$this->db->insert("topics",$topic);
						$this->db->select("*");
						$this->db->from("topics");
						$this->db->limit(1);
						$this->db->order_by("id","DESC");
						$topic = $this->db->get()->row();
						$this->db->insert("practicing_topics",array("practiceaccount_id"=>$practice->user_id,"	topic_id"=>$topic->id));
						$data['topicId'] = $topic->id;
					
						$this->db->insert("questions",$data);
						$i++;
				}
				else
				{
					$question = $this->db->get_where("questions",array("question"=>$data['question'],"topicId"=>$data['topicId']))->row();
					if($this->db->affected_rows()<=0)
					{
						$i++;
						$this->db->insert("questions",$data);
					}
				}
			}
		}
		return $i;
	}

	function topic($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('topics');
		return $query->row_array();
	}
	function last_topic() {
		$this->db->select("*")
    	->from("topics")
    	->order_by("id","DESC")
    	->limit("1");
        $query = $this->db->get();

		return $query->row_array();
	}

	function editTopic($id) {
		$data = array(
			'topicName' => $this->input->post('topicName'),
			'description' => $this->input->post('description')
		);
		$this->db->where('id', $id);
		$this->db->update('topics', $data);
	}

	function questions($id) {
		$this->db->where('topicId', $id);
		$this->db->where('is_deleted',0);
		$this->db->order_by('sortIndex', 'ASC');
		$query = $this->db->get('questions');
		return $query->result_array();
	}

	function deleteTopic($id) {
		$this->db->update('topics',array("is_deleted"=>1),array("id"=>$id));
	}

	function deleteQuestion($id) {

		$this->db->update('questions',array("is_deleted"=>1),array("id"=>$id));
		$this->db->update('questions',array("is_deleted"=>1),array("parentId"=>$id));

	}

	function addQuestion($sortIndex) {
		$show_to_20less_emp = $this->input->post('show_to_20less_emp')=="on"?1:0;
		$data = array(
			'question' => $this->input->post('questionName'),
			'topicId' => $this->input->post('topicId'),
			'type' => $this->input->post('type'),
			'sortIndex' => $sortIndex,
			'parentId' => $this->input->post('parentId'),
            'parentOption' => $this->input->post('parentOption'),
            'employeesNotice' => $this->input->post('employeesNotice'),
            'showNotice' => $this->input->post('showNotice'),
            'show_to_20less_emp'=>$show_to_20less_emp

		);
		$this->db->insert('questions', $data);
	}

	function updateQuestionsOrder($i, $v) {
			$data = array(
				'sortIndex' => $i
			);
			$this->db->where('id', $v);
			$this->db->update('questions', $data);
	}
 
	function editQuestion($id) {
		$data = array(
			'question' => $this->input->post('questionName'),
			'topicId' => $this->input->post('topicId'),
			'type' => $this->input->post('type'),
			'parentId' => $this->input->post('parentId'),
			'parentOption' => $this->input->post('parentOption'),
			'employeesNotice' => $this->input->post('employeesNotice'),
            'showNotice' => $this->input->post('showNotice')
		);
		$this->db->where('id', $id);
		$this->db->update('questions', $data);
	}

	function addEmailFolder($companyId) {
		$data = array(
			'companyId' => $companyId,
			'folder' => $this->input->post('folder'),
			'email' => $this->input->post('email'),
			'password' => sha1(salt.$this->input->post('password')),
			'host' => $this->input->post('host')
		);
		$this->db->insert('emailreports', $data);
	}

	function getFolder($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('emailreports');
		return $query->row_array();
	}

	function getCompanyReports($cId) {
		$this->db->where('companyId', $cId);
		$query = $this->db->get('emailreports');
		return $query->result();
	}

	function getQuestion($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('questions');
		return $query->row_array();
	}

	function companies() {

		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->select("c.*,u.*,pm.*,c.id as companyid,u.id as user_id")
				->from("practicing_members as pm")
				->join("users as u","u.id=pm.createduser_id")
				->join("company as c","c.userId=u.id")
				->where("pm.practiceaccount_id",$user_id)
				->where("c.is_deleted",0)
				->order_by("pm.created_on","DESC");
		$query = $this->db->get();
		//debug_array($this->db->queries);
		return $query->result();
	}
	function practicingcompanies() {
	
		$this->db->select("c.*,u.*,pm.*,c.id as companyid,u.id as user_id")
			->from("admin_practices as ap")
			->join("practicing_members as pm","ap.practice_id = pm.practiceaccount_id")
			->join("users as u","u.id=pm.createduser_id")
			->join("company as c","c.userId=u.id")
			->where("c.is_deleted",0)
			->where("ap.status",1)
			->order_by("pm.created_on","DESC");
			$query = $this->db->get();
		return $query->result();
	}
	function othercompanies() {
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->select('pm.*,u.*,c.*,c.id as companyid');
		$this->db->from("company as c");
		$this->db->join("users as u","c.userId = u.id");
		$this->db->join("practicing_members as pm","c.userId = pm.createduser_id","left");
		$this->db->where("pm.createduser_id",NULL);
		$this->db->where("pm.practiceaccount_id!=",$user_id);
		$this->db->order_by('pm.created_on', 'DESC');

		$query = $this->db->get();

		return $query->result();
	}


function users() {

		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->select("*,u.id as user_id")
				->from("users as u")
				->where("u.id != ",$user_id)
				->order_by("u.created","DESC");
		$query = $this->db->get();
		//debug_array($this->db->queries);
		return $query->result();
	}
function practice_users($user_id) {


		$this->db->select("*,u.id as user_id")
				->from("users as u")
				->join("practicing_members as pm","u.id=pm.createduser_id")
				->where("pm.practiceaccount_id = ",$user_id)
				->order_by("u.created","DESC");
		$query = $this->db->get();
		//debug_array($this->db->queries);
		return $query->result();
	}

function admin_users() {

		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;	
		$this->db->select("*,u.id as user_id")
				->from("users_groups as ug")
				->join("users as u","u.id=ug.user_id")
				->join("practicing_members as pm","u.id=pm.createduser_id")
				->where("pm.practiceaccount_id = ",$user_id)
				->where("ug.group_id",2)
				->order_by("u.created","DESC");
		$query = $this->db->get();
		//debug_array($this->db->queries);
		return $query->result();
	}

function admin_practices() {

		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;	
		$this->db->select("*,u.id as user_id")
				->from("admin_practices as ap")
				->join("users as u","u.id=ap.practice_id")
				->where("ap.status",1)
				->order_by("ap.created_on","DESC");
		$query = $this->db->get();
		//debug_array($this->db->queries);
		return $query->result();
	}
function admin_practices_as_clients() {

		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;	
		$this->db->select("u.first_name,u.last_name,u.id as user_id,mac.client_id as id")
				->from("users_groups as ug")
				->join("users as u","u.id=ug.user_id")
				->join("members_as_clients as mac","u.id = mac.member_id")
				->where("ug.group_id",3)
				->order_by("u.created","DESC");
		$query = $this->db->get();
		//debug_array($this->db->queries);


		return $query->result();
	}


function practices() {

		
		$this->db->select("u.*,u.id as user_id,ug.*")
				->from("users_groups as ug")
				->join("users as u","u.id=ug.user_id")
				->where("ug.group_id",3)
				->order_by("u.created","DESC");
		$query = $this->db->get();
	//	debug_array($this->db->queries);
		return $query->result();
	}


function members() {

		
		$this->db->select("u.*,u.id as user_id,ug.*")
				->from("users_groups as ug")
				->join("users as u","u.id=ug.user_id")
				->join("practicing_members as pm","ug.user_id = pm.createduser_id","left")
				->where("ug.group_id",2)
				->where("pm.createduser_id",NULL)
				->order_by("u.created","DESC");
		$query = $this->db->get();
		//debug_array($this->db->queries);
		return $query->result();
	}


function members_parent($user_id) {


		$this->db->select("pm.practiceaccount_id,pm.createduser_id")
				->from("practicing_members as pm")
				->where("pm.createduser_id = ",$user_id);
		$query = $this->db->get();
		//debug_array($this->db->queries);
		return $query->result();
	}

	function addNotif($userId, $questionId) {
		$data = array(
			'userId' => $userId,
			'topicId' => $this->input->post('topicId'),
			'questionId' => $questionId
		);
		$this->db->insert('newquestionsnotif', $data);
	}

	function getNotifs() {
		$this->db->select();
		$this->db->from('newquestionsnotif');
		$this->db->join('users', 'users.id = newquestionsnotif.userId');
		$query = $this->db->get();
		return $query->result();
	}

	function getNewQuestionsByUserId($userId) {
		$this->db->select();
		$this->db->from('questions');
		$this->db->join('newquestionsnotif', 'newquestionsnotif.questionId = questions.id');
		$this->db->where('newquestionsnotif.userId', $userId);
		$query = $this->db->get();
		return $query->result();
	}

	function getpractices() {
		$this->db->select("u.*, u.id as user_id")
		->from("users_groups as ug")
		->join("users as u","u.id = ug.user_id")
		->where("ug.group_id",3);
		$query = $this->db->get();
		return $query->result();
	}

	function getUsers() {
		$query = $this->db->get('users');
		return $query->result();
	}

	function getHazardGroups() {
		$query = $this->db->get('hazardgroups');
		return $query->result();
	}

	function getHazards() {
		$query = $this->db->get('hazards');
		return $query->result();
	}

	function addHazard() {
		$data = array(
			'hazardName' => $this->input->post('hazardName'),
			'groupId' => $this->input->post('groupId'),
			'class' => $this->input->post('class'),
			'regularTerm' => $this->input->post('regularTerm'),
			'secondTerm' => $this->input->post('secondTerm'),
			'aplicableCode' => $this->input->post('applicableCode')
		);
		$this->db->insert('hazards', $data);
	}

	function addBg() {
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$data = array(
			'bg' => $this->input->post('bg')
		);
		$this->db->insert('numberofbg', $data);
		$data = array(
			'bg_id' => $this->getlastBg()['id'],
			'practiceaccount_id' => $user_id
			);
		$this->db->insert('practicing_bgs', $data);

	}

	function getBgs() {
		$this->db->select("nob.*, u.first_name,u.last_name,u.id as user_id")
		->from('numberofbg as nob')
		->join("practicing_bgs as pbgs","nob.id = pbgs.bg_id ","left")
		->join("users as u","pbgs.practiceaccount_id = u.id","left")
		->where("nob.is_deleted",0);
		$query = $this->db->get();
		
		return $query->result();
    }
	function getpracticeBgs($uid=null) {
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		if($uid==NULL)
		$uid=isset($this->members_parent($user_id)[0])?$this->members_parent($user_id)[0]->practiceaccount_id:'';
		if($uid !='')
		{
			$this->db->select("nob.*")
			->from('numberofbg as nob')
			->join("practicing_bgs as pbgs","nob.id = pbgs.bg_id")
			->join("users as u","u.id = pbgs.practiceaccount_id")
			->where("u.id",$uid);
			$query = $this->db->get();
			//debug_array($this->db->queries);
			return $query->result();
		}
		else
		{
			return FALSE;
		}
    }
    
    function getBg($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('numberofbg');
		return $query->row_array();
    }
 
    function getlastBg() {
    	$this->db->select("*")
    	->from("numberofbg")
    	->order_by("id","DESC")
    	->limit("1");
        $query = $this->db->get();

		return $query->row_array();
    }
    function editBg($id) {
        $data = array(
            'bg' => $this->input->post('bg')
        );
        $this->db->where('id', $id);
        $this->db->update('numberofbg', $data);
    }

	function getCompanyById($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('company');
		return $query->row_array();
	}

	function getCompanyByUserId($user_id) {
		$this->db->select("c.id, c.companyName");
		$this->db->from("company as c");
		$this->db->join("users as u","u.id = c.userId");
		$this->db->where('u.id', $user_id);
		$query = $this->db->get();
		//debug_array($user_id);
		return $query->result();
	}

	function getCompany($id) {

	
		$this->db->select('c.id as companyId, userId, companyName, companyStreet, companyHouseNumber, companyZip,
		companyCity, safetyPersonPhone, companyPhone, companyWebsite, safetyPersonEmail, numberOfBg, safetyPersonName, directAdresseeForOshaNames,
		directAdresseeForOshaEmail, directAdresseeForOshaNumber, b.name, w.whiteCollarName, extraQuestionPopupChecked,c.extra_text');
		$this->db->from('company as c');
		$this->db->join('numbersofbluecollars as b', 'c.id = b.companyId','left');
		$this->db->join('numbersofwhitecollars as w', 'c.id = w.companyId', 'left');
		$this->db->where('c.id', $id);
		//$this->db->where('numbersofwhitecollars.whiteCollarYear', date("Y"));
		//$this->db->where('numbersofbluecollars.year', date("Y"));
		$this->db->order_by('b.year', 'DESC');
		$this->db->order_by('w.whiteCollarYear', 'DESC');
		$query = $this->db->get();
	
		return $query->row_array();
	}

	function deleteCompany($id) {
		$this->db->update('company',array("is_deleted"=>1),array("id"=>$id));
	}

	function updateCompany($id) {
		$this->db->where('id', $id);
		$query = $this->db->get("company");
		$workerNumber = $this->input->post('numberOfBlueCollar')  + $this->input->post('numberOfWhiteCollar');  
    	 $extraQuestionPopupChecked = 2;  
         if($workerNumber > 20) {
         		$extraQuestionPopupChecked = 1;
         	}
		$data = array(
			"companyName" => $this->input->post('companyName'),
			"companyStreet" => $this->input->post('companyStreet'),
			"companyHouseNumber" => $this->input->post('companyHouseNumber'),
			"companyZip" => $this->input->post('companyZip'),
			"companyCity" => $this->input->post('companyCity'),
			"companyPhone" => $this->input->post('companyPhone'),
			"companyWebsite" => $this->input->post('companyWebsite'),
			"numberOfBg" => $this->input->post('numberOfBg'),
			"safetyPersonName" => $this->input->post('safetyPersonName'),
			"safetyPersonEmail" => $this->input->post('safetyPersonEmail'),
			"safetyPersonPhone" => $this->input->post('safetyPersonPhone'),
			"directAdresseeForOshaNames" => $this->input->post('directAdresseeForOshaNames'),
			"directAdresseeForOshaEmail" => $this->input->post('directAdresseeForOshaEmail'),
			"directAdresseeForOshaNumber" => $this->input->post('directAdresseeForOshaNumber'),
			"extraQuestionPopupChecked" => $extraQuestionPopupChecked,
			"extra_text" => $this->input->post('extra_text')

		);
		$companyId = 0;
		if($query->num_rows() == 1) {
			$this->db->where('id', $id);
			$this->db->update('company', $data);
			$ret = $query->row();
			$companyId = $ret->id;
		} else {
			$this->db->insert('company', $data);
			$companyId = $this->db->insert_id();
		}

		$this->db->where('companyId', $companyId);
		$query = $this->db->get("numbersofbluecollars");
		$date = new DateTime();
		$data = array(
			"companyId" => $companyId,
			"name" => $this->input->post('numberOfBlueCollar'),
			"year" => $date->format('Y-m-d H:i:s'),
		);

		if($query->num_rows() == 1) {
			$this->db->where('companyId', $companyId);
			$this->db->update('numbersofbluecollars', $data);
		} else {
			$this->db->insert('numbersofbluecollars', $data);
		}

		$this->db->where('companyId', $companyId);
		$this->db->where('whiteCollarYear', date("Y"));
		$query = $this->db->get("numbersofwhitecollars");

		$data = array(
			"companyId" => $companyId,
			"whiteCollarName" => $this->input->post('numberOfWhiteCollar'),
			"whiteCollarYear" => $date->format('Y-m-d H:i:s'),
		);

		if($query->num_rows() == 1) {
			$this->db->where('companyId', $companyId);
			$this->db->update('numbersofwhitecollars', $data);
		} else {
			$this->db->insert('numbersofwhitecollars', $data);
		}

	}

	function changeQuestionTopic($topicId, $questionId) {
		$data = array(
			'topicId' => $topicId
		);
		$this->db->where('id', $questionId);
		$this->db->update('questions', $data);
	}

	function deleteBCollar($id) {
		$this->db->where('id', $id);
		$this->db->delete('numbersofbluecollars');
	}

	function deleteWCollar($id) {
		$this->db->where('id', $id);
		$this->db->delete('numbersofwhitecollars');
	}

	function getQuestionsByTopic($id) {
		$this->db->where('topicId', $id);
		$query = $this->db->get('questions');
		return $query->result_array();
	}

	function getQuestionsSortIndexByTopic($id) {
		$this->db->select('sortIndex');
		$this->db->where('topicId', $id);
		$query = $this->db->get('questions');
		return $query->result_array();
	}

	function getNumberOfBlueCollars($companyId, $date) {
		$this->db->where('companyId', $companyId);
		$this->db->where("DATE_FORMAT(year,'%Y-%m') = '".$date."'");
		$this->db->order_by("year", "DESC");
		$query = $this->db->get('numbersofbluecollars');
		//debug_array($this->db->queries);
		return $query->row_array();
	}

	function getNumberOfWhiteCollars($companyId, $date) {
		$this->db->where('companyId', $companyId);
		$this->db->where("DATE_FORMAT(whiteCollarYear,'%Y-%m') = '".$date."'");
		$this->db->order_by("whiteCollarYear", "DESC");
		$query = $this->db->get('numbersofwhitecollars');
		return $query->row_array();
    }
    
    function getQuestionByParentId($id) {
		$this->db->where('parentId', $id);
		$query = $this->db->get('questions');
		return $query->result();
	}

	function addWarningDates() {
		$data = array(
			'dateOne' => $this->input->post('dateOne'),
			'dateTwo' => $this->input->post('dateTwo'),
			'dateThree' => $this->input->post('dateThree'),
		);
		$this->db->where('id', 0);
		$this->db->update('warningdates', $data);
	}

	function getWdates() {
		$this->db->where('id', 0);
		$query = $this->db->get("warningdates");
		return $query->row_array();
	}

	function getUserById($id) {
		$this->db->where('id', $id);
		$query = $this->db->get("users");
		return $query->row_array();
    }
    
    function seizeCompany($id, $seized) {
        $data = array(
			'seized' => $seized
		);
		$this->db->where('id', $id);
		$this->db->update('company', $data);
    }

    function verify_companyByCode($id,$code) {
		$this->db->where(array('company_id'=> $id,"verification_code"=>$code));
		$query = $this->db->get('email_verification');
		return $query->num_rows();
	}
    function com_verfication($id,$code){

	    $response = $this->verify_companyByCode($id,$code);
	    if($response > 0)
	    {
		    	$data = array(
				'code_expired' => 1
			);
			$this->db->where(array('company_id'=> $id,"verification_code" => $code));
			$this->db->update('email_verification', $data);
			$this->seizeCompany($id,0);
			return true;


	    }
	    else return false;

    }
      function send_invoice_reminder($email,$link){
			$url = 'http://localhost/invoiceninjamaster/public/view/gwdflhv1lsyd8aidm1vr18tz6dre121w?phantomjs=true&phantomjs_secret=ak-wy03k-5z02t-6n0kt-dj5ey-m9str';
			

// Real date format (xxx-xx-xx)
$toDay   = date("Y-m-d");

// we give the file a random name
$name    = "archive_".$toDay.".pdf";

// a route is created, (it must already be created in its repository(pdf)).
$rute    = 'attachments/'.$name;

// decode base64
$pdf_b64 = eval(base64_decode(file_get_contents($url)));

// you record the file in existing folder
if(file_put_contents($rute, $pdf_b64)){
    //just to force download by the browser
    header("Content-type: application/pdf");

    //print base64 decoded
    echo $pdf_b64;
}

			//header("Content-type: application/pdf");

			//$result = file_get_contents($url);
			//print_r($result);
			
			//echo base64_decode($result);

			//file_put_contents('whatever.pdf', $result);
			//file_put_contents('attachments/content.pdf', $result);



			// $options = array(
			//     'http' => array(
			//         'header'  => "Content-type: application/pdf"
			       
			//     )
			// );
			// $context  = stream_context_create($options);
			
			
			// file_put_contents('attachments/content.pdf',$result);
		exit;
    	}
     function verfication_email($email,$id){

			$data = [
				'company_id'         => $id,
				'verification_code' => substr(md5(strtotime(date('d F Y h:i:s'))),0,25)
			];
				$message = "<center><h2>Company Verfication Email</h2><br><p><a href='".base_url().'admin/company/com_verfication/'.$data['company_id']."/".$data['verification_code']."'>Click on this link</a> to verify your company. Please do not share this email to anyone.</p></center>";

				$this->email->clear();
				$this->email->from($this->config->item('admin_email', 'ion_auth'), $this->config->item('site_title', 'ion_auth'));
				$this->email->to($email);
				$this->email->subject("Verification Email");
				$this->email->message($message);
				
				if(!empty($email))
				{

				    // check if e-mail address is well-formed
				    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
						echo "<script>alert('Invalid Email address');</script>";
				    }
				    else
				    {
						if ($this->email->send() === TRUE)
						{
							$this->db->insert('email_verification', $data);
							$this->seizeCompany($id,1);
							return 1;
						}
						else
						{
							return false;
						}
					}
				}
				else
				{
					echo "<script>alert('No e-mail address found');</script>";
					return false;
				}

    	}
}
