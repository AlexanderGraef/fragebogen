<?php
	 use InvoiceNinja\Config as NinjaConfig;
   		 use InvoiceNinja\Models\Invoice;
   		  use InvoiceNinja\Models\Client;
   		
class DoctorModel extends CI_Model {
	function __construct()
	{
	
	
	}

function client_as_member($user_id)
  {
  		$this->db->select('mac.client_id');
		$this->db->from("members_as_clients as mac");
		$this->db->where('mac.member_id',$user_id);

		$query = $this->db->get();
		
		return $query->result();
  }
function companies_as_clients()
  {
	  	$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
  		$this->db->select('mac.*,u.first_name,u.last_name,u.id as user_id,mac.client_id as id');
		$this->db->from("doctors_companies as dc");
		$this->db->join("company as c","c.id = dc.company_id");
		$this->db->join("members_as_clients as mac","c.userId = mac.member_id");
		$this->db->join("users as u","u.id = c.userId");
		$this->db->where('dc.user_id',$user_id);
		$this->db->where('c.is_deleted',0);
		$this->db->where('u.active',1);

		$query = $this->db->get();
		//debug_array($query->result());
		return $query->result();
  }
	function addUser($token) {
	$password = sha1(salt.$this->input->post('password'));
    $data = array(
      'username' => $this->input->post('username'),
      'confirmToken' => $token,
      'confirmed' => 0,
      'password' => $password
      );

    $this->db->insert('users', $data);
  }
 //  function update_user()
 //  {
 //  	$data=array(
 //  		'first_name' => $this->input->post('first_name'),
 //  		'last_name' => $this->input->post('last_name'),
 //  		'phone' => $this->input->post('phone'),
 //  	);
	// $user = $this->ion_auth->user()->row();
	// $user_id = $user->id;
 //  	$this->db->update("users",$data,array("id"=>$user_id));
  	
 //  }
   function getInfluenza()
  {
  	$user = $this->ion_auth->user()->row();
	$user_id = $user->id;
  	return $this->db->get_where("scheduled_influenza",array("practiceaccount_id"=>$user_id,"is_deleted"=>0))->result();
  }
  function influenza()
  {
	  	$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->select("in.*,u.first_name,u.last_name,ir.response");
	  	$this->db->from("scheduled_influenza as in");
	  	$this->db->join("practicing_members as pm","pm.practiceaccount_id = in.practiceaccount_id");
	  	$this->db->join("users as u","u.id = pm.createduser_id");
	  	$this->db->join("influenza_response as ir","u.id = ir.user_id and ir.influenza_id = in.id","left");
	  	$this->db->where("in.practiceaccount_id",$user_id);
	  	$this->db->order_by("send_date","ASC");
	  	$this->db->limit("15");
	  	$query = $this->db->get()->result();
	  	return $query;

  }
  
  function cron_scheduled_influenza()
  { 
  		$inf = $this->db->get_where("scheduled_influenza",array("send_date"=>date("Y-m-d"),"mark_sent"=>0,"is_deleted"=>0));
  		if($this->db->affected_rows()>0)
  		{

  			$influenza = $inf->result();
  			foreach ($influenza as $one_by_one) 
  			{
  				$companies = $this->companies($one_by_one->practiceaccount_id);
  				foreach ($companies as $company)
  				{

  					mail($company->u_email, "You Practice asking for Influenza.", "Your Practice account asking for influenza amount.");
  				}
  				$this->db->update("scheduled_influenza",array("mark_sent"=>1),array("practiceaccount_id"=>$one_by_one->practiceaccount_id,"send_date"=>date("Y-m-d")));
  			}
  		}
  		return TRUE;
  }
  function schedule_influenza()
  {
  	$user = $this->ion_auth->user()->row();
	$user_id = $user->id;
  	$send_date = $this->input->post("send_date");
  	$this->db->get_where("scheduled_influenza",array("send_date"=>date("Y-m-d",strtotime($send_date)),"	practiceaccount_id"=>$user_id));
  	if($this->db->affected_rows()>0)
  	{
  		return "Already Scheduled";
  	}
  	else
  	{
  		$this->db->insert("scheduled_influenza",array("send_date"=>$send_date,"	practiceaccount_id"=>$user_id));
  		  	$this->cron_scheduled_influenza();
		return "Scheduled Successfully";
  	}
  }

	function getrequests() {
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->select('pjr.*,u.first_name,u.last_name,u.id as user_id');
		$this->db->from("practice_joining_requests as pjr");
		$this->db->join("users as u","u.id = pjr.user_id");
		$this->db->where('pjr.practiceaccount_id',$user_id);
		$this->db->order_by('pjr.requested_on', 'DESC');

		$query = $this->db->get();
		
		return $query->result();
	}
	function update_member_practice($data)
	{
		if($data['request_id'] != "" && $data['action'] == 1)
		{			
			$user = $this->ion_auth->user()->row();
			$user_id = $user->id;
			$this->db->insert('practicing_members', array("practiceaccount_id"=>$user_id,"createduser_id"=>$data['user_id']));
			$where = array(
			      'status' => 1
			      );
		  	$this->db->where('id', $data['request_id']);
		  	$this->db->update('practice_joining_requests', $where);
			return TRUE;
		}
		else if($data['request_id'] != "" && $data['action'] == 2)
		{			
				$where = array(
			      'status' => 2
			      );
		  	$this->db->where('id', $data['request_id']);
		  	$this->db->update('practice_joining_requests', $where);
		  	$this->session->set_flashdata('message', 'Request approved');
		  	return TRUE;
		}
		else
		{
			$this->session->set_flashdata('message', 'You can not accept requests this time.');
			return FALSE;
		}
	}
  function addPasswordResetToken($token) {
  		$data = array(
      'resetPasswordToken' => $token
      );

  	$this->db->where('username', $this->input->post('username'));
  	$this->db->update('users', $data);
  }

  function checkToken($token) {
		  $this->db->select('id, username, password', 'resetPasswordToken');
		  $this->db->from('users');
		  $this->db->where('resetPasswordToken', $token);
		  $this->db->limit(1);

		  $query = $this->db->get();

		  if($query->num_rows() == 1)
		  {
			  return $query->result();
		  }
		  else
		  {
			  return false;
		  }
  }

	function checkConfirmed($username) {
		$this->db->where('username', $username);
		$this->db->where('confirmed', 1);
		$this->db->limit(1);

		$query = $this->db->get('users');

		if($query->num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function addConfirmationToken($token) {
		$data = array(
			'confirmToken' => $token
		);

		$this->db->where('username', $this->input->post('username'));
		$this->db->update('users', $data);
	}

	function checkConfirmToken($token) {
		$this->db->select('id, username, password', 'confirmToken');
		$this->db->from('users');
		$this->db->where('confirmToken', $token);
		$this->db->limit(1);

		$query = $this->db->get();

		if($query->num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function updateUserStatus($token) {
		$data = array(
			'confirmed' => 1
		);
		$this->db->where('confirmToken', $token);
		$this->db->update('users', $data);
	}

	function addTokenToUser($token, $email) {
		$data = array(
			'confirmToken' => $token
		);

		$this->db->where('username', $email);
		$this->db->update('users', $data);
	}

	function updatePassword() {
		$data = array(
			'password' => sha1(salt.$this->input->post('password'))
		);
		$this->db->where('resetPasswordToken', $this->input->post('token'));
		$this->db->update('users', $data);
	}


	function add_meeting_schedule()
	{
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$topics = $this->input->post("topics");
		$ctopics="";
		foreach ($topics as $topic)
		{
			$ctopics.= $topic.',';
		}
		$para = array('company_id' => $this->input->post('company_id'),
				'start_date'=>$this->input->post('start_date'),
				'meeting_duration'=>$this->input->post('meeting_duration'),
				'notes'=>$this->input->post('notes'),
				'attendee_id'=>$user_id,
				'meeting_topics'=>$ctopics
					);
		 $this->db->insert("meeting_schedule",$para);
 	}

	function addMeetingTopic() {
	if($this->input->post('meeting_topic') != '')
		{

			$meeting_topics = $this->input->post("meeting_topic");
			foreach ($meeting_topics as $topic)
			{
				if($topic != "")
				{
					$data = array(
						'topic_name' => $topic,
					);
					$this->db->insert('meeting_topics', $data);
					$companies = $this->input->post('company_id');
					foreach ($companies as $company)
					{
						$data = array(
						"company_id"=>$company,
							'topic_id' => $this->lastmeetingtopic()['id']
						);
						$q = $this->db->get_where("company_topics",$data);
						if($q->num_rows()<=0)
						$this->db->insert('company_topics', $data);
					}
				}
			}
		}
		else if($this->input->post('meeting_topics') != "")
		{
			$topics = $this->input->post('meeting_topics');
 			$companies = $this->input->post('company_id');
 			foreach ($topics as $topic)
 			{
				foreach ($companies as $company)
				{
					$data = array(
					"company_id"=>$company,
						'topic_id' => $topic
					);
					$q = $this->db->get_where("company_topics",$data);
					if($q->num_rows()<=0)
					$this->db->insert('company_topics', $data);
				}
			}
		}
	}
	function getmeetings()
	{
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->select('ms.*,u.first_name,u.last_name,c.companyName');
		$this->db->from("meeting_schedule as ms");
		$this->db->join("company as c","c.id = ms.company_id");
		$this->db->join("users as u","u.id = c.userId");
		$this->db->where('ms.mark_as_attend', 0);
		$this->db->where('attendee_id', $user_id);
		$this->db->order_by("created_on","DESC");
		$query = $this->db->get();
		return $query->result();
	}
	function lastmeetingtopic() {
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get('meeting_topics');
		return $query->row_array();
	}
	function getmeetingtopic() {
		$query = $this->db->get('meeting_topics');
		return $query->result();
	}

	function topic($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('topics');
		return $query->row_array();
	}


	function questions($id) {
		$this->db->where('topicId', $id);
		$this->db->order_by('sortIndex', 'ASC');
		$query = $this->db->get('questions');
		return $query->result_array();
	}
function practice_users($user_id) {


		$this->db->select("*,u.id as user_id")
				->from("users as u")
				->join("practicing_members as pm","u.id=pm.createduser_id")
				->where("pm.practiceaccount_id = ",$user_id)
				->order_by("u.created","DESC");
		$query = $this->db->get();
		//debug_array($this->db->queries);
		return $query->result();
	}
	function deleteTopic($id) {
		$user = $this->ion_auth->user()->row();
      	$user_id = $user->id;
		$query = $this->db->get_where("practicing_topics",array("topic_id"=>$id,"practiceaccount_id"=>$user_id));
		if($query->num_rows() > 0)
		{
			$this->db->update('topics',array("is_deleted"=>1),array("id"=>$id));
			$this->deletepracticingTopic($id);
		}
		else
		{
			$this->db->set("is_deleted_by","CONCAT(is_deleted_by,',',".$user_id.")",FALSE);
			$this->db->where(array("id"=>$id));
			$this->db->update('topics');
		}
	}
	function deleteInfluenza($id) {
		$this->db->update('scheduled_influenza',array("is_deleted"=>1),array("id"=>$id));

	}
	function delete_company_topics($id) {
		$this->db->update('company_topics',array("is_deleted"=>1),array("topic_id"=>$id));
	}

	function deletepracticingTopic($id) {
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->where(array('topic_id'=> $id,"practiceaccount_id"=>$user_id));
		$this->db->delete('practicing_topics');

	}

	function deleteQuestion($id) {
		$user = $this->ion_auth->user()->row();
      	$user_id = $user->id;
		$query = $this->db->get_where("questions",array("id"=>$id));
		if($query->num_rows() > 0)
		{
			$query = $query->row();
			$this->db->select("pt.*");
			$this->db->from("practicing_topics as pt");
			$this->db->join("questions as q","q.topicId = pt.topic_id");
			$this->db->where("q.id",$id);
			$this->db->where("pt.practiceaccount_id",$user_id);
			$q = $this->db->get();
			//debug_array($q);
			if($q->num_rows()>0)
			{
				$this->db->update('questions',array("is_deleted"=>1),array("id"=>$id));
				$this->db->update('questions',array("is_deleted"=>1),array("parentId"=>$id));
			}
			else
			{
				$this->db->set("is_deleted_by","CONCAT(is_deleted_by,',',".$user_id.")",FALSE);
				$this->db->where(array("id"=>$id));
				$this->db->update('questions');

				$this->db->set("is_deleted_by","CONCAT(is_deleted_by,',',".$user_id.")",FALSE);
				$this->db->where(array("parentId"=>$id));
				$this->db->update('questions');
			}
		}
	}

	function addQuestion($sortIndex) {
		$data = array(
			'question' => $this->input->post('questionName'),
			'topicId' => $this->input->post('topicId'),
			'type' => $this->input->post('type'),
			'sortIndex' => $sortIndex,
			'parentId' => $this->input->post('parentId'),
            'parentOption' => $this->input->post('parentOption'),
            'employeesNotice' => $this->input->post('employeesNotice'),
            'showNotice' => $this->input->post('showNotice')
		);
		$this->db->insert('questions', $data);
	}

	function updateQuestionsOrder($i, $v) {
			$data = array(
				'sortIndex' => $i
			);
			$this->db->where('id', $v);
			$this->db->update('questions', $data);
	}
 
	function editQuestion($id) {
		$data = array(
			'question' => $this->input->post('questionName'),
			'topicId' => $this->input->post('topicId'),
			'type' => $this->input->post('type'),
			'parentId' => $this->input->post('parentId'),
			'parentOption' => $this->input->post('parentOption'),
			'employeesNotice' => $this->input->post('employeesNotice'),
            'showNotice' => $this->input->post('showNotice')
		);
		$this->db->where('id', $id);
		$this->db->update('questions', $data);
	}

	function addEmailFolder($companyId) {
		$data = array(
			'companyId' => $companyId,
			'folder' => $this->input->post('folder'),
			'email' => $this->input->post('email'),
			'password' => sha1(salt.$this->input->post('password')),
			'host' => $this->input->post('host')
		);
		$this->db->insert('emailreports', $data);
	}

	function getFolder($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('emailreports');
		return $query->row_array();
	}

	function getCompanyReports($cId) {
		$this->db->where('companyId', $cId);
		$query = $this->db->get('emailreports');
		return $query->result();
	}

	function getQuestion($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('questions');
		return $query->row_array();
	}
	function get_company_topics($id) {
		$this->db->select("mt.*,ct.*");
		$this->db->from("company_topics as ct");
		$this->db->join("meeting_topics as mt","ct.topic_id = mt.id");
		$this->db->where('ct.company_id', $id);
		$this->db->where('ct.is_deleted', 0);
		$query = $this->db->get();
		return $query->result();
	}
	function getvisits() {
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;	

		$this->db->select("v.*,c.companyName");
		$this->db->from("visits as v");
		$this->db->join("company as c","c.id = v.company_id");
		$this->db->where('v.visitor_id', $user_id);
		$this->db->order_by('v.created_on', "DESC");
		$query = $this->db->get();
		return $query->result();

	}
	function addvisit() {
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;		
		$data = array(
			"visit_name"=> $this->input->post('visit_name'),
			"start_date"=> $this->input->post('start_date'),
			"visit_duration"=> $this->input->post('duration'),
			"comments"=> $this->input->post('comments'),
			"company_id"=> $this->input->post('company_id'),
			"visitor_id"=> $user_id
			);
	return $this->db->insert('visits', $data);
		
	}
	function get_company_non_topics($id) {
		$this->db->select("mt.*");
		$this->db->from("meeting_topics as mt");
		$this->db->join("company_topics as ct","mt.id = ct.topic_id","left outer");

		$this->db->where('ct.company_id != '.$id.' or ct.company_id ='.NULL);
		$query = $this->db->get();
		return $query->result();
	}
		function getCompany_wb($userId) {
		$this->db->select('c.id,c.userId,b.name, w.whiteCollarName');
		$this->db->from('company as c');
		$this->db->join('numbersofbluecollars as b', 'b.companyId = c.id',"left");
		$this->db->join('numbersofwhitecollars as w', 'w.companyId = c.id', 'left');
		$this->db->where('c.userId', $userId);

		//$this->db->where('numbersofwhitecollars.whiteCollarYear', date("Y"));
		//$this->db->where('numbersofbluecollars.year', date("Y"));
		$this->db->order_by('b.year', 'DESC');
		$this->db->order_by('w.whiteCollarYear', 'DESC');
		$query = $this->db->get();
		
		return $query->row_array();
	}

	function companies($doctor_id=null) {

		$user = $this->ion_auth->user()->row();
		if($doctor_id == null)
		{
			$user_id = $user->id;		
		}
		else
		{
			$user_id = $doctor_id;
		}
		$this->db->select('c.*,u.first_name,u.last_name');
		$this->db->from("doctors_companies as dc");
		$this->db->join("company as c","c.id = dc.company_id");
		$this->db->join("users as u","u.id = c.userId");
		$this->db->where('dc.user_id',$user_id);
		$this->db->where('c.is_deleted',0);
		$query = $this->db->get();
		$companies = $query->result();
		$return_arr = new stdclass();
		$i=0;

		foreach ($companies as $company)
		{
			$temp_com =(Object) $this->getCompany_wb($company->userId);
			$return_arr->$i =(Object) array_merge((array)$company,(array)$temp_com);
			$i++;
		}
		return $return_arr;
	}
function admin_users() {

		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;	
		$this->db->select("*,u.id as user_id")
				->from("users_groups as ug")
				->join("users as u","u.id=ug.user_id")
				->join("practicing_members as pm","u.id=pm.createduser_id")
				->where("pm.practiceaccount_id = ",$user_id)
				->where("ug.group_id",2)
				->order_by("u.created","DESC");
		$query = $this->db->get();
		//debug_array($this->db->queries);
		return $query->result();
	}
function practice_doctors() {

		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;	
		$this->db->select("*,u.id as user_id")
				->from("users_groups as ug")
				->join("users as u","u.id=ug.user_id")
				->join("practicing_members as pm","u.id=pm.createduser_id")
				->where("pm.practiceaccount_id = ",$user_id)
				->where("ug.group_id",4)
				->order_by("u.created","DESC");
		$query = $this->db->get();
		//debug_array($this->db->queries);
		return $query->result();
	}
function doctors_companies($doc_id) {

		
		$this->db->select("c.*")
				->from("doctors_companies as dc")
				->join("company as c","c.id=dc.company_id")
				
				->where("dc.user_id = ",$doc_id);
		$query = $this->db->get();
		//debug_array($this->db->queries);
		return $query->result();
	}
	function addcompanies_to_doctor()
	{
		$companies = $this->input->post('company_id');
		foreach ($companies as $company)
		{
			$data = array(
			"company_id"=>$company,
				'user_id' => $this->input->post('doctor_id')
			);
			$q = $this->db->get_where("doctors_companies",$data);
			if($q->num_rows()<=0)
			$this->db->insert('doctors_companies', $data);
		}
	}
	function turn_members_into_clients($user = NULL)
  {	
  		
		$client = new Client($user->email);
		$client->name = $user->first_name.' '.$user->last_name;
		$client->work_phone = $user->phone;
		$client->save();
		$this->db->insert("members_as_clients",array("member_id"=>$user->user_id,"client_id"=>$client->id));
  		return $client;
  }
	function addNotif($userId, $questionId) {
		$data = array(
			'userId' => $userId,
			'topicId' => $this->input->post('topicId'),
			'questionId' => $questionId
		);
		$this->db->insert('newquestionsnotif', $data);
	}

	function getNotifs() {
		$this->db->select();
		$this->db->from('newquestionsnotif');
		$this->db->join('users', 'users.id = newquestionsnotif.userId');
		$query = $this->db->get();
		return $query->result();
	}

	function getNewQuestionsByUserId($userId) {
		$this->db->select();
		$this->db->from('questions');
		$this->db->join('newquestionsnotif', 'newquestionsnotif.questionId = questions.id');
		$this->db->where('newquestionsnotif.userId', $userId);
		$query = $this->db->get();
		
		return $query->result();
	}

	function getUsers() {
		$query = $this->db->get('users');
		return $query->result();
	}

	function getHazardGroups() {
		$query = $this->db->get('hazardgroups');
		return $query->result();
	}

	function getHazards() {
		$query = $this->db->get('hazards');
		return $query->result();
	}

	function addHazard() {
		$data = array(
			'hazardName' => $this->input->post('hazardName'),
			'groupId' => $this->input->post('groupId'),
			'class' => $this->input->post('class'),
			'regularTerm' => $this->input->post('regularTerm'),
			'secondTerm' => $this->input->post('secondTerm'),
			'aplicableCode' => $this->input->post('applicableCode')
		);
		$this->db->insert('hazards', $data);
	}

	function addBg() {
		$data = array(
			'bg' => $this->input->post('bg')
		);
		$this->db->insert('numberofbg', $data);

		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$data = array(
			'bg_id' => $this->getLastBg()['id'],
			'practiceaccount_id' => $user_id
		);
		$this->db->insert('practicing_bgs', $data);
	}

	function getBgs() {

		$query = $this->db->get_where("numberofbg",array("is_deleted"=>0));
		
		return $query->result();

    }

    function getLastBg() {

		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
    	$query = $this->db->get_where('numberofbg',array("is_deleted"=>0));
		return $query->row_array();

    }

    function getBg($id) {
        $this->db->where('id', $id);
        $this->db->where('is_deleted', 0);
        $query = $this->db->get('numberofbg');
		return $query->row_array();
    }

    function editBg($id) {
        $data = array(
            'bg' => $this->input->post('bg')
        );
        $this->db->where('id', $id);
        $this->db->update('numberofbg', $data);
    }

	function getCompanyById($id) {
		$this->db->where('id', $id);
		$this->db->where('is_deleted', 0);
		$query = $this->db->get('company');
		return $query->row_array();
	}

	function getCompany($id) {

	$this->db->select('c.id as companyId, userId, companyName, companyStreet, companyHouseNumber, companyZip,
		companyCity, safetyPersonPhone, companyPhone, companyWebsite, safetyPersonEmail, numberOfBg, safetyPersonName, directAdresseeForOshaNames,
		directAdresseeForOshaEmail, directAdresseeForOshaNumber, b.name, w.whiteCollarName, extraQuestionPopupChecked');
		$this->db->from('company as c');
		$this->db->join('numbersofbluecollars as b', 'c.id = b.companyId','left');
		$this->db->join('numbersofwhitecollars as w', 'c.id = w.companyId', 'left');
		$this->db->where('c.id', $id);
		//$this->db->where('numbersofwhitecollars.whiteCollarYear', date("Y"));
		//$this->db->where('numbersofbluecollars.year', date("Y"));
		$this->db->order_by('b.year', 'DESC');
		$this->db->order_by('w.whiteCollarYear', 'DESC');
		$query = $this->db->get();
	
		return $query->row_array();
	}

	function deleteCompany($id) {
		$this->db->update('company',array("is_deleted"=>1),array("id"=>$id));
	}

	function updateCompany($id) {
		$this->db->where('id', $id);
		$query = $this->db->get("company");
		$workerNumber = $this->input->post('numberOfBlueCollar')  + $this->input->post('numberOfWhiteCollar');  
    	 $extraQuestionPopupChecked = 2;  
         if($workerNumber > 20) {
         		$extraQuestionPopupChecked = 1;
         	}
		$data = array(
			"companyName" => $this->input->post('companyName'),
			"companyStreet" => $this->input->post('companyStreet'),
			"companyHouseNumber" => $this->input->post('companyHouseNumber'),
			"companyZip" => $this->input->post('companyZip'),
			"companyCity" => $this->input->post('companyCity'),
			"companyPhone" => $this->input->post('companyPhone'),
			"companyWebsite" => $this->input->post('companyWebsite'),
			"numberOfBg" => $this->input->post('numberOfBg'),
			"safetyPersonName" => $this->input->post('safetyPersonName'),
			"safetyPersonEmail" => $this->input->post('safetyPersonEmail'),
			"safetyPersonPhone" => $this->input->post('safetyPersonPhone'),
			"directAdresseeForOshaNames" => $this->input->post('directAdresseeForOshaNames'),
			"directAdresseeForOshaEmail" => $this->input->post('directAdresseeForOshaEmail'),
			"directAdresseeForOshaNumber" => $this->input->post('directAdresseeForOshaNumber'),
			"extraQuestionPopupChecked" => $extraQuestionPopupChecked

		);
		$companyId = 0;
		if($query->num_rows() == 1) {
			$this->db->where('id', $id);
			$this->db->update('company', $data);
			$ret = $query->row();
			$companyId = $ret->id;
		} else {
			$this->db->insert('company', $data);
			$companyId = $this->db->insert_id();
		}

		$this->db->where('companyId', $companyId);
		$query = $this->db->get("numbersofbluecollars");
		$date = new DateTime();
		$data = array(
			"companyId" => $companyId,
			"name" => $this->input->post('numberOfBlueCollar'),
			"year" => $date->format('Y-m-d H:i:s'),
		);

		if($query->num_rows() == 1) {
			$this->db->where('companyId', $companyId);
			$this->db->update('numbersofbluecollars', $data);
		} else {
			$this->db->insert('numbersofbluecollars', $data);
		}

		$this->db->where('companyId', $companyId);
		$this->db->where('whiteCollarYear', date("Y"));
		$query = $this->db->get("numbersofwhitecollars");

		$data = array(
			"companyId" => $companyId,
			"whiteCollarName" => $this->input->post('numberOfWhiteCollar'),
			"whiteCollarYear" => $date->format('Y-m-d H:i:s'),
		);

		if($query->num_rows() == 1) {
			$this->db->where('companyId', $companyId);
			$this->db->update('numbersofwhitecollars', $data);
		} else {
			$this->db->insert('numbersofwhitecollars', $data);
		}

	}

	function changeQuestionTopic($topicId, $questionId) {
		$data = array(
			'topicId' => $topicId
		);
		$this->db->where('id', $questionId);
		$this->db->update('questions', $data);
	}

	function deleteBCollar($id) {
		$this->db->where('id', $id);
		$this->db->delete('numbersofbluecollars');
	}

	function deleteWCollar($id) {
		$this->db->where('id', $id);
		$this->db->delete('numbersofwhitecollars');
	}

	function getQuestionsByTopic($id) {
		$this->db->where('topicId', $id);
		$this->db->where('is_deleted', 0);
		$query = $this->db->get('questions');
		return $query->result_array();
	}

	function getQuestionsSortIndexByTopic($id) {
		$this->db->select('sortIndex');
		$this->db->where('topicId', $id);
		$this->db->where('is_deleted', 0);
		$query = $this->db->get('questions');
		return $query->result_array();
	}

	function getNumberOfBlueCollars($companyId, $date) {
		$this->db->where('companyId', $companyId);
		$this->db->where("DATE_FORMAT(year,'%Y-%m') = '".$date."'");
		$this->db->order_by("year", "DESC");
		$query = $this->db->get('numbersofbluecollars');
		return $query->row_array();
	}

	function getNumberOfWhiteCollars($companyId, $date) {
		$this->db->where('companyId', $companyId);
		$this->db->where("DATE_FORMAT(whiteCollarYear,'%Y-%m') = '".$date."'");
		$this->db->order_by("whiteCollarYear", "DESC");
		$query = $this->db->get('numbersofwhitecollars');
		return $query->row_array();
    }
    
    function getQuestionByParentId($id) {
		$this->db->where('parentId', $id);
		$this->db->where('is_deleted',0);
		$query = $this->db->get('questions');
		return $query->result();
	}

	function addWarningDates() {
		$data = array(
			'dateOne' => $this->input->post('dateOne'),
			'dateTwo' => $this->input->post('dateTwo'),
			'dateThree' => $this->input->post('dateThree'),
		);
		$this->db->where('id', 0);
		$this->db->update('warningdates', $data);
	}

	function getWdates() {
		$this->db->where('id', 0);
		$query = $this->db->get("warningdates");
		return $query->row_array();
	}

	function getUserById($id) {
		$this->db->where('id', $id);
		$query = $this->db->get("users");
		return $query->row_array();
    }
    
    function seizeCompany($id, $seized) {
        $data = array(
			'seized' => $seized
		);
		$this->db->where('id', $id);
		$this->db->update('company', $data);
    }

    function verify_companyByCode($id,$code) {
		$this->db->where(array('company_id'=> $id,"verification_code"=>$code));
		$query = $this->db->get('email_verification');
		return $query->num_rows();
	}
    function com_verfication($id,$code){

	    $response = $this->verify_companyByCode($id,$code);
	    if($response > 0)
	    {
		    	$data = array(
				'code_expired' => 1
			);
			$this->db->where(array('company_id'=> $id,"verification_code" => $code));
			$this->db->update('email_verification', $data);
			$this->seizeCompany($id,0);
			return true;


	    }
	    else return false;

    }
     function verfication_email($email,$id){

			$data = [
				'company_id'         => $id,
				'verification_code' => substr(md5(strtotime(date('d F Y h:i:s'))),0,25)
			];
				$message = "<center><h2>Company Verfication Email</h2><br><p><a href='".base_url().'practice/company/com_verfication/'.$data['company_id']."/".$data['verification_code']."'>Click on this link</a> to verify your company. Please do not share this email to anyone.</p></center>";

				$this->email->clear();
				$this->email->from($this->config->item('admin_email', 'ion_auth'), $this->config->item('site_title', 'ion_auth'));
				$this->email->to($email);
				$this->email->subject("Verification Email");
				$this->email->message($message);
				
				if(!empty($email))
				{

				    // check if e-mail address is well-formed
				    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
						echo "<script>alert('Invalid Email address');</script>";
				    }
				    else
				    {
						if ($this->email->send() === TRUE)
						{
							$this->db->insert('email_verification', $data);
							$this->seizeCompany($id,1);
							return 1;
						}
						else
						{
							return false;
						}
					}
				}
				else
				{
					echo "<script>alert('No e-mail address found');</script>";
					return false;
				}

    	}
}