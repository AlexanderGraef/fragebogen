<?php

class InfluenzaModel extends CI_Model {

  function influenza()
  {
  	$user = $this->ion_auth->user()->row();
	$user_id = $user->id; 
	$practice = $this->AdminModel->members_parent($user_id);
	if(isset($practice[0]))
	{
		$practice = $practice[0];
		$practiceaccount_id = $practice->practiceaccount_id;
	  	$this->db->select("in.*");
	  	$this->db->from("scheduled_influenza as in");
	  	$this->db->where(array("practiceaccount_id"=>$practiceaccount_id,"mark_sent"=>1,"is_deleted"=>0));
	  	$this->db->order_by("send_date","DESC");
	  	$this->db->limit(1);
	  	$inf =  $this->db->get();
	  	if($inf->num_rows()>0)
	  	{
	  		$influenza = $inf->row();
	  		$res = $this->db->get_where("influenza_response",array("influenza_id"=>$influenza->id,"user_id"=>$user_id));
	  		if($res->num_rows()>0)
	  		{
	  			return false;
	  		}
	  		return $influenza;
	  	}
	  
	}
	else
	{
		return false;
	}
  	

  }
 function respond_influenza()
  {
  	$user = $this->ion_auth->user()->row();
	$user_id = $user->id;
  	$influenza_id = $this->input->post("influenza_id");
  	$influenza_response = $this->input->post("influenza_response");
  	$this->db->insert("influenza_response",array("influenza_id"=>$influenza_id,"	response"=>$influenza_response,"user_id"=>$user_id));
  	if($this->db->affected_rows()>0)
  	{
  		return "Submitted Successfully.";
  	}
  	else
  	{
  		return "Error Occured";
  	}
  }
}

?>
