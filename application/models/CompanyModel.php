<?php
	use InvoiceNinja\Config as NinjaConfig;
   		use InvoiceNinja\Models\Invoice;
   		use InvoiceNinja\Models\Client;
   		
class CompanyModel extends CI_Model {

	function __construct()
	{
		$token = $this->db->get_where('users_tokens',array('user_id'=>$this->session->userdata('user_id')))->row();
		if(isset($token->token) && isset($token->token_url))
		{
			$this->config->set_item('invoice_ninja_token', $token->token);
			$this->config->set_item('invoice_ninja_url', $token->token_url);
	   		NinjaConfig::setURL($this->config->item('invoice_ninja_url'));
	     	NinjaConfig::setToken($this->config->item('invoice_ninja_token'));	
		}
	}
	
	function getCompany($userId) {
		$this->db->select('c.id as companyId, userId, companyName, companyStreet, companyHouseNumber, companyZip,
		companyCity, safetyPersonPhone, companyPhone, companyWebsite, safetyPersonEmail, numberOfBg, safetyPersonName, 
		directAdresseeForOshaNames, directAdresseeForOshaEmail, w.whiteCollarYear as wYear, 
		b.year as bYear, directAdresseeForOshaNumber, b.name, 
		w.whiteCollarName, extraQuestionPopupChecked,c.extra_text');
		$this->db->from('company as c');
		$this->db->join('numbersofbluecollars as b', 'b.companyId = c.id',"left");
		$this->db->join('numbersofwhitecollars as w', 'w.companyId = c.id', 'left');
		$this->db->where('c.userId', $userId);

		//$this->db->where('numbersofwhitecollars.whiteCollarYear', date("Y"));
		//$this->db->where('numbersofbluecollars.year', date("Y"));
		$this->db->order_by('b.year', 'DESC');
		$this->db->order_by('w.whiteCollarYear', 'DESC');
		$query = $this->db->get();
		
		return $query->row_array();
	}

	function updateCompany($userId) {
		if($this->input->post('email')!=$this->user->email)
		{
			if(empty($this->db->get_where("users",array("email"=>$this->input->post('email')))->row()))
			{
				$email_hash = md5(strtotime(date('y-m-d h:i:s'))+12451);
	            $e = mail($this->input->post('email'), "Change email request", "Click on the link if you requested to change your email address <br><a href='".base_url()."auth/confirm_email?email=".$this->input->post('email')."&hash=".$email_hash."'>Confirm Email</");
	            $up_user= $email_hash.'&'.$this->input->post('email');
	            if($e)
	            {
	            	$this->db->update("users",array("email_hash"=>$up_user),array("id"=>$this->user->id));
	            }
	            else
	            {
		        	echo "<script>alert('Error ocured'); window.location.assign('".base_url()."')</script>";
	            }
	        }
	        else
	        {
	        	echo "<script>alert('Email already in use'); window.location.assign('".base_url()."')</script>";
	        }
		}
		$this->db->where('userId', $userId);
    	$query = $this->db->get("company");
    	 $workerNumber = $this->input->post('numberOfBlueCollar')  + $this->input->post('numberOfWhiteCollar');  
    	 $extraQuestionPopupChecked = 0;  
         if($workerNumber < 20) {
         		$extraQuestionPopupChecked = 2;
         	}
         if($workerNumber > 30) {
         		$extraQuestionPopupChecked = 1;
         	}
         
		$data = array(
			"userId" => $userId,
			"companyName" => $this->input->post('companyName'),
			"companyStreet" => $this->input->post('companyStreet'),
			"companyHouseNumber" => $this->input->post('companyHouseNumber'),
			"companyZip" => $this->input->post('companyZip'),
			"companyCity" => $this->input->post('companyCity'),
			"companyPhone" => $this->input->post('companyPhone'),
			"companyWebsite" => $this->input->post('companyWebsite'),
			"numberOfBg" => $this->input->post('numberOfBg'),
			"safetyPersonName" => $this->input->post('safetyPersonName'),
			"safetyPersonEmail" => $this->input->post('safetyPersonEmail'),
			"safetyPersonPhone" => $this->input->post('safetyPersonPhone'),
			"directAdresseeForOshaNames" => $this->input->post('directAdresseeForOshaNames'),
			"directAdresseeForOshaEmail" => $this->input->post('directAdresseeForOshaEmail'),
			"directAdresseeForOshaNumber" => $this->input->post('directAdresseeForOshaNumber'),
			"extraQuestionPopupChecked" => $extraQuestionPopupChecked,
			"extra_text" => $this->input->post('extra_text')
		);
		$companyId = 0;
		if($query->num_rows() == 1) {
			$this->db->where('userId', $userId);
			$this->db->update('company', $data);
			$ret = $query->row();
			$companyId = $ret->id;
		} else {
			$this->db->insert('company', $data);
			$companyId = $this->db->insert_id();
		}

		$date = new DateTime();
		$data = array(
			"companyId" => $companyId,
			"name" => $this->input->post('numberOfBlueCollar'),
			"year" => $date->format('Y-m-d H:i:s'),
		);
		if($this->input->post('numberOfBlueCollar') != "") {
			$this->db->insert('numbersofbluecollars', $data);
		}

		$data = array(
			"companyId" => $companyId,
			"whiteCollarName" => $this->input->post('numberOfWhiteCollar'),
			"whiteCollarYear" => $date->format('Y-m-d H:i:s'),
		);

		if($this->input->post('numberOfWhiteCollar') != "") {
			$this->db->insert('numbersofwhitecollars', $data);
		}
		if($this->input->post('practiceaccount') != "")
			{			
				$user = $this->ion_auth->user()->row();
				$user_id = $user->id;
				$this->db->insert('practice_joining_requests', array("practiceaccount_id"=>$this->input->post('practiceaccount'),"user_id"=>$user_id));
			}
		$receive_invoices = (isset($_POST['receive_invoices'])) ? 1 : 0;
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$this->db->where('member_id', $user_id);
	   	$this->db->delete('invoice_preferences'); 
	   	$this->db->insert("invoice_preferences",array("member_id"=>$user_id));
		if($this->input->post('invoice_email') != "")
		{	
				
				$practice = $this->AdminModel->members_parent($user_id);
				$token = $this->db->get_where('users_tokens',array('user_id'=>$practice[0]->practiceaccount_id))->row();
			if($this->InvoiceModel->validate_token_url($token->token,$token->token_url))
			{
				$this->config->set_item('invoice_ninja_token', $token->token);
		   		NinjaConfig::setURL($this->config->item('invoice_ninja_url'));
		     	NinjaConfig::setToken($this->config->item('invoice_ninja_token'));		
			
				$client = Client::find($this->AdminModel->client_as_member($user_id)[0]->client_id);
				$client->contacts[1]->email = $this->input->post('invoice_email');
				$client->save();
			}
				$this->db->update('invoice_preferences', array("invoice_email"=>$this->input->post('invoice_email'),"receive_invoices"=>$receive_invoices),array("member_id"=>$user_id));
				
			}
			else if($this->input->post('invoice_email') == "")
			{
				$practice = $this->AdminModel->members_parent($user_id);
				$token = $this->db->get_where('users_tokens',array('user_id'=>$practice[0]->practiceaccount_id))->row();
				if(isset($token->token))
				{
					$this->config->set_item('invoice_ninja_token', $token->token);
			   		NinjaConfig::setURL($this->config->item('invoice_ninja_url'));
			     	NinjaConfig::setToken($this->config->item('invoice_ninja_token'));	
					$client = Client::find($this->AdminModel->client_as_member($user_id)[0]->client_id);
					$client->contacts[1]->email = $this->ion_auth->user()->row()->email;
					$client->save();
				}
					$this->db->update('invoice_preferences', array("invoice_email"=>$this->ion_auth->user()->row()->email,"receive_invoices"=>$receive_invoices),array("member_id"=>$user_id));
			}
			
		
	}

function getpracticeBgs($uid=null) {
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		if($uid==NULL)
		$uid=isset($this->members_parent($user_id)[0])?$this->members_parent($user_id)[0]->createduser_id:'';
		if($uid !='')
		{
			$this->db->select("nob.*")
			->from('numberofbg as nob')
			->join("practicing_bgs as pbgs","nob.id = pbgs.bg_id")
			->join("users as u","u.id = pbgs.practiceaccount_id")
			->where("u.id",$uid);
			$query = $this->db->get();
			return $query->result();
		}
		else
		{
			return FALSE;
		}
    }
	function getNumberOfBlueCollars($companyId) {
		$this->db->where('companyId', $companyId);
		$this->db->order_by("year", "DESC");
		$query = $this->db->get('numbersofbluecollars');
		return $query->result();
	}

	function getNumberOfWhiteCollars($companyId) {
		$this->db->where('companyId', $companyId);
		$this->db->order_by("whiteCollarYear", "DESC");
		$query = $this->db->get('numbersofwhitecollars');
		return $query->result();
	}

	function updateNumberOfBlueCollars($companyId) {
		$this->db->where('companyId', $companyId);
		$this->db->where('year', date("Y"));
		$query = $this->db->get("numbersofbluecollars");

		$data = array(
			"numberOfBlueCollar" => $this->input->post('numberOfBlueCollar'),
		);

		if($query->num_rows() == 1) {
			$this->db->where('companyId', $companyId);
			$this->db->where('year', date("Y"));
			$this->db->update('numbersofbluecollars', $data);
		} else {
			$this->db->insert('numbersofbluecollars', $data);
		}
	}
	
	function noticeCheck($companyId, $check) {
		
		$data = array(
			'extraQuestionPopupChecked' => $check
		);
		$this->db->where('id', $companyId);
		$this->db->update("company", $data);
	}
}

?>
