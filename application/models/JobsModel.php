<?php
class JobsModel extends CI_Model {

	function getJobs() {
		$query = $this->db->get('jobs');
		return $query->result();
	}

	function getOccupations() {
		$query = $this->db->get('occupations');
		return $query->result();
	}

	function getHazardsByGroup($id) {
		$this->db->where('groupId', $id);
		$query = $this->db->get('hazards');
		return $query->result_array();
	}

	function addJob() {
		$data = array(
			'jobName' => $this->input->post('jobName')
		);
		$this->db->insert('jobs', $data);
	}

	function jobsHazards($jobId, $hazardId) {
		$data = array(
			'jobId' => $jobId,
			'hazardId' => $hazardId
		);
		$this->db->insert('jobshazards', $data);
	}

	function addOccupation() {
		$data = array(
			'occupationName' => $this->input->post('occupationName')
		);
		$this->db->insert('occupations', $data);
	}

	function jobsOccupations($occupationId, $job) {
		$data = array(
			'jobId' => $job,
			'occupationId' => $occupationId
		);
		$this->db->insert('jobsoccupations', $data);
	}

	function addWorker() {
		$data = array(
			'firstName' => $this->input->post('firstName'),
			'lastName' => $this->input->post('lastName'),
			'occupationId' => $this->input->post('occupationId')
		);
		$this->db->insert('workers', $data);
	}

	function getJobsHazards($occupationId) {
		$this->db->select('*');
		$this->db->from('jobsoccupations');
		$this->db->join('occupations', 'occupations.id = jobsoccupations.occupationId');
		$this->db->join('jobs', 'jobs.id = jobsoccupations.jobId');
		$this->db->join('jobshazards', 'jobshazards.jobId = jobs.id');
		$this->db->join('hazards', 'hazards.id = jobshazards.hazardId');
		$this->db->where('occupations.id', $occupationId);
		$query = $this->db->get();
		return $query->result_array();
	}
}
