<?php

$lang['our_company'] = 'Our Company';
$lang['questionnare'] = 'Questionnare';
$lang['logout'] = 'Logout';
$lang['your_company_info'] = 'Your company info';
$lang['company_name'] = 'Company name';
$lang['company_street'] = 'Company Address';
$lang['company_house_number'] = 'Company house number';
$lang['company_zip'] = 'Zip Code';
$lang['company_city'] = 'Company city';
$lang['company_phone'] = 'Phone number';
$lang['company_website'] = 'Company Website';
$lang['latest_number_of_blue_collars'] = 'Latest Number of blue collar';
$lang['latest_number_of_white_collars'] = 'Latest Number of white collar';
$lang['number_of_bg'] = 'BG number';
$lang['practice_accounts'] = 'Practice Accounts';
$lang['member'] = 'Member Name';
$lang['safety_person_name'] = 'Name of security specialist, also external! (SIFA)';
$lang['safety_person_email'] = 'SIFA Email, also External!';
$lang['safety_person_phone'] = 'SIFA phone number, also External!';
$lang['direct_adresse_for_osha_names'] = 'direct address for osha names (AMAP)';
$lang['direct_adresse_for_osha_email'] = 'AMAP Email';
$lang['direct_adresse_for_osha_number'] = 'AMAP Phone number';
$lang['save'] = 'Save';
$lang['number_of_blue_collars_in_past_time'] = 'Number of blue collars in past time';
$lang['number_of_blue_collar'] = 'Number of blue collar';
$lang['number_of_white_collars_in_past_time'] = 'Number of white collars in past time';
$lang['number_of_white_collar'] = 'Number of white collar';
$lang['date_added'] = 'Date added:';
$lang['choose_one_topic'] = 'Choose topic';
$lang['topic'] = 'Topic';
$lang['progress'] = 'Progress';
$lang['new'] = 'New';
$lang['answer_on_questions'] = 'Answer on question';
$lang['logout'] = 'Logout';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['status'] = 'Status';
$lang['send_date'] = 'Send Date';
$lang['send_emails'] = 'Send Date';
$lang['response'] = 'Response';
$lang['unsure'] = 'Unsure';
$lang['influenza'] = 'Influenza Immunization';
$lang['company_update_success_msg'] = 'Company update success message';
$lang['question_update_success_msg'] = 'Question Update success message';
?>
