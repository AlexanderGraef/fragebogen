<?php
$lang['new_questions_email_subject'] = 'Es wurden neue Fragen im Portal Ihres Werksarztzentrums eingestellt.';
$lang['new_questions_email_text'] = "Hier sind die noch zu beantwortenden Fragen:";
$lang['new_register_email_subject'] = "Ein neuer Nutzer hat sich registriert:";
$lang['new_register_email_text'] = "Ein neuer Benutzer hat sich unter dem folgenden Namen registriert: ";
$lang['confirm_email_subject'] = "Bitte bestätigen Sie Ihre Email Adresse. ";
$lang['confirm_email_text'] = "Sie haben sich erfolgreich im Portal Ihres Werksarztzentrums registriert. Bitte bestätigen Sie die Registrierung: ";
$lang['password_reset_email_subject'] = "Passwort zurücksetzen";
$lang['password_reset_email_text'] = "Klicken Sie auf diesen Link, um Ihr Passwort zurückzusetzen: ";
$lang['email_reminder_one'] = "Bitte aktualisieren Sie  die Anzahl Ihrer Mitarbeiter im Portal des Werksarztzentrums.";
$lang['email_reminder_two'] = "Wir möchten Sie noch einmal an die Aktualisierung Ihrer Mitarbeiterzahlen erinnern.";
$lang['email_reminder_three'] = "Leider haben Sie noch immer nicht Ihre Mitarbeiterzahlen aktualisiert.";
?>