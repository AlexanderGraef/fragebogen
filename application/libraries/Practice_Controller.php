<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
class Practice_Controller extends MY_Controller {
    public $is_admin;
    public $logged_in_name;

    function __construct() {
        parent::__construct();

        // Set container variable
        $this->_container = $this->config->item('ci_my_admin_template_dir_practice') . "layout.php";
        $this->_modules = $this->config->item('modules_locations');

        $this->load->library(array('ion_auth'));

        if (!$this->ion_auth->logged_in()) {
            redirect('/auth', 'refresh');
        }

        if(!$this->session->userdata('language')) {
            $this->session->set_userdata('language', 'german');
        }
        $idiom = $this->session->userdata('language');
        $this->lang->load('admin_lang', $idiom);
        $this->lang->load('main_lang', $idiom);
        $this->lang->load('auth_lang', $idiom);
        $this->lang->load('ion_auth_lang', $idiom);

        $this->is_admin = $this->ion_auth->is_practice();
        $user = $this->ion_auth->user()->row();

        $this->logged_in_name = $user->first_name;

        log_message('debug', 'CI My Practice : Admin_Controller class loaded');
    }
}