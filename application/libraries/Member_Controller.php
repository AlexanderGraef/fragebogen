<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
class Member_Controller extends MY_Controller {
    public $is_admin;
    public $logged_in_name;
    public $user;

    function __construct() {
        parent::__construct();
        
        // Set container variable
        $this->_container = $this->config->item('ci_my_admin_template_dir_public') . "layout.php";
        $this->_modules = $this->config->item('modules_locations');

        $this->load->library(array('ion_auth'));

        if (!$this->ion_auth->logged_in()) {
            redirect('/auth', 'refresh');
        }

        if(!$this->session->userdata('language')) {
            $this->session->set_userdata('language', 'german');
        }
        $idiom = $this->session->userdata('language');
        $this->lang->load('main_lang', $idiom);
    
        $this->is_admin = $this->ion_auth->is_admin();
        $this->user = $this->ion_auth->user()->row();
        $this->logged_in_name = $this->user->first_name;

        log_message('debug', 'CI My Admin : Admin_Controller class loaded');
    }
}