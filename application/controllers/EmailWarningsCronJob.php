<?php

class EmailWarningsCronJob extends CI_Controller {

    public function index() {
        $this->load->library('email');
		 $today = date("Y-m-d");
		 $warningDates = $this->AdminModel->getWdates();
         $companies = $this->AdminModel->companies();
		 //First Email
		 if($warningDates['dateOne'] == $today) {
			
			$config=array(
				'charset'=>'utf-8',
				'wordwrap'=> TRUE,
				'mailtype' => 'html'
				);
				
			$this->email->initialize($config);
			foreach($companies as $c) {
				$user = $this->AdminModel->getUserById($c->userId);
				$this->email->from($this->config->item('email_from'), $this->config->item('name_from'));
                $this->email->to($user->username);
				$this->email->subject("Worker Number reminder 1");
				$msg = $this->load->view('emails/reminderOne', '', true);
				$this->email->message($msg);
				$this->email->send();
			}
		 }
		 //Second Email
		else if(isset($warningDates['dateTwo']) && $warningDates['dateTwo'] == $today) {
			$config=array(
				'charset'=>'utf-8',
				'wordwrap'=> TRUE,
				'mailtype' => 'html'
				);
				
			$this->email->initialize($config);
			foreach($companies as $c) {
				$bCollars = $this->CompanyModel->getNumberOfBlueCollars($c->id);
				$wCollars = $this->CompanyModel->getNumberOfWhiteCollars($c->id);
				$singleCompany = $this->CompanyModel->getCompany($c->userId);
				if(($singleCompany['bYear'] < $warningDates['dateTwo'] || $singleCompany['wYear'] < $warningDates['dateTwo'])
				&& !($singleCompany['bYear'] > $warningDates['dateOne'] || $singleCompany['wYear'] > $warningDates['dateOne'])) {
					$user = $this->AdminModel->getUserById($c->userId);
					$this->email->from($this->config->item('email_from'), $this->config->item('name_from'));
                    $this->email->to($user->username);
					$this->email->subject("Worker Number reminder 2");
					$msg = $this->load->view('emails/reminderTwo', '', true);
					$this->email->message($msg);
					$this->email->send();
				}
			}
         }
         
        //Third Email
		else if(isset($warningDates['dateThree']) && $warningDates['dateThree'] == $today) {
			$config=array(
				'charset'=>'utf-8',
				'wordwrap'=> TRUE,
				'mailtype' => 'html'
				);
				
			$this->email->initialize($config);
			foreach($companies as $c) {
				$bCollars = $this->CompanyModel->getNumberOfBlueCollars($c->id);
				$wCollars = $this->CompanyModel->getNumberOfWhiteCollars($c->id);
				$singleCompany = $this->CompanyModel->getCompany($c->userId);
				if(($singleCompany['bYear'] < $warningDates['dateThree'] || $singleCompany['wYear'] < $warningDates['dateThree'])
				&& !($singleCompany['bYear'] > $warningDates['dateOne'] || $singleCompany['wYear'] > $warningDates['dateOne'])) {
					$user = $this->AdminModel->getUserById($c->userId);
					$this->email->from($this->config->item('email_from'), $this->config->item('name_from'));
                    $this->email->to($user->username);
					$this->email->subject("Worker Number reminder 3");
					$msg = $this->load->view('emails/reminderThree', '', true);
					$this->email->message($msg);
					$this->email->send();
				}
			}
        }
        
        if(isset($warningDates['dateThree']) && $today == date('Y-m-d', strtotime($warningDates['dateThree']. ' + 3 days'))) {
            
			foreach($companies as $c) {
				$bCollars = $this->CompanyModel->getNumberOfBlueCollars($c->id);
				$wCollars = $this->CompanyModel->getNumberOfWhiteCollars($c->id);
				$singleCompany = $this->CompanyModel->getCompany($c->userId);
				if(!($singleCompany['bYear'] > $warningDates['dateOne'] || $singleCompany['wYear'] > $warningDates['dateOne'])) {
					$this->AdminModel->seizeCompany($c->id, true);
				}
			}
        }
    }

}