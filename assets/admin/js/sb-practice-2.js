(function($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function() {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });


/* $('.row_position').on('click', 'tr', function () {
    var id = $(this).attr("id")
    $(this).toggleClass('selected');
    $('tr[data-parent="'+ id +'"]').toggleClass('selected');
 }); */

 $( "table" ).sortable({
    delay: 150,
    stop: function() {
        var selectedData = new Array();
        $('tbody.row_position').each(function() {
            selectedData.push($(this).attr("id"));
        });
        updateOrderMove(selectedData);
    }
});

/* $( ".row_position" ).sortable({
    delay: 150,
    connectWith: '.row_position',
    opacity: 0.6,
    revert: true,
    helper: function (e, item) { //create custom helper
      var id = item.attr("id")
        if(!item.hasClass('selected'))
           item.addClass('selected');
           $('tr[data-parent="'+ id +'"]').toggleClass('selected');
        // clone selected items before hiding
        var elements = $('.selected').not('.ui-sortable-placeholder').clone();
        //hide selected items
        item.siblings('.selected').addClass('hidden');
        //$('tr[data-parent="'+ id +'"]').siblings('.selected').addClass('hidden');
        var helper = $('<table/>');
        return helper.append(elements);
    },
    start: function (e, ui) {
        var elements = ui.item.siblings('.selected.hidden').not('.ui-sortable-placeholder');
        //store the selected items to item being dragged
        ui.item.data('items', elements);
    },
    receive: function (e, ui) {
        //manually add the selected items before the one actually being dragged
        ui.item.before(ui.item.data('items'));
        var parentId = ui.item.attr("id");
        $('tr[data-parent="'+ parentId +'"]').before(ui.item.data('items'));
    },
    stop: function (e, ui) {
        //show the selected items after the operation
        ui.item.siblings('.selected').removeClass('hidden');
        var parentId = ui.item.attr("id");
        $('tr[data-parent="'+ parentId +'"]').siblings('.selected').removeClass('hidden');
        //unselect since the operation is complete
        $('.selected').removeClass('selected');
        var selectedData = new Array();
        $('.row_position>tr').each(function() {
            selectedData.push($(this).attr("id"));
        });
        //updateOrderDrag(selectedData);
    }
}); */

/* $('a.moveUp').click(function(){
    var id = $(this).parents('.roww').attr("id")
    $("tr").find('[data-parent="'+ id +'"]').toggleClass('selected');
      $('.selected').insertBefore($(this).parents('.roww').prev())
      $('tr[data-parent="'+ id +'"]').toggleClass('selected');
        // clone selected items before hiding
        var elements = $('.selected').not('.ui-sortable-placeholder').clone();
        //hide selected items
        item.siblings('.selected').addClass('hidden');
        //$('tr[data-parent="'+ id +'"]').siblings('.selected').addClass('hidden');
        var helper = $('<table/>');
        var parentId = ui.item.attr("id");
        $('tr[data-parent="'+ parentId +'"]').before(ui.item.data('items'));
        $('.selected').removeClass('selected');
      var selectedData = new Array();
        $('.row_position>tr').each(function() {
            selectedData.push($(this).attr("id"));
        });
        //updateOrderDrag(selectedData);
        
});

$('a.moveDown').click(function(){
    var id = $(this).parents('.roww').attr("id")
    console.log(id)
    $(this).parents('.roww').toggleClass('selected');
    $('tr[data-parent="'+ id +'"]').toggleClass('selected');
    $('.selected').insertAfter($('.selected').next())
      var selectedData = new Array();
        $('.row_position>tr').each(function() {
            selectedData.push($(this).attr("id"));
        });
        //updateOrderDrag(selectedData);
}); */

$(".moveUp, .moveDown").click(function() {
    var par = $(this).parents("tbody");
    if ($(this).is(".moveUp")) {
        par.insertBefore(par.prev());
        var selectedData = new Array();
        $('tbody.row_position').each(function() {
            selectedData.push($(this).attr("id"));
            console.log(selectedData)
        });
        updateOrderMove(selectedData);
        
    } else {
        par.insertAfter(par.next());
        var selectedData = new Array();
        $('tbody.row_position').each(function() {
            selectedData.push($(this).attr("id"));
        });
        updateOrderMove(selectedData);
    }
  });


$( ".row_position" ).disableSelection();

$('.roww #deleteQuestion').click(function(e) {
e.preventDefault();
if (confirm('Bist du sicher?'))
{
var link = $(this);
$.ajax({
    url: base_url + 'practice/questionnare/deleteQuestion/' + link.data('id'),
    dataType: 'json',
    success: function(data) {
    console.log(link.data('number'))
     
   }
})
var parentId = link.data('id')
    $(".roww[data-parent='" + parentId +"']").fadeOut('fast', function() {
        $(this).remove();
     }); 
link.parents('.roww').fadeOut('fast', function() {
              $(this).remove();
           });    
}

})
    
    $('.roww #deleteTopic').click(function(e) {
    e.preventDefault();
    if (confirm('Bist du sicher?'))
    {
        var link = $(this);
        $.ajax({
            url: base_url + 'practice/questionnare/deleteTopic/' + link.data('id'),
            dataType: 'json',
            success: function(data) {
            console.log(link.data('number'))
             
           }
        })
        link.parents('.roww').fadeOut('fast', function() {
                      $(this).remove();
                   });    
    }
    })
    
    $('.roww #deleteCompany').click(function(e) {
    e.preventDefault();
    if (confirm('Bist du sicher?'))
    {
        var link = $(this);
        $.ajax({
            url: base_url + 'practice/company/delete/' + link.data('id'),
            dataType: 'json',
            success: function(data) {
            console.log(link.data('number'))
             
           }
        })
        link.parents('.roww').fadeOut('fast', function() {
                      $(this).remove();
                   });    
    }
    })
    
    $('.roww #deleteBCollar').click(function(e) {
    e.preventDefault();
    if (confirm('Bist du sicher?'))
    {
        var link = $(this);
        $.ajax({
            url: base_url + 'practice/company/deleteBCollar/' + link.data('id'),
            dataType: 'json',
            success: function(data) {
            console.log(link.data('id'))
             
           }
        })
        link.parents('.roww').fadeOut('fast', function() {
                      $(this).remove();
                   });    
    }
    })
    
    $('.roww #deleteBg').click(function(e) {
    e.preventDefault();
    if (confirm('Bist du sicher?'))
    {
        var link = $(this);
        $.ajax({
            url: base_url + 'practice/bg/delete/' + link.data('id'),
            dataType: 'json',
            success: function(data) {
            console.log(link.data('id'))
             
           }
        })
        link.parents('.roww').fadeOut('fast', function() {
                      $(this).remove();
                   });    
    }
    })
    
    $('.roww #deleteWCollar').click(function(e) {
    e.preventDefault();
    if (confirm('Bist du sicher?'))
    {
        var link = $(this);
        $.ajax({
            url: base_url + 'practice/company/deleteWCollar/' + link.data('id'),
            dataType: 'json',
            success: function(data) {
            console.log(link.data('id'))
             
           }
        })
        link.parents('.roww').fadeOut('fast', function() {
                      $(this).remove();
                   });    
    }
    })

})(jQuery); 

function updateOrderDrag(data) {
    console.log(base_url)
    $.ajax({
        url: base_url + "practice/questionnare/updateQuestionsOrder",
        type:'post',
        data:{position:data},
        success:function(){
          location.reload();
        }
    })
}

function updateOrderMove(data) {
  console.log(base_url)
  $.ajax({
      url: base_url + "practice/questionnare/updateQuestionsOrder",
      type:'post',
      data:{position:data},
      success:function(){
        
      }
  })
}

function moveToTopic(id) {
    var optionVal = $('#moveToTopic'+ id +' option:selected').val();
    console.log(optionVal)
    $.ajax({
        url: base_url + "practice/questionnare/moveToTopic",
        type:'post',
        data:{topicId:optionVal, questionId:id},
        success:function(){
            alert("Question Moved To Topic Successfully")
        }
    })
    
}// End of use strict
